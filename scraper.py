import io
import json
import os
import sys
import time
import uuid
import zipfile
from concurrent.futures import ThreadPoolExecutor

import prettytable
import requests

from helpers import colors, commons, confighelper
from helpers.classes.errors import ScrapingErrors
from helpers.classes.repositorystatus import RepositoryStatus
from helpers.mongohelper import Client

PACKAGE_SEARCH_URL = "https://%s/api/3/action/package_search"
mongodb_client = Client()

def add_repository(name = None, url = None, print_result = True):
    """
    Stores the metadata of a repository into the MongoDB database.
    
    :param name: (opt) The screen name of the repository.
    :param url: (opt) The URL to the repository.
    :param print_result: (opt) Boolean if the result of the add operation \
    shall be printed or not.
    :returns: The new repo UUID.
    """
    if not (name and url):
        # execution from command line
        if len(sys.argv) > 3:
            url, name = sys.argv[2], sys.argv[3]
        else:
            url, name = input("URL: "), input("Name: ")
    
    # try to request the package search result to check whether the
    # repository is valid
    if url.startswith("http://") or url.startswith("https://"):
        url = url.replace("https://", "").replace("http://", "")

    try:
        r = requests.get(PACKAGE_SEARCH_URL % url)
    except requests.exceptions.SSLError:
        r = requests.get(PACKAGE_SEARCH_URL.replace("https", "http") % url)
    except Exception:
        exit(colors.cstring(
            "[ERROR] Either this website isn't fit for CKAN scraping or another error occured. Aborting.",
            foreground=colors.TColorsForeground.RED
        ))
        
    if r.status_code != requests.codes["ok"] or not r.json()["success"]:
        exit(colors.cstring(
            "[ERROR] This website isn't fit for CKAN scraping, aborting.",
            foreground=colors.TColorsForeground.RED
        ))

    # store the metadata into the MongoDB
    try:
        repo_uuid = mongodb_client.add_repository(name, url)
        if print_result:
            print(f"> OK, added {url} as {name}. UUID: {repo_uuid}")
    except Exception as e:
        print(e, file=sys.stderr)
        repo_uuid = None

    # return the newly generated uuid of the repository
    return repo_uuid


def delete_repository(uuid):
    """
    Deletes a repository.
    
    :param uuid: The UUID of the repository to be deleted.
    :returns: Whether or not the deletion was successful (boolean).
    """
    if mongodb_client.delete_repository(uuid):
        print("> Deleted.")
    else:
        print("> Error: Repository wasn't deleted, probably because it didn't exist.")


def get_package_count(url):
    """
    Gets the number of packages available in a CKAN repository.
    
    :param url: The url to the CKAN repository.
    :returns: The number of packages.
    """
    try:
        r = requests.get(f"https://{url}/api/3/action/package_search?rows=0")
    except requests.exceptions.SSLError:
        r = requests.get(f"http://{url}/api/3/action/package_search?rows=0")

    package_count = int(r.json()["result"]["count"])
    return package_count


def scrape_chunk(repository_uuid, rows, start):
    """
    Fetches a whole chunk of documents.
    
    :param url: The url to the CKAN repository.
    :param rows: The chunk size.
    :param start: The offset.
    :returns: The fetched links, as a list.
    """
    valid_links = []
    url = mongodb_client.get_repository_url(repository_uuid)
    
    try:
        request = requests.get(f"https://{url}/api/3/action/package_search?rows={rows}&start={start}")
    except requests.exceptions.SSLError:
        request = requests.get(f"http://{url}/api/3/action/package_search?rows={rows}&start={start}")
    except Exception as e:
        print(f"----- EXCEPTION ----- {e}")
        mongodb_client.store_scraping_error(repository_uuid, ScrapingErrors.CONNECTION, e)
        return []

    try:
        response = request.json()
    except Exception as e:
        print(f"----- EXCEPTION ----- {e}")
        mongodb_client.store_scraping_error(repository_uuid, ScrapingErrors.UNKNOWN, e)
        return []

    if not "success" in response:
        print(f"----- ERROR ----- Invalid CKAN response (invalid JSON): {str(request.content)}")
        mongodb_client.store_scraping_error(repository_uuid, ScrapingErrors.UNKNOWN, e)
        return []

    if not response["success"]:
        print(f"----- ERROR ----- Invalid CKAN response: {str(request.content)}")
        if "error" in response:
            mongodb_client.store_scraping_error(repository_uuid, ScrapingErrors.CKAN, response["error"]["message"])
        return []
        
    if "results" in response["result"]:
        for result in response["result"]["results"]:
            for resource in result["resources"]:
                if resource["format"].lower() in ["json", "geojson"]:
                    valid_links.append(resource["url"])

    if len(valid_links) > 0:
        print(f"> {len(valid_links)} links fetched (from a chunk).")
    return valid_links


def scrape_urls(repository_uuid):
    """
    Fetches a repository.

    :param uuid: The UUID of the repository to scrape.
    :returns: The number of downloadable resources.
    """
    url = mongodb_client.get_repository_url(repository_uuid)
    start = confighelper.SKIP_RESOURCES
    fetched_links = []
    package_count = get_package_count(url)
    max_links = confighelper.MAX_LINKS_FETCHING if confighelper.MAX_LINKS_FETCHING > 0 else package_count

    executor = ThreadPoolExecutor(max_workers=confighelper.SCRAPING_THREAD_COUNT)
    threads = []
    # Known bug: max. number of links might be higher than MAX_LINKS_FETCHING
    while start < package_count and start < max_links:
        # for _ in range(math.ceil(package_count / confighelper.CHUNK_SIZE)):
        thread = executor.submit(scrape_chunk, repository_uuid, confighelper.CHUNK_SIZE, start)
        start += confighelper.CHUNK_SIZE
        threads.append(thread)

        # Periodically read links out of threads and remove them
        if len(threads) >= confighelper.SCRAPING_THREAD_COUNT * 5:
            for thread in threads[:]:
                valid_links = thread.result()
                threads.remove(thread)
                if len(valid_links) > 0:
                    mongodb_client.add_scraped_urls(repository_uuid, valid_links)
                    print(f"> Stored {len(valid_links)} links.")
                    fetched_links += valid_links
    executor.shutdown()

    # Repetitive code
    for thread in threads[:]:
        valid_links = thread.result()
        threads.remove(thread)
        if len(valid_links) > 0:
            mongodb_client.add_scraped_urls(repository_uuid, valid_links)
            print(f"> Stored {len(valid_links)} links.")
            fetched_links += valid_links
    
    colors.cprint(f"> {len(fetched_links)} links totally fetched.\n", foreground=colors.TColorsForeground.GREEN)
    mongodb_client.add_repository_stats(repository_uuid, package_count, len(fetched_links))
    return len(fetched_links)


def download_repository(uuid = None):
    """
    Starts downloading data of a specific repository.
    
    :param uuid: The UUID of the repository. If no UUID is given, you have \
    to choose from the available ones.
    :returns: The time that the process took in seconds (float).
    """
    if uuid:
        duration = download_files(uuid)
    else:
        available_repositories = mongodb_client.get_repositories()
        choice = None
        print("No UUID provided. Please select one of the available repositories:")
        while not choice:
            i = 0
            table = prettytable.PrettyTable(["No.", "Date", "UUID", "Name"])
            for repo in available_repositories:
                table.add_row([i + 1, repo["date_added"], repo["uuid"], repo["name"]])
                i += 1
            print(table)
            choice = input("\nPlease enter the number of the repository to download: ")
            try:
                repository_uuid = available_repositories[int(choice)-1]["uuid"]
                print(f"> Downloading files of repository with UUID {repository_uuid}...")
                duration = download_files(str(repository_uuid))
            except IndexError:
                print(colors.cstring(
                    "[ERROR] The selection you made does not exist.",
                    foreground=colors.TColorsForeground.RED
                ))
                choice = None
            except ValueError:
                print(colors.cstring(
                    "[ERROR] Your input is not a valid number.",
                    foreground=colors.TColorsForeground.RED
                ))
                choice = None
    return duration


def download_files(uuid):
    """
    Downloads every single file from a given repository.
    
    :param uuid: The UUID of the repository to download files from.
    :returns: The duration in seconds (float).
    """
    start_time = time.time() # Capture start time

    storage_path = os.path.join(confighelper.STORAGE_DIRECTORY, str(uuid))
    
    # download every single file
    link_list = mongodb_client.get_scraped_links(uuid)

    # Download files. If a download limit is set, stop after the limit has
    # been reached.
    downloaded_files = 0
    for link in link_list:
        new_file_uuid = commons.download_file(link, storage_path, uuid)
        if new_file_uuid:
            downloaded_files += 1
        if downloaded_files == confighelper.MAX_LINKS_DOWNLOAD:
            break

    duration = time.time() - start_time
    colors.cprint(f"> Done. Duration: {round(duration, 2)} seconds", foreground=colors.TColorsForeground.GREEN)
    mongodb_client.set_repository_status(uuid, RepositoryStatus.DOWNLOADED)
    return duration


if __name__ == "__main__":
    # No options given
    if len(sys.argv) < 2:
        print("Usage: python3 scraper.py <action> [<options>]")
        print("Available actions: add, delete, download, scrape")
        print("add: <url> <name>")
        print("delete: <uuid>")
        print("download: [<uuid>]")
        print("scrape: <uuid>")
        exit()
    action = sys.argv[1]

    # download a repository
    if action == "download":
        repo_uuid = sys.argv[2] if len(sys.argv) > 2 else None
        download_repository(repo_uuid)

    # add a new repository to the database
    elif action == "add":
        add_repository()

    # delete an existing repository
    elif action == "delete":
        if len(sys.argv) < 3:
            exit("> No UUID given.")
        else:
            delete_repository(sys.argv[2])

    # scrape links of a CKAN repository
    elif action == "scrape":
        if len(sys.argv) > 2:
            repo_uuid = sys.argv[2]
        else:
            # get repositories where status = "added"
            repos = mongodb_client.get_repositories()
            choice = None # the selected repository
            added_repos = [r for r in repos if r["status"] == "added"]

            if len(added_repos) == 0:
                exit("> No repositories available.")
            
            # print repositories as a table
            print("> No UUID given. Please select one of the available scrapable repositories:")
            table = prettytable.PrettyTable(["No.", "UUID", "Name", "URL"])
            for i in range(0, len(added_repos)):
                table.add_row([i + 1, added_repos[i]["uuid"], added_repos[i]["name"], added_repos[i]["url"]])
            print(table)
            while type(choice) is not int:
                choice = input("> Which of the above repositories shall be scraped? ")
                try:
                    choice = int(choice) - 1
                except ValueError:
                    choice = None
            
            # choice has to be in interval [0, len(added_repos)]
            if not choice in range(len(added_repos)):
                exit(colors.cstring(
                    "[ERROR] Invalid input: the number is either negative or too large.",
                    foreground=colors.TColorsForeground.RED
                ))

            repo_uuid = added_repos[choice]["uuid"]

        # scrape the selected repository
        available_resources = scrape_urls(repo_uuid)
        if available_resources:
            colors.cprint(
                f"> {available_resources} links have been scraped into repository {repo_uuid}, exiting...",
                foreground=colors.TColorsForeground.GREEN
            )
        else:
            colors.cprint("> No links have been scraped.", foreground=colors.TColorsForeground.YELLOW)
