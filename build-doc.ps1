# If an old build exists, delete it first
if (Test-Path .\doc\_build\) {
    Write-Output "Deleting old build..."
    Remove-Item -Recurse -Force .\doc\_build\
}

# Create RST files
Write-Output "`nBuilding rst files..."
sphinx-apidoc -o .\doc\ . -f

# Build actual HTML
Write-Output "`nCreating html..."
Set-Location doc
.\make.bat html
Set-Location ..

# Move files into another folder
Write-Output "`nMoving html into sphinx-doc..."
Remove-Item -Recurse -Force .\doc\sphinx-doc\
Copy-Item -Recurse .\doc\_build\html .\doc\sphinx-doc\

Write-Output "Done!"

if ($args[0] -eq "show") {
    Invoke-Item .\doc\sphinx-doc\index.html
}