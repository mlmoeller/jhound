from cmd import Cmd

import prettytable

import analysis
import scraper
from helpers import confighelper
from helpers.colors import TColorsForeground as Colors
from helpers.colors import cstring
from helpers.mongohelper import Client, DurationType

mongodb_client = Client()


class jHoundShell(Cmd):
    prompt = 'jhound> '
    intro = "Welcome to the jHound shell! Type ? to list all commands."
    version = "2.0α"

    # Actual functions

    def do_add(self, inp):
        """
        Add a new repository.
        
        :param inp: Can be ignored.
        """
        name = input("Enter a name for the repository: ")
        url = input("Enter the URL for the repository: ")
        scraper.add_repository(name, url)


    def do_analyze(self, arg):
        """
        Analyze a repository (the legacy way).

        :param arg: The UUID or positional index of the repository.
        """
        if not arg:
            self.list_repos()
            arg = input("UUID or number: ")

        try:
            index = int(arg)
            repos = mongodb_client.get_repositories()
            arg = repos[index - 1]["uuid"]
        except ValueError:
            pass
        finally:
            analysis.compute_files(arg)


    def do_config(self, inp):
        """
        Prints the current configuration or modifies a setting.

        :param inp: If "set", the edit mode will be enabled. \
        Otherwise, the config will be printed.
        """
        if not inp:
            confighelper.print_config()
        elif inp == "set":
            print("Available: ", ", ".join(confighelper.KEYS))
            key = input("Key: ")
            value = input("New value: ")
            
            if not (key or value):
                return
            
            success = mongodb_client.update_config_dict(confighelper.CONFIG_NAME, {key: value})
            print("Done!" if success else "Error while updating config.")


    def do_delete(self, arg):
        """
        Delete a repository.

        :param arg: The UUID or positional index of the repository.
        """
        if not arg:
            self.list_repos()
            arg = input("UUID or number: ")
        
        try:
            index = int(arg)
            repos = mongodb_client.get_repositories()
            arg = repos[index - 1]["uuid"]
        except ValueError:
            pass
        finally:
            scraper.delete_repository(arg)


    def do_download(self, arg):
        """
        Download a repository (the legacy way).

        :param arg: The UUID or positional index of the repository.
        """
        if not arg:
            self.list_repos()
            arg = input("UUID or number: ")

        try:
            index = int(arg)
            repos = mongodb_client.get_repositories()
            arg = repos[index - 1]["uuid"]
        except ValueError:
            pass
        finally:
            duration = scraper.download_repository(arg)
            mongodb_client.store_duration(arg, DurationType.DOWNLOAD_DURATION, duration)


    def do_exit(self, inp):
        """
        Exit the CLI.

        :param inp: Can be ignored.
        :returns: ``True``
        """
        print("Bye")
        return True


    def do_list(self, arg):
        """
        List either all repositories or all nodes, depending on ``arg``.

        :param arg: If either "repo", "repos", or "repositories", all \
        existing repositories will be printed. Otherwise, if "node" or \
        "nodes", all nodes will be printed.
        """
        if arg in ("repo", "repos", "repositories"):
            self.list_repos()
        elif arg in ("node", "nodes"):
            self.list_nodes()
        else:
            self.help_list()


    def do_scrape(self, arg):
        """ Scrape a repository.
        
        :param arg: The UUID or positional index of the repository.
        """
        if not arg:
            self.list_repos()
            arg = input("UUID or number: ")

        try:
            index = int(arg)
            repos = mongodb_client.get_repositories()
            arg = repos[index - 1]["uuid"]
        except ValueError:
            pass
        finally:
            scraper.scrape_urls(arg)


    def do_toggle(self, arg):
        """
        Toggle a node (= turn it on if it's off and vice versa).

        :param arg: The UUID or positional index of the node.
        """
        if not arg:
            self.list_nodes()
            arg = input("UUID or number: ")
            
        if arg.lower() == "all":
            mongodb_client.toggle_all_nodes(confighelper.CONFIG_NAME)
        else:
            try:
                # Check if the argument is an integer. If so, prepare a
                # position-dependent toggle
                index = int(arg)
                nodes = mongodb_client.get_nodes(confighelper.CONFIG_NAME)
                arg = nodes[index - 1]["uuid"]
            except ValueError:
                # Not an integer
                pass
            finally:
                # Actually toggle the node
                (success, is_enabled) = mongodb_client.toggle_node(confighelper.CONFIG_NAME, arg)
                if success:
                    print(f"Switched node {arg} {'on' if is_enabled else 'off'}.")
                else:
                    print("The node couldn't be switched on or off.")


    def do_version(self, arg):
        """
        Get the version of this CLI.
        
        :param arg: Can be ignored.
        """
        print(f"jHound {self.version}")

    # Help functions

    def help_add(self):
        print("Add a new repository. Usage: add <repo-url>")

    
    def help_analyze(self):
        print("Analyze a repository. Usage: analyze <repo-uuid>")
    
    
    def help_config(self):
        print("Print the current configuration. Usage: config")


    def help_delete(self):
        print("Delete a repository. Usage: delete <repo-uuid>")
    
    
    def help_download(self):
        print("Download a repository. Usage: download <repo-uuid>")


    def help_exit(self):
        print("Exit the application. Shorthand: x / q  / Ctrl-D")


    def help_list(self):
        print("List all repositories or nodes. Usage: list [repos|nodes]")

    
    def help_scrape(self):
        print("Scrape a repository. Usage: scrape <repo-uuid>")


    def help_toggle(self):
        print("Toggles a node server on or off. Usage: toggle <node-uuid>")


    def help_version(self):
        print("Shows the version of jHound. Usage: version")

    # Other functions

    def default(self, inp):
        if inp == 'x' or inp == 'q':
            return self.do_exit(inp)
        print(f"Invalid command: {inp}")


    def list_nodes(self):
        """ List all existing nodes. """
        nodes = mongodb_client.get_nodes(confighelper.CONFIG_NAME)
        pt = prettytable.PrettyTable([
            cstring("No."),
            cstring("UUID", foreground=Colors.RED),
            cstring("Name", foreground=Colors.YELLOW),
            cstring("Address", foreground=Colors.GREEN),
            cstring("Port", foreground=Colors.CYAN),
            cstring("Status", foreground=Colors.PINK)
        ])
        for i, node in enumerate(nodes):
            pt.add_row([
                cstring(i + 1),
                cstring(node["uuid"], foreground=Colors.RED),
                cstring(node["name"], foreground=Colors.YELLOW),
                cstring(node["address"], foreground=Colors.GREEN),
                cstring(node["port"], foreground=Colors.CYAN),
                cstring("On" if node["enabled"] else "Off", foreground=Colors.PINK)
            ])
        print(pt)


    def list_repos(self):
        """ List all existing repositories. """
        repos = mongodb_client.get_repositories()
        pt = prettytable.PrettyTable([
            cstring("No."),
            cstring("UUID", foreground=Colors.RED),
            cstring("Name", foreground=Colors.YELLOW),
            cstring("URL", foreground=Colors.GREEN),
            cstring("Date added", foreground=Colors.CYAN),
            cstring("Status", foreground=Colors.PINK)
        ])
        for i, repo in enumerate(repos):
            pt.add_row([
                cstring(i + 1),
                cstring(repo["uuid"], foreground=Colors.RED),
                cstring(repo["name"], foreground=Colors.YELLOW),
                cstring(repo["url"], foreground=Colors.GREEN),
                cstring(repo["date_added"], foreground=Colors.CYAN),
                cstring(repo["status"], foreground=Colors.PINK)
            ])
        print(pt)


    do_EOF = do_exit
    help_EOF = help_exit


if __name__ == '__main__':
    try:
        jHoundShell().cmdloop()
    except KeyboardInterrupt:
        print()
