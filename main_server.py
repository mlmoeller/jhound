import getopt
import json
import os
import pickle
import socket
import sys
import threading
import time

import scraper
from helpers import colors, commons, confighelper
from helpers.classes.filestatus import FileStatus
from helpers.classes.node import Node
from helpers.classes.repositorystatus import RepositoryStatus
from helpers.mongohelper import Client, DurationType

mongodb_client = Client()

nindex, findex = 0, 0 # server/file pointer
repository_uuid = None # UUID of the repository to analyze
OUTPUT = True # whether or not the full results shall be printed
TERMINATE_FLAG = 0

# Flag whether or not the master server listens for incoming messages
LISTENING = False

PORT = confighelper.MASTER_PORT # port of the server to listen to
DELAY = confighelper.DELAY_SECONDS # delay between new connections
MAX_THREADS = confighelper.MAX_THREADS # max number of threads to run on a single node

file_dict = {}
file_urls = []
file_uuids = []

# create a list of (address, port) tuples
# nodes without a uuid will get one temporarily
node_list = []

start_time = None # Variable to capture the start time

def oprint(text, end = "\n", file = sys.stdout):
    global OUTPUT
    if OUTPUT:
        print(text, end=end, file=file)


def check_for_nodes():
    global node_list
    confighelper.load()
    for node in confighelper.NODES:
        uuid = node["uuid"] if "uuid" in node else None
        enabled = node["enabled"] if "enabled" in node else False
        node = Node(node["address"], node["port"], uuid, enabled)

        in_list = False
        for s in node_list:
            if s.get_uuid() == node.get_uuid():
                in_list = True
                if s.is_enabled() != node.is_enabled():
                    s.toggle()

        if not in_list:
            node_list.append(node)


def request_statuses(mode):
    """ Requests the status information for all node servers. """
    global LISTENING
    if not LISTENING:
        threading.Thread(target=listen, args=(mode,), daemon=True).start()
        send_ids_broadcast()

    for node in [s for s in node_list if s.is_enabled()]:
        if not TERMINATE_FLAG:
            s = socket.socket()
            try:
                s.connect(node.get_connection_tuple())
                message = commons.create_message("STATUS")
                s.send(message)
                s.close()
            except ConnectionRefusedError:
                colors.cprint(f"[ERROR] The node at {node.get_connection_tuple()[0]}:{node.get_connection_tuple()[1]}" + \
                    " refused the connection. Make sure that the server is up and running.", foreground=colors.TColorsForeground.RED)


def send_ids(node):
    global node_list, repository_uuid
    s = socket.socket()
    try:
        s.connect(node.get_connection_tuple())
        message = commons.create_message(f"UUID {node.get_uuid()} {repository_uuid}")
        s.send(message)
        s.close()

        for s in node_list:
            if s.get_uuid() == node.get_uuid():
                s.register_sent_uuid()
    except ConnectionRefusedError:
        colors.cprint(f"[ERROR] The node at {node.get_connection_tuple()[0]}:{node.get_connection_tuple()[1]}" + \
            "refused the connection. Make sure that the server is up and running.", foreground=colors.TColorsForeground.RED)


def send_ids_broadcast():
    """ Informs the nodes about their UUIDs and the UUID of the repository to analyze. """
    global node_list, repository_uuid
    for node in [s for s in node_list if s.is_enabled()]:
        send_ids(node)


def proceed_analyzed_message(message):
    """ Handles an incoming ANALYZED message. The results will be \
    decoded and stored to the database.
    
    :param message: The message. """

    global file_dict, OUTPUT, node_list

    node_uuid = message.decode("utf8").split(" ")[1]
    filename = message.decode("utf8").split(" ")[2]
    file_uuid = filename.split("/")[-1]
    result = message.decode("utf8").split(" ")[3]

    # Decode the stats
    result_bytes = bytes.fromhex(result)
    (filesize, stat_builder, glob, array_properties, is_geojson) = pickle.loads(result_bytes)

    # Print results
    oprint(f"> Received result of {file_uuid} by node {node_uuid}.")

    # Write results into the MongoDB
    mongodb_client.store_full_document_results(
        repository_uuid, file_uuid, filesize, stat_builder, glob, array_properties, is_geojson, node_uuid
    )
    oprint(f"> Added results for {file_uuid} to MongoDB.")

    for node in node_list:
        if node.get_uuid() == node_uuid:
            node.remove_file(file_uuid)
            if len(file_dict) > 0:
                file_dict[file_uuid]["status"] = FileStatus.FINISHED


def proceed_downloaded_message(message, mode):
    """
    Handles an incoming DOWNLOADED message. The now downloaded
    file will be removed from the list of pending files.

    :param message: The message.
    :param mode: The mode of the process (download, analyze, both)
    """

    global file_dict, file_urls, node_list

    node_uuid = message.decode("utf8").split(" ")[1]
    file_url = message.decode("utf8").split(" ")[2]
    file_uuid = message.decode("utf8").split(" ")[3]
    oprint(f"> Node {node_uuid} downloaded {file_url} as {file_uuid}.")
    
    try:
        file_urls.remove(file_url)
    except ValueError as e:
        oprint(f"Internal error: {e} not in file url list; probably a thread issue")

    if "a" in mode:
        # Add new/downloaded file to the file dictionary for analysis
        file_dict[file_uuid] = {
            "node": node_uuid,
            "status": FileStatus.WIP
        }

    for node in node_list:
        if node.get_uuid() == node_uuid:
            node.remove_file(file_url)


def proceed_status_message(message, print_message = False):
    """
    Handles an incoming STATUS message. The status information \
    will be stored to the database.

    :param message: The message.
    :param print_message: (opt) Whether or not the \
    status information shall be printed. (Default: False)
    """
    global node_list

    message_params = message.decode("utf8").split(" ")[1:]
    node_uuid = message_params[0]
    cpu_usage = message_params[1]
    ram_total = message_params[2]
    ram_available = message_params[3]
    jobs = message_params[4]
    os_info = message_params[5]
    distro = message_params[6]

    config_name = confighelper.CONFIG_NAME

    node_address, node_port = None, None
    for s in node_list:
        if s.get_uuid() == node_uuid:
            node_address = s.get_address()
            node_port = s.get_port()

    node_stats = {
        "address": node_address,
        "port": node_port,
        "cpu_usage": cpu_usage,
        "ram_total": ram_total,
        "ram_available": ram_available,
        "platform": os_info,
        "distro": distro
    }

    # unmarshal jobs and display information about them
    jobs = pickle.loads(bytes.fromhex(jobs))
    running_jobs = [job for job in jobs if job.exitcode is None]
    failed_jobs = [job for job in jobs if job.exitcode not in (None, 0)]
    if len(failed_jobs) > 0:
        jobs_status = colors.cstring(f"{len(failed_jobs)} failed", foreground=colors.TColorsForeground.RED)
    else:
        jobs_status = colors.cstring(f"{len(failed_jobs)} failed", foreground=colors.TColorsForeground.GREEN)
    
    print(f"Up to {len(running_jobs)} job(s) running on node {node_address}:{node_port}; {jobs_status}")
    for job in failed_jobs:
        colors.cprint("[FAILED JOB]", job, foreground=colors.TColorsForeground.RED)

    mongodb_client.update_node_status(config_name, node_uuid, node_stats)


def distribute_files(mode = "a", wait = False):
    """
    Sends the name of the files to be analyzed to the nodes.
    
    :param download: (opt) If true, the repository will be downloaded. \
    If false, it will be analyzed.
    :param mode: The type of distribution. This can either be \
    'a' (analyze), 'd' (download) or 'ad'/'da' (both).
    """
    global file_uuids, findex, nindex, node_list, start_time, TERMINATE_FLAG

    entries = file_urls.copy() if "d" in mode else file_uuids.copy()
    # print(entries)

    while len(node_list) and findex < len(entries):
        node = node_list[nindex]
        if not node.is_enabled():
            # Increase node pointer by 1 and continue with the next one
            nindex = (nindex + 1) % len(node_list)
            continue

        if not node.knows_uuid():
            send_ids(node)
            node.register_sent_uuid()

        try:
            if node.get_semaphore() < MAX_THREADS:
                # connect to the server
                s = socket.socket()
                oprint(f"> Connecting to {node}...")
                s.connect(node.get_connection_tuple())
                # print(f"> Semaphore (+), new: {node.get_semaphore()}")
            else:
                continue
        except ConnectionRefusedError:
            # connection has been refused; remove server from list and continue
            print(f"> ERROR: Connection to {node} has been refused.")
            print("  This server will be ignored.\n")
            del node_list[nindex]
            nindex -= 1
            if len(node_list) == 0:
                oprint("> No nodes left; exiting.")
                TERMINATE_FLAG = 1
                exit()

            continue
        # next server (imagine them arranged as a ring)
        nindex = (nindex + 1) % len(node_list)

        # transfer next filename in list (as a byte array)
        # syntax: ANALYZE <filename>
        file_descriptor = entries[findex]
        node.add_file(file_descriptor)

        if "a" in mode and "d" in mode:
            message_string = f"DLAN {file_descriptor}"
        elif "d" in mode:
            message_string = f"DOWNLOAD {file_descriptor}"
        else:
            message_string = f"ANALYZE {file_descriptor}"

        s.send(commons.create_message(message_string))
        # request_statuses(mode)
        oprint(f"> Sent {message_string} to {node.get_uuid()}.")
        s.close()
        findex += 1
        time.sleep(DELAY)

    # Terminate iff all files have been downloaded and/or analyzed
    # (depending on the mode); otherwise wait for missing messages to arrive
    wait_until_completion = True
    while wait_until_completion:
        wait_until_completion = False
        if mode in ("da", "ad"):
            # Check if there are files that haven't beem analyzed yet in DLAN mode
            files_left_analysis = False
            for file in file_dict.copy():
                if file_dict[file]["status"] in [FileStatus.NULL, FileStatus.WIP]:
                    files_left_analysis = True

            # Wait until all files are downloaded and analyzed
            wait_until_completion = len(file_urls) > 0 or files_left_analysis

        elif mode == "d":
            # Wait until all files are downloaded
            wait_until_completion = len(file_urls) > 0

        elif mode == "a":
            # Wait until all files are analyzed
            for file in file_dict.copy():
                if file_dict[file]["status"] in [FileStatus.NULL, FileStatus.WIP]:
                    wait_until_completion = True

    # Set the repository status to either "downloaded" or "analyzed",
    # depending on the task        
    if "a" in mode:
        new_status = RepositoryStatus.ANALYZED
        duration_type = DurationType.ANALYSIS_DURATION
    elif "d" in mode:
        new_status = RepositoryStatus.DOWNLOADED
        duration_type = DurationType.DOWNLOAD_DURATION
        
    mongodb_client.set_repository_status(repository_uuid, new_status)

    # Terminate the master server
    oprint("> No files left; exiting.")
    duration = time.time() - start_time
    
    if not wait:
        print(f"> Duration: {round(duration, 2)} seconds")
        mongodb_client.store_duration(
            repository_uuid,
            duration_type,
            duration,
            len([node for node in node_list if node.enabled])
        )
        TERMINATE_FLAG = 1
        return

    return duration


def listen(mode):
    """ Listens for incoming messages, e.g. from the nodes. """
    global OUTPUT, node_list, TERMINATE_FLAG, LISTENING, PORT

    LISTENING = True

    # Set everything up and wait for incoming messages
    s = socket.socket()
    s.bind(("", PORT))
    print("> Listening...")
    s.listen()

    while not TERMINATE_FLAG:
        # Accept incoming messages and receive its content
        (con, addr) = s.accept()
        oprint(f"\n> Connection from {addr[0]}:{addr[1]}")
        
        message = commons.recvall(con)
        command = message.decode("utf8").split(" ")[0]
        
        # Distinguish between message types
        oprint(f"> Command: {command}")
        
        # Analysis of a file has been done; results have arrived
        if command == "ANALYZED":
            threading.Thread(
                target=proceed_analyzed_message,
                args=(message,),
                daemon=True
            ).start()
        
        # A file has been downloaded
        elif command == "DOWNLOADED":
            threading.Thread(
                target=proceed_downloaded_message,
                args=(message, mode),
                daemon=True
            ).start()

        # Node status
        elif command == "STATUS":
            threading.Thread(
                target=proceed_status_message,
                args=(message,),
                daemon=True
            ).start()

        # Close commection
        oprint("> Connection closed.")
        con.close()
    s.close()


def start(repo_uuid, mode, wait = False):
    """ Starts the master server.

    :param repo_uuid: The UUID of the repository.
    :param mode: The mode.
    :param wait: Whether the function should run synchronously (True) \
    or threaded (False). """

    global file_urls, file_uuids, LISTENING, repository_uuid, start_time

    global mongodb_client
    mongodb_client = Client()
    
    start_time = time.time()
    repository_uuid = repo_uuid
    check_for_nodes()
    send_ids_broadcast() # send the ids to the nodes

    if "d" in mode:
        # create a list of all files that should be downloaded 
        file_urls = mongodb_client.get_scraped_links(repo_uuid)
        if not file_urls:
            # Repository hasn't been scraped yet
            oprint("The repository hasn't been scraped yet. It will be done now.")
            scraper.scrape_urls(repo_uuid)
            file_urls = mongodb_client.get_scraped_links(repo_uuid)
    
    if mode == "a":
        # create a list of all file UUIDs in the specified directory 
        try:
            file_uuids = mongodb_client.get_downloaded_files(repo_uuid)
        except KeyError:
            # Repository hasn't been downloaded yet
            exit(colors.cstring(
                "[ERROR] The repository hasn't been downloaded yet, therefore it cannot be analyzed.",
                foreground=colors.TColorsForeground.RED
            ))

        # create a dictionary containing every single file, the responsible node and the
        # current status (WIP, finished, ...)
        for file in file_uuids:
            file_dict[file] = {
                "node": None,
                "status": FileStatus.NULL
            }

    # start listening thread
    if not LISTENING:
        threading.Thread(target=listen, args=(mode,), daemon=True).start()
    
    # start node-checking thread
    threading.Thread(
        target=commons.execute_function_periodically,
        args=(check_for_nodes, confighelper.NODE_UPDATE_INTERVAL),
        daemon=True
    ).start()

    # start status-requesting thread
    threading.Thread(
        target=commons.execute_function_periodically,
        args=(lambda: request_statuses(mode), confighelper.NODE_UPDATE_INTERVAL),
        daemon=True
    ).start()

    # start distribution of files - either threaded or awaited
    if wait:
        duration = distribute_files(mode, True)
        return duration
    else:
        threading.Thread(target=distribute_files, kwargs={"mode": mode}, daemon=False).start()


if __name__ == "__main__":
    # parameter management
    # config: the config file that shall be used; default: "config.json"

    # Possible parameters:
    # -a/--analyze: Analyze the given repository.
    # -d/--download: Download the given repository.
    # -q/--quiet: Print less information about the sent/received information.
    # -r/--repository: The repository that shall be analyzed and/or downloaded.
    # (-a and -d can either be used separately or simultaneously)

    try:
        (opts, args) = getopt.getopt(sys.argv[1:], "adqr:w", ["analyze", "download", "quiet", "repository=", "wait"])
    except getopt.GetoptError as err:
        exit(err)

    mode = ""
    wait = False

    for (opt, arg) in opts:
        if opt in ("-a", "--analyze"):
            # ANALYZE_REPOSITORY = True
            mode += "a"
        elif opt in ("-d", "--download"):
            # DOWNLOAD_REPOSITORY = True
            mode += "d"
        elif opt in ("-q", "--quiet"):
            OUTPUT = False
        elif opt in ("-r", "--repository"):
            repository_uuid = arg
            # print(f"> Analyzing repository {arg}.")
        elif opt in ("-w", "--wait"):
            wait = True

    if not repository_uuid:
        repository_uuid = input("No UUID given. Please enter one. > ")

    check_for_nodes()
    # list servers
    i = 1
    for server in node_list:
        oprint(f"> Server {i}:\n  {server} (Enabled: {server.is_enabled()})\n")
        i += 1

    start(repository_uuid, mode, wait)
