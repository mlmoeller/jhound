import os
import unittest
from helpers.nodeextractor import parse_document


class TestStatistics(unittest.TestCase):
    cwd = None

    def setUp(self):
        self.cwd = os.path.dirname(os.path.abspath(__file__))

    def tearDown(self):
        pass

    def test_geojson(self):
        (_, _, _, is_geojson) = parse_document("test", os.path.join(self.cwd, "test_files", "geojson.json"))
        self.assertEqual(is_geojson, True)