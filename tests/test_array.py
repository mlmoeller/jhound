import os
import unittest
from helpers.nodeextractor import parse_document


class TestStatistics(unittest.TestCase):
    # Add Documentation
    cwd = None

    def setUp(self):
        self.cwd = os.path.dirname(os.path.abspath(__file__))

    def tearDown(self):
        pass

    def __test_func(self, document_name, expected_distribution):
        (_, _, array_properties, _) = parse_document("test", document_name)
        self.assertEqual(array_properties.get_results()[0], expected_distribution)

    def test_simple(self):
        self.__test_func(self.cwd + "/test_files/simple.json", [0, 0, 0, 0, 0])

    def test_array_dif_types(self):
        self.__test_func(self.cwd + "/test_files/array_dif_types.json", [1, 0, 0, 0, 0])

    def test_array(self):
        self.__test_func(self.cwd + "/test_files/array.json", [1, 0, 0, 0, 0])

    def test_all_arrays(self):
        self.__test_func(self.cwd + "/test_files/all_arrays.json", [1, 1, 0, 1, 1])
