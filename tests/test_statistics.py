import os
import unittest
from helpers.nodeextractor import parse_document
from helpers.classes.statisticsbuilder import StatisticsBuilder


class TestStatistics(unittest.TestCase):
    cwd = None

    def setUp(self):
        self.cwd = os.path.dirname(os.path.abspath(__file__))

    def tearDown(self):
        pass

    def __test_func(self, document_name, nb_str, nb_nb, nb_int, nb_bool, nb_array, nb_obj, nb_null):
        custom_objects = {}
        stats = StatisticsBuilder()
        parse_document("test", document_name, stats.tcp, custom_objects)
        # stats.type_analyze_co(custom_objects, custom_objects)
        self.assertEqual(stats.tcp.STRING, nb_str)
        self.assertEqual(stats.tcp.NUMBER, nb_nb)
        self.assertEqual(stats.tcp.INTEGER, nb_int)
        self.assertEqual(stats.tcp.BOOLEAN, nb_bool)
        self.assertEqual(stats.tcp.ARRAY, nb_array)
        self.assertEqual(stats.tcp.OBJECT, nb_obj)
        self.assertEqual(stats.tcp.NULL, nb_null)

    def test_simple(self):
        self.__test_func(self.cwd + "/test_files/simple.json",
                         nb_str=1,
                         nb_nb=0,
                         nb_int=0,
                         nb_bool=0,
                         nb_array=0,
                         nb_obj=1,
                         nb_null=0)

    def test_array(self):
        self.__test_func(self.cwd + "/test_files/array.json",
                         nb_str=0,
                         nb_nb=5,
                         nb_int=0,
                         nb_bool=0,
                         nb_array=1,
                         nb_obj=1,
                         nb_null=0)

    def test_properties(self):
        self.__test_func(self.cwd + "/test_files/properties.json",
                         nb_str=0,
                         nb_nb=3,
                         nb_int=0,
                         nb_bool=0,
                         nb_array=0,
                         nb_obj=4,
                         nb_null=0)

    def test_object(self):
        self.__test_func(self.cwd + "/test_files/object.json",
                         nb_str=2,
                         nb_nb=1,
                         nb_int=0,
                         nb_bool=0,
                         nb_array=0,
                         nb_obj=2,
                         nb_null=0)

    def test_array_dif_types(self):
        self.__test_func(self.cwd + "/test_files/array_dif_types.json",
                         nb_str=1,
                         nb_nb=1,
                         nb_int=0,
                         nb_bool=0,
                         nb_array=1,
                         nb_obj=1,
                         nb_null=1)

    def test_nested_arrays(self):
        self.__test_func(self.cwd + "/test_files/nested_arrays.json",
                         nb_str=0,
                         nb_nb=3,
                         nb_int=0,
                         nb_bool=0,
                         nb_array=3,
                         nb_obj=1,
                         nb_null=0)

    def test_nested_objects(self):
        self.__test_func(self.cwd + "/test_files/nested_objects.json",
                         nb_str=0,
                         nb_nb=1,
                         nb_int=0,
                         nb_bool=0,
                         nb_array=0,
                         nb_obj=3,
                         nb_null=1)

    def test_simple_example(self):
        self.__test_func(self.cwd + "/test_files/example_simple.json",
                         nb_str=1,
                         nb_nb=2,
                         nb_int=0,
                         nb_bool=1,
                         nb_array=1,
                         nb_obj=4,
                         nb_null=0)

    def test_redundancy(self):
        self.__test_func(self.cwd + "/test_files/example_redundancy.json",
                         nb_str=0,
                         nb_nb=6,
                         nb_int=0,
                         nb_bool=1,
                         nb_array=0,
                         nb_obj=7,
                         nb_null=0)

    def test_all_arrays(self):
        self.__test_func(self.cwd + "/test_files/all_arrays.json",
                         nb_str=0,
                         nb_nb=7,
                         nb_int=0,
                         nb_bool=0,
                         nb_array=4,
                         nb_obj=4,
                         nb_null=0)
