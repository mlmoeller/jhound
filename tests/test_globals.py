import os
import unittest
from helpers.nodeextractor import parse_document

class TestStatistics(unittest.TestCase):
    cwd = None

    def setUp(self):
        self.cwd = os.path.dirname(os.path.abspath(__file__))

    def tearDown(self):
        pass

    def __test_func(self, document_name, depth, levels):
        (_, globs, _, _) = parse_document("test", document_name)
        self.assertEqual(globs["max_depth"], depth)
        l = 0
        for level in levels:
            self.assertEqual(globs["children"][l], level)
            l += 1

    def test_simple(self):
        self.__test_func(self.cwd + "/test_files/simple.json", 0, [1])

    def test_array(self):
        self.__test_func(self.cwd + "/test_files/array.json", 0, [1])

    def test_properties(self):
        self.__test_func(self.cwd + "/test_files/properties.json", 1, [3, 3])

    def test_object(self):
        self.__test_func(self.cwd + "/test_files/object.json", 1, [1, 3])

    def test_array_dif_types(self):
        self.__test_func(self.cwd + "/test_files/array_dif_types.json", 0, [1])

    def test_nested_arrays(self):
        self.__test_func(self.cwd + "/test_files/nested_arrays.json", 0, [1])

    def test_nested_objects(self):
        self.__test_func(self.cwd + "/test_files/nested_objects.json", 2, [1, 2, 1])

    def test_simple_example(self):
        self.__test_func(self.cwd + "/test_files/example_simple.json", 3, [1, 3, 1, 2])

    def test_redundancy(self):
        self.__test_func(self.cwd + "/test_files/example_redundancy.json", 2, [3, 3, 7])
