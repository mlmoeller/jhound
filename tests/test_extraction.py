import os
import unittest
from helpers import nodeextractor
from helpers.nodeextractor import NodeType

class TestExtractionUnit(unittest.TestCase):
    """ will need a way to clear custom_objects in nodeextractor """
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_infer(self):
        self.assertEqual(nodeextractor.infer_type("string"), NodeType.STRING)
        self.assertEqual(nodeextractor.infer_type("boolean"), NodeType.BOOLEAN)
        self.assertEqual(nodeextractor.infer_type("integer"), NodeType.INTEGER)
        self.assertEqual(nodeextractor.infer_type("number"), NodeType.NUMBER)
        self.assertEqual(nodeextractor.infer_type("null"), NodeType.NULL)
        # ERRORS
        self.assertEqual(nodeextractor.infer_type("blipbloup"), NodeType.VOID)
        self.assertEqual(nodeextractor.infer_type(452), NodeType.VOID)

    def test_create_object(self):
        # First part
        name = "test_one"
        custom_objects = {}
        nodeextractor.create_object(name, custom_objects)
        self.assertEqual(custom_objects[name]["types"], {})
        self.assertEqual(custom_objects[name]["ltype"], NodeType.VOID)
        # Second part
        nodeextractor.create_object(NodeType.ARRAY, custom_objects)
        self.assertEqual(custom_objects[NodeType.ARRAY]["types"], {})
        self.assertEqual(custom_objects[NodeType.ARRAY]["ltype"], NodeType.VOID)

    def test_create_object_type(self):
        # error meant to be raised
        with self.assertRaises(KeyError):
            nodeextractor.create_object_type({}, NodeType.ARRAY)

        mock_node, mock_type = {"types": {}}, NodeType.NUMBER
        nodeextractor.create_object_type(mock_node, mock_type)
        self.assertTrue(mock_type in mock_node["types"])
        self.assertTrue("children" in mock_node["types"][mock_type])
        self.assertEqual(mock_node["types"][mock_type]["occ"], 1)

    def test_parent_name(self):
        # custom_objects is empty
        (p, _) = nodeextractor.parent_name("abc.def.parent.ghi", {})
        self.assertEqual(p, nodeextractor.ROOT_NAME)

        co = {}
        nodeextractor.create_object("parent", co)
        (p, _) = nodeextractor.parent_name("abc.def.parent.ghi", co)
        self.assertEqual(p, "parent")

        (p, _) = nodeextractor.parent_name("", co)
        self.assertEqual(p, nodeextractor.ROOT_NAME)
        # test the proper nesting detection
        (p, n) = nodeextractor.parent_name("parent.item.item.item.item", co)
        self.assertEqual(p, "parent")
        self.assertEqual(n, 4)

    def test_leaf_name(self):
        self.assertEqual(nodeextractor.leaf_name("aaa.bbb.ccc.son"), "son")
        self.assertEqual(nodeextractor.leaf_name(""), "")
        self.assertEqual(nodeextractor.leaf_name("leaf"), "leaf")

    def test_act_leaf(self):
        # nested array case - init
        custom_objects = {}
        nodeextractor.create_object("test", custom_objects)
        nodeextractor.create_object_type(custom_objects["test"], NodeType.ARRAY)
        custom_objects["test"]["ltype"] = NodeType.ARRAY
        # adds TWO nested children elements
        nodeextractor.act_leaf("aaa.test.item.item.item", "boolean", False, custom_objects)
        self.assertTrue("children" in custom_objects["test"]["types"][NodeType.ARRAY]
                                                                     ["children"][NodeType.ARRAY])
        self.assertTrue("children" in custom_objects["test"]["types"][NodeType.ARRAY]
                                                                     ["children"][NodeType.ARRAY]
                                                                     ["children"][NodeType.ARRAY])
        with self.assertRaises(KeyError):
            self.assertTrue("children" in custom_objects["test"]["types"][NodeType.ARRAY]
                                                                         ["children"][NodeType.ARRAY]
                                                                         ["children"][NodeType.ARRAY]
                                                                         ["children"][NodeType.ARRAY])
        self.assertTrue(custom_objects["test"]["types"][NodeType.ARRAY]
                                                       ["children"][NodeType.ARRAY]
                                                       ["children"][NodeType.ARRAY]
                                                       ["children"][NodeType.BOOLEAN], 1)
        # make sure the field is properly incremented
        nodeextractor.act_leaf("aaa.test.item.item.item", "boolean", False, custom_objects)
        self.assertTrue(custom_objects["test"]["types"][NodeType.ARRAY]
                                                       ["children"][NodeType.ARRAY]
                                                       ["children"][NodeType.ARRAY]
                                                       ["children"][NodeType.BOOLEAN], 2)

        # property case
        nodeextractor.create_object("test_second", custom_objects)
        nodeextractor.act_leaf("aaa.test_second", "integer", 412, custom_objects)
        self.assertEqual(custom_objects["test_second"]["ltype"], NodeType.PROPERTY)
        self.assertEqual(custom_objects["test_second"]["types"][NodeType.PROPERTY]
                                                               ["children"][NodeType.INTEGER], 1)

    def test_act_map_key(self):
        # init
        custom_objects = {}
        nodeextractor.create_object("parent", custom_objects)
        nodeextractor.create_object_type(custom_objects["parent"], NodeType.OBJECT)
        custom_objects["parent"]["ltype"] = NodeType.OBJECT
        # make sure the new key has been added everywhere it needed to be
        nodeextractor.act_map_key("aaa.parent", "newnode", custom_objects)
        self.assertTrue("newnode" in custom_objects)
        self.assertEqual(custom_objects["parent"]["types"][NodeType.OBJECT]
                                                          ["children"]["newnode"], 1)

    def test_act_start_map(self):
        # init
        custom_objects = {}
        self.assertFalse(nodeextractor.act_start_map("aaa", custom_objects))

        nodeextractor.create_object("parent", custom_objects)

        nodeextractor.act_start_map("parent", custom_objects)
        self.assertEqual(custom_objects["parent"]["ltype"], NodeType.OBJECT)
        self.assertEqual(custom_objects["parent"]["types"][NodeType.OBJECT]["occ"], 1)

    def test_act_start_array(self):
        # init
        custom_objects = {}
        self.assertFalse(nodeextractor.act_start_array("aaa", custom_objects))
        nodeextractor.create_object("parent", custom_objects)

        nodeextractor.act_start_array("parent", custom_objects)
        self.assertEqual(custom_objects["parent"]["ltype"], NodeType.ARRAY)
        self.assertEqual(custom_objects["parent"]["types"][NodeType.ARRAY]["occ"], 1)


class TestExtractionGlobal(unittest.TestCase):
    cwd = None

    def setUp(self):
        self.cwd = os.path.dirname(os.path.abspath(__file__))

    def tearDown(self):
        pass

    def test_simple(self):
        (custom_objects, _, _, _) = nodeextractor.parse_document("test", self.cwd + "/test_files/simple.json")
        self.assertTrue("test" in custom_objects)
        self.assertTrue("hello" in custom_objects["test"]["types"][NodeType.OBJECT]["children"])
        self.assertTrue(NodeType.STRING in custom_objects["hello"]["types"][NodeType.PROPERTY]["children"])
        nodeextractor.parsing_done(custom_objects)
