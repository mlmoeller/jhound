# JSON Schemata analytics

The analysis is based on semantic actions stored in *custom_semantics.py*  for a JSON Schema grammar . A recap of the grammar content is available in *grammar-compact.txt* and its variant formatted for the Tatsu library is also available for changes in *grammar-tatsu.ebnf*.
If changes are made to this file, they have to be compiled in order to produce the python files used for parsing. This compilation process is launchable with the script *generate_parser.sh*. The compilation process will overwrite *JSONSchemaParser.py*. Note that this file should never be manually modified.

*custom_semantics_ignored.py* contains all the semantic actions that result from the compilation of the grammar, but aren't used for statistical purposes. 

## Setup

This tool requires multiple libraries  in order to parse huge JSON datasets efficiently, to plot graphs ... and runs under *python3*:
In order to avoid spoiling the rest of the computer with python packages, we suggest creating a virtual environment for this tool

```[bash]
> virtualenv --no-site-packages <name>
> source <name>/bin/activate
```

At this point, you are in a separate environment and only absolute necessary packages are installed.
You need to install the following packages in order to make the project work:

```[bash]
(env)> pip3 install -r <data-tools-dir>/requirements.txt
```

## Usage

The entirety of the tool is available via the file *schema_analytics_script.py*:

```[bash]
python3 schema_analytics_script.py -h
python3 schema_analytics_script.py -c
python3 schema_analytics_script.py -c -d./schemata-dir
python3 schema_analytics_script.py => shows the results based on a file results.txt at the same level

./generate_parser.sh
```
