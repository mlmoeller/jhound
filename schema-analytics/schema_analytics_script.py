import os
import sys
import getopt

from JSONSchemaParser import JSONSchemaParser
from custom_semantics import CustomSemantics


def compute_data(setdir):
    """ Compute statistics thanks to the CustomSemantics object callbacks"""
    parser = JSONSchemaParser()
    reqs, opts, mults, tcps, globs, a_p = [], [], [], [], [], []

    with open('results-by-schema.txt', 'w+') as f:
        # * overwrite file -- intended behaviour
        pass

    for filename in os.listdir(setdir):
        print(filename)
        with open(setdir + '/' + filename, 'r') as content_file:
            content = content_file.read()
            sem = CustomSemantics()
            try:
                ast = parser.parse(content, rule_name='JSDoc', semantics=sem)
            except Exception as e:
                print(str(e))
                continue
            (r, opt, cmrp, ctcp, glob) = sem.report()
            # * update stat objects
            reqs.append(r)
            opts.append(opt)
            mults.append(cmrp)
            tcps.append(ctcp)
            globs.append(glob)
            a_p.append(([0, 0, 0, 0, 0], [0] * 10))  # ! PLACEHOLDER
            # * Save data in a more user-friendly approach, to visualize stats associated with each schema
            with open('results-by-schema.txt', 'a') as f:
                f.write(
                    repr((filename, (r, opt), cmrp, ctcp, glob, ([0, 0, 0, 0, 0], [0] * 10))) + '\n')  # ! PLACEHOLDER

    save_results(reqs, opts, mults, tcps, globs, a_p)


###########################################################################
########################### MAIN ##########################################
###########################################################################

def usage(name):
    print("usage: {} [-h] [-c] [-d=...]".format(name))
    print("  -c (--compute): recompute statistics")
    print("  -l (--loose): compute required stats based on the 'default' property")
    print("  -d=<dir> (--directory): sets directory - default 'sets' ")
    print("  -h (--help): print this help")


def main(argv):
    """Includes calls to computation and graphics display functions"""
    # TODO block execution without any arguments
    compute_data, setdir = False, "sets/"
    try:
        opts, _ = getopt.getopt(argv[1:], "hcd:", ["help", "compute", "directory="])
    except getopt.GetoptError:
        usage(argv[0])
        exit()
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage(argv[0])
            exit()
        elif opt in ("-c", "--compute"):
            compute_data = True
        elif opt in ("-d", "--directory"):
            setdir = arg

    if compute_data:
        compute_data(setdir)

    (r, opt, mult, tcp, globs, _) = load_results()  # ! PLACEHOLDER ARRAY PROPERTIES
    req_percentages = compute_req_stats(r, opt)
    mult_percentages = compute_mult_stats(mult, r, opt)
    type_percentages = compute_type_stats(tcp)
    glob_stats = compute_global_stats(globs)
    show_charts(req_percentages, mult_percentages, type_percentages, glob_stats, [])


if __name__ == "__main__":
    sys.path.append("..")
    from charts_creation_functions import *

    # try:
    #    computeDataSimple()
    # except KeyboardInterrupt:
    #    pass
    main(sys.argv)
