class CSE():
    S_TYPE, S_TEST = range(2)


class CustomSemantics(object):
    class TypeCountProperties(object):
        def __init__(self):
            self.STRING, self.NUMBER, self.INTEGER = 0, 0, 0
            self.BOOLEAN, self.ARRAY, self.OBJECT, self.NULL = 0, 0, 0, 0

        def compact(self):
            return (self.STRING, self.NUMBER, self.INTEGER,
                    self.BOOLEAN, self.ARRAY, self.OBJECT, self.NULL)

    class MultResProperties(object):
        def __init__(self):
            self.ALLOF, self.ANYOF, self.ONEOF = 0, 0, 0

        def compact(self):
            return self.ALLOF, self.ANYOF, self.ONEOF

    class RequiredProperties(object):
        def __init__(self):
            self.REQUIRED, self.KEYS = 0, 0

    def __init__(self):
        # global properties
        self.DEPTH, self.MAX_DEPTH = 0, 0
        self.CHILDREN_COUNT = [0]
        # local properties
        self.tcp = CustomSemantics.TypeCountProperties()
        self.mrp = CustomSemantics.MultResProperties()
        self.rp = CustomSemantics.RequiredProperties()

    def JSDoc(self, ast):  # noqa
        assert (self.DEPTH == 0)
        # ? the whole parsing has been done, you can print the final step of internal objects here
        #  print(self.rp.KEYS, self.mrp.compact())
        # assert(self.rp.KEYS >= (self.mrp.ALLOF + self.mrp.ANYOF + self.mrp.ONEOF))
        return ast

    def globval(self, ast):  # noqa
        return ast

    def comment(self, ast):  # noqa
        return ast

    def version(self, ast):  # noqa
        return ast

    def defs(self, ast):  # noqa
        return ast

    def kSch(self, ast):  # noqa
        self.rp.KEYS += 1
        return ast

    def JSch(self, ast):  # noqa
        return ast

    def name(self, ast):
        return ast

    def res(self, ast):  # noqa
        return ast

    def type(self, ast):  # noqa
        return (CSE.S_TYPE, ast[-1])

    def typename(self, ast):  # noqa
        tname = ast[1:-1]
        if tname == "null":
            self.tcp.NULL += 1
        elif tname == "string":
            self.tcp.STRING += 1
        elif tname == "integer":
            self.tcp.INTEGER += 1
        elif tname == "number":
            self.tcp.NUMBER += 1
        elif tname == "boolean":
            self.tcp.BOOLEAN += 1
        elif tname == "array":
            self.tcp.ARRAY += 1
        elif tname == "object":
            self.tcp.OBJECT += 1
        else:
            print("BUG infering type")
        return tname

    def arrRes(self, ast):  # noqa
        return ast

    def items(self, ast):  # noqa
        return ast

    def sameitems(self, ast):  # noqa
        return ast

    def varitems(self, ast):  # noqa
        return ast

    def additems(self, ast):  # noqa
        return ast

    def objRes(self, ast):  # noqa
        return ast

    def prop(self, ast):  # noqa
        return ast

    def addprop(self, ast):  # noqa
        return ast

    def req(self, ast):  # noqa
        self.rp.REQUIRED += len(ast)
        return ast

    def pattprop(self, ast):  # noqa
        return ast

    def patSch(self, ast):  # noqa
        return ast

    def multRes(self, ast):  # noqa
        return ast

    def anyOf(self, ast):  # noqa
        self.mrp.ANYOF += 1
        return ast

    def allOf(self, ast):  # noqa
        self.mrp.ALLOF += 1
        return ast

    def oneOf(self, ast):  # noqa
        self.mrp.ONEOF += 1
        return ast

    def not_(self, ast):  # noqa
        return ast

    def enum(self, ast):  # noqa
        return ast

    def annotations(self, ast):  # noqa
        return ast

    def id(self, ast):  # noqa
        return ast

    def default(self, ast):
        return ast

    def examples(self, ast):  # noqa
        return ast

    def format(self, ast):  # noqa
        return ast

    def refSch(self, ast):  # noqa
        return ast

    def value(self, ast):  # noqa
        return ast

    def n(self, ast):  # noqa
        return ast

    def r(self, ast):  # noqa
        return ast

    def bool(self, ast):  # noqa
        return ast

    def address(self, ast):  # noqa
        return ast[1:-1]

    def kword(self, ast):  # noqa
        key = ast[1:-1]
        return key

    def string(self, ast):  # noqa
        return ast[1:-1]

    def _default(self, ast):
        # print("UNMATCHED ! {}".format(ast))
        return ast

    def ob(self, ast):
        self.CHILDREN_COUNT[self.DEPTH] += 1
        self.DEPTH += 1
        if self.DEPTH > self.MAX_DEPTH:
            self.CHILDREN_COUNT += [0]
            self.MAX_DEPTH = self.DEPTH
        return ast

    def cb(self, ast):
        self.DEPTH -= 1
        return ast

    def report(self, printReport=False):
        # self.rp.REQUIRED = min(self.rp.REQUIRED, self.rp.KEYS) # only to avoid errors
        if printReport:
            print("REQUIRED : {}".format(self.rp.REQUIRED))
            print("OPTIONAL: {}".format(self.rp.KEYS - self.rp.REQUIRED))
            print("ONEOF : {}".format(self.mrp.ONEOF))
            print("ALLOF : {}".format(self.mrp.ALLOF))
            print("ANYOF : {}".format(self.mrp.ANYOF))
            print("ARRAY : {}".format(self.tcp.ARRAY))
            print("DEPTH : {}".format(self.MAX_DEPTH))
        # print("{} {}".format(self.rp.KEYS, self.rp.REQUIRED))
        assert ((self.rp.KEYS - self.rp.REQUIRED) >= 0)
        return (self.rp.REQUIRED, self.rp.KEYS - self.rp.REQUIRED, self.mrp.compact(), self.tcp.compact(),
                (self.MAX_DEPTH, self.CHILDREN_COUNT))
