import json
import os

config_path = __file__[:-len(os.path.basename(__file__))].replace("webapp/", "") + "config.json"

def get_config_name():
    """ Gets the name of the config to use.

    :returns: The name of the config to use, or None, if no local \
    config.json file exists. """
    
    if os.path.exists(config_path):
        config_file = open(config_path, "r", encoding="utf8")
        config_name = json.load(config_file)["config_name"]
        config_file.close()
        return config_name
    return None


def create_local_config(config_dict):
    """
    Creates a new local config.json file containing the MongoDB configuration.
    
    :param config_dict: The dictionary containing config_name, mongo_url, \
    mongo_port (optional; default: 27017), mongo_username, mongo_password and \
    collection_name.
    """

    global config_path

    # Check if all required properties are there
    required_values = ["config_name", "mongo_url", "mongo_username", "mongo_password", "collection_name"]

    if set(required_values).issubset(config_dict.keys()):
        config_file = open(config_path, "w", encoding="utf8")
        json.dump(config_dict, config_file, indent=4)
        config_file.close()


def update_config_param(key, value):
    """ Updates a single parameter of the config.json file.
    
    :param key: The name of the parameter.
    :param value: The new value of the parameter. """
    
    global config_path

    # Load current configuration
    config_file = open(config_path, "r", encoding="utf8")
    config = json.load(config_file)
    config_file.close()

    # Update configuration
    if key and value:
        config[key] = value
    else:
        # TODO throw an error)
        pass

    # Write new configuration
    config_file = open(config_path, "w", encoding="utf8")
    json.dump(config, config_file, indent=4)
    config_file.close()