$(document).ready(function () {

    var debug = true;
    debug && console.log('%c Achtung! Debug-Modus ist aktiviert ', 'color: red; font-size:14px;');

    if (!window.location.origin) {
        window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
    }

    function baselink(target){
        return window.location.origin + '/' + target + '/';
    }

    /**
     * Get available repositories for the sidebar - This is the case for ALL pages
     */
    
    $.get('/getRepositories' , function (response) {
        response = $.parseJSON(response);
        $(response).each(function (key, value) {
            $('#navbar-section-available-datasources').append('<li><a href="'+ baselink('repository') + value.uuid + '">' + value.name + '</a></li>');
            $('#table-repository-overview').append('<tr>' +
                '<td><button class="btn btn-sm btn-primary" onclick="location.href=\'' + baselink('repository') + value.uuid + '\'"><span class="fa fa-search"></span></button></td>' +
                '<td>' + value.name + '</td>' +
                '<td>' + value.url + '</td>' +
                '<td>' + value.date_added + '</td>' +
                '<td><span class="label label-default">' + value.uuid + '</span></td>' +
                '<td><span class="label label-primary">' + value.status + '</span></td>' +
                '<td>' + handleRepositoryStatus(value.status, value.uuid) + '</td>' +
                '</tr>'
            );
            if(value.status === 'analyzed') {
                $('#navbar-section-available-results').append('<li><a href="'+ baselink('result') + value.uuid + '">' + value.name + '</a></li>');
            }
            debug && console.log(value)
        })
    });


    function handleRepositoryStatus(status, uuid) {
        switch (status) {
            case 'added':
                // Create Button
                button = '<button class="btn btn-default btn-sm" id="scrape-' + uuid +'" name="scrape-' + uuid + '">Scrape</button>';

                /**
                 * Add Ajax Button Listener
                 * Because the button with the id #scrape-<uuid> is generated dynamically,
                 * we need to bind it to an existing static anchestor element. In this case it is the table
                 */
                $('#table-of-repositories, #repository-action-container').off('click').on('click', '#scrape-' + uuid, function () {
                    debug && console.log('Scraping Repository ' + uuid);
                    $(this).prop('disabled', true);
                    $.ajax({
                        type: 'GET',
                        url: '/startScraping/' + uuid,
                        dataType: 'json',
                        success: function (response) {
                            console.log(response)
                        },
                        error: function (response) {
                            console.error(response)
                        }
                    })
                });

                return button;

            case 'scraped':
                button = '<button class="btn btn-default btn-sm" id="download-' + uuid +'" name="download-' + uuid + '">Download</button>';

                /**
                 * Add Ajax Button Listener
                 * Because the button with the id #scrape-<uuid> is generated dynamically,
                 * we need to bind it to an existing static anchestor element. In this case it is the table
                 */
                $('#table-of-repositories, #repository-action-container').off('click').on('click', '#download-' + uuid, function () {
                    debug && console.log('Downloading Repository Files ' + uuid);
                    $(this).prop('disabled', true);
                    $.ajax({
                        type: 'POST',
                        url: '/downloadRepository/' + uuid,
                        data: {},
                        dataType: 'json',
                        success: function (response) {
                            console.log(response)
                        },
                        error: function (response) {
                            console.error(response)
                        }
                    })
                });

                return button;

            case 'scraping':
            case 'downloading':
            case 'analyzing':
                return '<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>'

            case 'downloaded':
            case 'downloaded_ir': // download_interrupt only for testing purposes
                button = '<button class="btn btn-default btn-sm" id="analyze-' + uuid +'" name="analyze-' + uuid + '">Analyze</button>';

                /**
                 * Add Ajax Button Listener
                 * Because the button with the id #scrape-<uuid> is generated dynamically,
                 * we need to bind it to an existing static anchestor element. In this case it is the table
                 */
                $('#table-of-repositories, #repository-action-container').off('click').on('click', '#analyze-' + uuid, function () {
                    debug && console.log('Analyzing Repository ' + uuid);
                    $(this).prop('disabled', true);
                    $.ajax({
                        type: 'POST',
                        url: '/analyzeRepository/' + uuid,
                        data: {},
                        dataType: 'json',
                        success: function (response) {
                            console.log(response)
                        },
                        error: function (response) {
                            console.error(response)
                        }
                    })
                });

                return button;


            case 'analyzed':
                button = '<button class="btn btn-success btn-sm" id="viewresult-' + uuid +'" name="viewresult-' + uuid + '">View Results</button>';

                /**
                 * Add Ajax Button Listener
                 * Because the button with the id #scrape-<uuid> is generated dynamically,
                 * we need to bind it to an existing static anchestor element. In this case it is the table
                 */
                $('#table-of-repositories, #repository-action-container').off('click').on('click', '#viewresult-' + uuid, function () {
                    window.location.href = '/result/' + uuid
                });

                return button;
            default:
                return "unimplemented";
        }
    }


    /**
     * Handle form for adding a new repository
     */

    $('#new-repository-submit').click(function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/addRepository',
            data: {
                'new_repository_name': $("[name=new_repository_name]").val(),
                'new_repository_url': $("[name=new_repository_url]").val()
            },
            dataType: 'json',
            success: function (response) {
                console.log(response);
                window.location.href = '/repositories/?highlight=' + response.uuid;
            },
            error: function (response) {
                console.error(response)
            }
        })
    });


    /**
     * Parse the URL and execute custom onload actions
     * e.g. http://127.0.0.1:5000/repository/A1B2C3
     * --> pathArray['0'] = ""
     * --> pathArray['1'] = "repository"
     * --> pathArray['2'] = "A1B2C3"
     */

    var pathArray = window.location.pathname.split('/');
    if (pathArray['1'] === 'repository'){
        onloadForSingleRepository(pathArray['2'])
    } else if(pathArray['1'] === 'result'){
        onloadForRepositoryResultPage(pathArray['2'])
    } else if(pathArray['1'] === '' || pathArray['1'] === 'dashboard'){
        onloadForDashboard()
    }

    function onloadForDashboard() {

        /*
         * Get Node Information
         */

        $.get('/node/get/all/statuses' , function (response) {
        response = $.parseJSON(response);
        $(response).each(function (key, value) {
            $('#node_overview_container').append(
                '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">' +
                '   <div class="panel panel-success" id="nodepanel-' + value['node_uuid'] + '">' +
                '       <div class="panel-heading" style="margin-bottom: 20px;">' +
                '           <div class="row">'+
                '               <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9" style="font-weight: bold" id="">' +
                                  //  value['node_uuid'] +
                '               </div>' +
                '               <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-right" id="nodepanel-activestate-' + value['node_uuid'] + '">'+
                '               </div>'+
                '           </div>' +
                '       </div>' +
                '       <div class="panel-body">' +
                '           <div class="row">' +
                '               <div class="col-xs-2 col-sm-2 col-md-4 col-lg-4"></div>' +
                '               <div class="col-xs-8 col-sm-8 col-md-4 col-lg-4 text-center"><img height="75px" src="' + getOSImagePath(value['distro']) + '" class="distro-logo distro-logo-colored" id="distro-logo-' + value['node_uuid'] + '" /></div>' +
                '               <div class="col-xs-2 col-sm-2 col-md-4 col-lg-4"></div>' +
                '         </div>' +
                '         <div class="row">' +
                '               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: center">' +
                '                   <h3 id="gecos-'+ value['node_uuid'] +'"></h3>' +
                '                   <p>' + value['node_uuid'] + '</p>' +
                '               </div>' +
                '         </div>' +
                '         <hr />' +
                '         <div class="row">' +
                '            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-right">' +
                '                  <span class="fa fa-hdd-o"></span> URI' +
                '            </div>' +
                '            <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9" id="uri-'+ value['node_uuid'] +'">' +
                                value['address'] + ':' + value['port'] +
                '              </div>' +
                '           </div>' +
                '           <div class="row">' +
                '               <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-right">' +
                '                   <span class="fa fa-tachometer"> CPU </span>'+
                '               </div>' +
                '               <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">' +
                                    value['properties'].slice(-1).pop()['cpu_usage'] +  // .slice(-1).pop() gets last array element
                '               </div>' +
                '           </div>' +
                '           <div class="row">' +
                '               <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-right">' +
                '                   <span class="fa fa-tasks"> RAM </span>' +
                '               </div>' +
                '               <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">' +
                                    formatBytes(value['properties'].slice(-1).pop()['ram_available'],2) + '/' + formatBytes(value['properties'].slice(-1).pop()['ram_total'],2) +
                '               </div>' +
                '           </div>' +
                '           <hr />' +
                '           <div class="row text-center">' +
                '               <button type="button" class="btn btn-default btn-circle" id="nodepanel-toggleactivebtn-'+ value['node_uuid'] +'">' +
                '                   <i class="fa fa-power-off"></i>' +
                '               </button>' +
                '               <button type="button" class="btn btn-default btn-circle" id="nodepanel-inspectbtn-'+ value['node_uuid'] +'">' +
                '                   <i class="fa fa-search"></i>' +
                '               </button>' +
                '               <button type="button" class="btn btn-default btn-circle" id="nodepanel-modifybtn-'+ value['node_uuid'] +'">' +
                '                   <i class="fa fa-pencil"></i>' +
                '               </button>' +
                '               <button type="button" class="btn btn-default btn-circle" id="nodepanel-deletebtn-'+ value['node_uuid'] +'">' +
                '                   <i class="fa fa-times"></i>' +
                '               </button>' +
                '           </div>' +
                '       </div>' +
                '   </div>'+
                '</div>'
            );

            $.get('/node/get/'+ value['node_uuid'], function (innerresponse) {
                innerresponse = $.parseJSON(innerresponse);
                $('#uri-'+ value['node_uuid']).html(innerresponse['address'] + ':' + innerresponse['port']);
                $('#gecos-'+ value['node_uuid']).html(innerresponse['name']);
            });

            $('#nodepanel-toggleactivebtn-' + value['node_uuid']).click(function () {
                console.log(value['node_uuid']);
                panel = $('#nodepanel-' + value['node_uuid']);
                distrologo = $('#distro-logo-' + value['node_uuid']);
                activelabel = $('#nodepanel-activestate-' + value['node_uuid']);

                if(panel.hasClass('panel-success')){
                    // Shutdown active node
                    panel.removeClass('panel-success').addClass('panel-danger');
                    distrologo.removeClass('distro-logo-colored').addClass('distro-logo-grayscale');
                } else {
                    // Boot-up inactive node
                    panel.removeClass('panel-danger').addClass('panel-success');
                    distrologo.removeClass('distro-logo-grayscale').addClass('distro-logo-colored');
                }
            });

            debug && console.log(value)
        });
    });
    }

    function onloadForSingleRepository(uuid){

        var linksAlreadyFetched = false;

        /**
         * Get basic repository information
         */

        $.ajax({
            type: 'GET',
            url: uuid ? '/getBasicRepositoryInformation/' + uuid : '/getBasicRepositoryInformation',
            success: function (response) {
                debug && console.log(response);
                $('#repository-basic-information').empty().append(
                    '<ul>'+
                    '<li>Repository Name: ' + response.name + '</li>' +
                    '<li>Repository URL: ' + response.url + '</li>' +
                    '<li>Repository added: ' + response.date_added + '</li>' +
                    '<li>UUID: ' + uuid + '</li>' +
                    '<li>Status: ' + response.status + '</li>' +
                    '</ul>'
                );
                $('#repository-name').empty().append("Repository <b>" + response.name + "</b>");

                // Handle repository action button

                $("#repository-action-container").empty().append(handleRepositoryStatus(response.status, uuid))

            },
            error: function () {
                $('#repository-basic-information').empty().append(
                    '<div class="alert alert-danger">' +
                    '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>' +
                    ' The basic information could not be retrieved' +
                    '</div>'
                )
            }
        });

        /**
         * Get basic repository statistics
         */

        $.ajax({
            type: 'GET',
            url: '/getBasicRepositoryStatistics/' + uuid,
            success: function (response) {
                debug && console.log(response);
                $('#repository-basic-statistics').empty().append(
                    '<ul>'+
                    '<li>Links scraped: ' + response.links_scraped + '</li>' +
                    '<li>Total packages: ' + response.total_packages + '</li>' +
                    '</ul>'
                )
            },
            error: function (response) {
                debug && console.error(response);
                $('#repository-basic-statistics').empty().append(
                    '<div class="alert alert-info">' +
                    '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>' +
                    ' The repository needs to be <b>scraped</b> before basic statistics can be displayed.' +
                    '</div>'
                );
                $('#repository-scraped-links').empty().append(
                    '<div class="alert alert-info">' +
                    '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>' +
                    ' The repository needs to be <b>scraped</b> before basic statistics can be displayed.' +
                    '</div>'
                )
            }
        });


        /**
         * Get advanced repository statistics
         */

        $.ajax({
            type: 'POST',
            url: '/getAdvancedRepositoryStatistics/'+uuid,
            data: {},
            dataType: 'json',
            success: function (response) {
                debug && console.log(response);

                tableHTML =
                    '<table class="table table-striped table-bordered table-hover table-condensed table-responsive"><thead><tr>' +
                    '<th>UUID Prefix</th>' +
                    '<th>Req. Props.</th>' +
                    '<th>Nreq. Props.</th>' +
                    '<th>Str</th>' +
                    '<th>Num</th>' +
                    '<th>Int</th>' +
                    '<th>Bool</th>' +
                    '<th>Arr</th>' +
                    '<th>Obj</th>' +
                    '<th>Null</th>' +
                    '<th>Simple</th>' +
                    '<th>Only Arr</th>' +
                    '<th>Obj</th>' +
                    '<th>Mixed</th>' +
                    '<th>Empty</th>' +
                    '<th>Nested</th>' +
                    '<th>Bool Abuse</th>' +
                    '<th>Num Abuse</th>' +
                    '<th>Empty String</th>' +
                    '<th>Max Depth</th>' +
                    '<th>Childs</th>' +
                    '</tr></thead><tbody>';

                $.each(response[0], function (key, pvalue) {
                    $.each(pvalue, function (key, value) {
                        tableHTML +=
                            '<tr>' +
                            '<td> ' + value.uuid.substr(0,8) + ' </td>' +
                            '<td style="text-align: right;"> ' + value.analysis_properties.required_properties + ' </td>' +
                            '<td style="text-align: right;"> ' + value.analysis_properties.optional_properties + ' </td>' +
                            '<td style="text-align: right;"> ' + value.analysis_properties.type_counts.string + ' </td>' +
                            '<td style="text-align: right;"> ' + value.analysis_properties.type_counts.number + ' </td>' +
                            '<td style="text-align: right;"> ' + value.analysis_properties.type_counts.integer + ' </td>' +
                            '<td style="text-align: right;"> ' + value.analysis_properties.type_counts.boolean + ' </td>' +
                            '<td style="text-align: right;"> ' + value.analysis_properties.type_counts.array + ' </td>' +
                            '<td style="text-align: right;"> ' + value.analysis_properties.type_counts.object + ' </td>' +
                            '<td style="text-align: right;"> ' + value.analysis_properties.type_counts.null + ' </td>' +
                            '<td style="text-align: right;"> ' + value.analysis_properties.array_properties.simple + ' </td>' +
                            '<td style="text-align: right;"> ' + value.analysis_properties.array_properties.only_array + ' </td>' +
                            '<td style="text-align: right;"> ' + value.analysis_properties.array_properties.object + ' </td>' +
                            '<td style="text-align: right;"> ' + value.analysis_properties.array_properties.mixed + ' </td>' +
                            '<td style="text-align: right;"> ' + value.analysis_properties.array_properties.empty + ' </td>' +
                            '<td> ' + value.analysis_properties.array_properties.nested + ' </td>' +
                            '<td style="text-align: right;"> ' + value.analysis_properties.string_abuse.abusive_boolean + ' </td>' +
                            '<td style="text-align: right;"> ' + value.analysis_properties.string_abuse.abusive_number + ' </td>' +
                            '<td style="text-align: right;"> ' + value.analysis_properties.string_abuse.empty_string + ' </td>' +
                            '<td style="text-align: right;"> ' + value.analysis_properties.globs.max_depth + ' </td>' +
                            '<td> ' + value.analysis_properties.globs.children + ' </td>' +
                            '</tr>'
                    })
                });

                tableHTML += '</tbody></table>';
                $('#repository-advanced-statistics').empty().append(tableHTML)
            },
            error: function (response) {
                debug && console.error(response);
                $('#repository-advanced-statistics').empty().append(
                    '<div class="alert alert-info">' +
                    '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>' +
                    ' The repository needs to be <b>analyzed</b> before advanced statistics can be displayed.' +
                    '</div>'
                );
            }
        });


        /**
         * Listener for toggling links
        */

        $("#repository-toggle-scraped-links").click(function () {
            if(linksAlreadyFetched) {
                return // Fetch the links once
            }

            $.ajax({
                type: 'POST',
                url: '/getScrapedLinksForRepository/'+uuid,
                data: {},
                dataType: 'json',
                success: function (response) {
                    debug && console.log(response);
                    $('#collapseScrapedLinks').empty();
                    $.each(response, function (index, value) {
                        $('#collapseScrapedLinks').append(
                            '<p>[' + index + '] <a target="_blank" href="' + value + '">' + value + '</a></p>'
                        )
                    })
                },
                error: function (response) {
                    debug && console.error(response);
                    $('#collapseScrapedLinks').empty().append(
                        '<div class="alert alert-warning">' +
                        '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>' +
                        ' The scraped links cannot be found in the database.' +
                        '</div>'
                    )
                }
            });

            linksAlreadyFetched = true
        });


        /**
         * Create delete listeners
         */

        $("#repository-delete-fully").click(function () {
            $.ajax({
                type: 'POST',
                url: '/deleteRepository/Full/' + uuid,
                data: {},
                dataType: 'json',
                success: function (response) {
                    debug && console.log(response);
                    window.location.href = '/repositories';
                },
                error: function (response) {
                    debug && console.error(response)

                }
            })
        });

        $("#repository-delete-db").click(function () {
            $.ajax({
                type: 'POST',
                url: '/deleteRepository/DB/' + uuid,
                data: {},
                dataType: 'json',
                success: function (response) {
                    debug && console.log(response);
                    window.location.href = '/repositories';
                },
                error: function (response) {
                    debug && console.error(response)

                }
            })
        });

        $("#repository-delete-local").click(function () {
            $.ajax({
                type: 'POST',
                url: '/deleteRepository/Local/' + uuid,
                data: {},
                dataType: 'json',
                success: function (response) {
                    debug && console.log(response);
                    window.location.href = '/repositories';
                },
                error: function (response) {
                    debug && console.error(response)

                }
            })
        })
    }
    
    
    function onloadForRepositoryResultPage(uuid) {
        $.ajax({
            type: 'GET',
            url: uuid ? '/getOptionalRequiredProperties/' + uuid : '/getOptionalRequiredProperties',
            success: function (response) {
                debug && console.log("Required properties " + response.required_properties);
                debug && console.log("Optional properties " + response.optional_properties);
                drawOptionalRequiredChart(response.optional_properties, response.required_properties)
            },
            error: function (response) {
                debug && console.error(response)

            }
        });


        $.ajax({
            type: 'GET',
            url: uuid ? '/getDataTypeDistribution/' + uuid : '/getDataTypeDistribution',
            success: function (response) {
                debug && console.log("Required properties " + response);
                drawDatatypeDistributionChart(
                    response.strings,
                    response.numbers,
                    response.integers,
                    response.booleans,
                    response.arrays,
                    response.objects,
                    response.nulls
                )
            },
            error: function (response) {
                debug && console.error(response)

            }
        });


        $.ajax({
            type: 'GET',
            url: uuid ? '/getFilesizeHistogramValues/' + uuid : '/getFilesizeHistogramValues',
            success: function (response) {
                debug && console.log(response);
                drawFilesizeHistogramChart(response)
            },
            error: function (response) {
                debug && console.error(response)

            }
        });


        $('#filesizeHistogramToggleLinearLogarithmic').click(function () {
                if(filesizeHistogram.options.scales.yAxes[0].type === 'linear'){
                    filesizeHistogram.options.scales.yAxes[0].type = 'logarithmic';
                    filesizeHistogram.options.scales.yAxes[0].ticks.maxTicksLimit = 10;
                } else {
                    filesizeHistogram.options.scales.yAxes[0].type = 'linear';
                    filesizeHistogram.options.scales.yAxes[0].ticks.maxTicksLimit = 100;
                }
                filesizeHistogram.update()
            }
        );


        $.ajax({
            type: 'GET',
            url: uuid ? '/getBubblechartValues/' + uuid : '/getBubblechartValues',
            success: function (response) {
                debug && console.log(response.data);
                drawBubblechart(response.data)
            },
            error: function (response) {
                debug && console.error(response)

            }
        });

        $('#view-failed-downloads').click(function () {
            $.ajax({
                type: 'GET',
                url: uuid ? '/getFailedDownloads/' + uuid : '/getFailedDownloads',
                success: function (response) {
                    debug && console.log(response)
                    $('#failedDownloadsModalBody').empty()
                    $.each(response, function (key, value) {
                        $('#failedDownloadsModalBody').append(
                            '<p><a href="' + value.url + '">' + value.url.substr(0,80) + '...</a><br /> failed with error code <b>' + value.type + '</b> (' + value.context + ') at ' + value.ts + '</p>'
                        )
                    })
                    $('#failedDownloadsModal').modal('show')
                },
                error: function (response) {
                    debug && console.error(response)

                }
            });
        })
    }

});


/*
Chart functions
 */

/**
 * Chart for Optional and Required Charts
 * @param amtOptional The amount of optional properties
 * @param amtRequired The amout of required properties
 */

function drawOptionalRequiredChart(amtOptional, amtRequired) {
    var ctx = document.getElementById("optionalRequiredChart").getContext('2d');
    new Chart(ctx, {
        type: 'pie',
        data: {
            labels: ["Always Present", "Not Always Present"],
            datasets: [{
                data: [amtRequired,amtOptional],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)'
                ],
                borderWidth: 1,
                pointStyle: 'circle'
            }]
        },
        options: {
            title: {
                text: 'Property Frequency',
                display: true,
                fontSize: 18,
                fontStyle: 'normal'
            },
            legend: {
                position: 'bottom',
                labels: {
                    usePointStyle: true
                }
            }
        }
    });
}


function drawDatatypeDistributionChart(amtString, amtNumbers, amtIntegers, amtBooleans, amtArrays, amtObjects, amtNulls) {
    var ctx = document.getElementById("datatypeDistributionChart").getContext('2d');
    new Chart(ctx, {
        type: 'pie',
        data: {
            labels: ["String", "Number", "Integer", "Boolean", "Array", "Object", "Null"],
            datasets: [{
                data: [amtString, amtNumbers, amtIntegers, amtBooleans, amtArrays, amtObjects, amtNulls],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 159, 64, 0.6)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                    borderWidth: 1,
                    pointStyle: 'circle'
                }]
        },
        options: {
            title: {
                text: 'Data Type Distribution',
                display: true,
                fontSize: 18,
                fontStyle: 'normal'
            },
            legend: {
                position: 'bottom',
                labels: {
                    usePointStyle: true
                }
            }
        }
    });
}


function drawFilesizeHistogramChart(histogramValues) {

    diagram_keys = [];
    diagram_values = [];

    $.each(histogramValues.histogram_values, function (key, value) {
        diagram_keys.push(formatBytes(Math.pow(2,key-1)) + ' to ' + formatBytes(Math.pow(2,key)));
        diagram_values.push(value)
    });

    var ctx = document.getElementById("filesizeHistogramChart").getContext('2d');
    filesizeHistogram = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: diagram_keys,
            datasets: [{
                data: diagram_values,

                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 159, 64, 0.6)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 159, 64, 0.6)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 159, 64, 0.6)',
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255, 159, 64, 1)',
                ],
                    borderWidth: 1,
                    pointStyle: 'circle'
                }]
        },
        options: {
            title: {
                text: 'File Size Histogram',
                display: true,
                fontSize: 18,
                fontStyle: 'normal'
            },
            legend: {
                display: false
            },
            scales: {
                xAxes: [{
                    ticks: {
                        autoSkip: false
                    }
                }],
                yAxes: [{
                    type: 'linear',
                    autoSkip: true,
                    ticks: {
                        min: 0
                    }
                }]
            }
        }
    });
}


function drawBubblechart(responsedata) {

    console.log(responsedata)

    var ctx = document.getElementById("bubbleChart").getContext('2d');

    data = [];

    for(var l = 0; l < responsedata.length; l++) {
        var point_data = [];

        point_data.push({
            x: responsedata[l]['x'],
            y: responsedata[l]['y'],
            r: responsedata[l]['r']
        });
        data.push({
            label: responsedata[l]['dc'] + " Documents",
            data: point_data,
            backgroundColor: 'rgba(255, 99, 132, 0.2)'
        })
    }

    var data = {
            datasets: data
        };

    var options = {
            title: {
                text: 'Bulk of Data Location',
                display: true,
                fontSize: 18,
                fontStyle: 'normal'
            },
			aspectRatio: 1,
			legend: {
                display: false
            },

            elements: {
			    point: {
					radius: function(context) {
                        var index = context.dataIndex;
                        var data = context.dataset.data[index];
                        var size = context.chart.width;
                        var base = data.value / 100;
                        return (size / 24) * base;
					}
				}
			},
            scales: {
			    yAxes: [{
			        scaleLabel: {
			          display: true,
                      labelString: "Level with bulk of data"
                    },
			        ticks: {
			            beginAtZero: true,
                        min: 0
                    }
                }],
                xAxes: [{
			        type: 'linear',
			        scaleLabel: {
			          display: true,
                      labelString: "Tree Level"
                    },
			        ticks: {
			            beginAtZero: true,
                        min: 0
                    }
                }]
            }
        };

    bubblechart = new Chart(ctx, {
        type: 'bubble',
        data: data,
        options: options
    });

    bubbleChart.onclick = function (event) {
        var activePoints = bubblechart.getElementAtEvent(event)
        var uuid = window.location.pathname.split('/')[2];
        console.log(activePoints)

        if (activePoints[0]) {
            var chartData = activePoints[0]['_chart'].config.data;
            var idx = activePoints[0]['_datasetIndex'];
            var value = chartData.datasets[idx].data[0];
            console.log(uuid)
            $.ajax({
                type: 'GET',
                url: uuid ? '/provenance/bubblechart/' + value.x + '/' + value.y + '/' + uuid : '/provenance/bubblechart/' + value.x + '/' + value.y,
                success: function (response) {
                    $('#bubblechartProvenanceModalTitle').empty().append('Provenance inspector<br/><small>Tree Level = ' + value.x + ', Height = ' + value.y + '<br />Collection UUID: ' + uuid + '</small>')
                    $('#bubblechartProvenanceModalBody').empty().append('<p><b>Documents which influenced this bubble:<b/></p>')
                    $.each(response, function (key, value) {
                        $.each(value, function (k, document_id) {
                            console.log('Found ' + document_id)
                            $('#bubblechartProvenanceModalBody').append('<p>'
                              + '<button id="provenance-doc-' + document_id + '" type="button" class="btn btn-default btn-xs">View</button> '
                              + document_id +
                              '</p>')
                                $('#provenance-doc-' + document_id).click(function () {
                                    $.ajax({
                                        type: 'GET',
                                        url: '/provenance/file/' + uuid + '/' + document_id,
                                        dataType: 'json',
                                        success: function (response) {
                                            console.log(response);
                                            response = JSON.parse(response)
                                            $('#inspectFileModalBody').empty().append(
                                                JSON.stringify(response, null, 2)
                                            )
                                            $('#inspectFileModal').modal('show')
                                            $('#bubblechartProvenanceModal').modal('hide')
                                        },
                                        error: function (response) {
                                            console.error(response)

                                        }
                                    });
                                })
                            })
                    });
                    $('#bubblechartProvenanceModal').modal('show')
                },
                error: function (response) {
                    console.error(response)

                }
            });
        }
    }
}


function getOSImagePath(osName){
    dir = 'static/distro/'
    switch (osName.toLowerCase()) {
        case 'ubuntu':
            return dir + 'ubuntu.png';
        case 'debian':
            return dir + 'debian.png';
        case 'windows':
            return dir + 'windows.png';
        case 'darwin':
        case 'mac os':
        default:
            return dir + 'apple.png';
    }
}


/*
 * Third party scripts
 */

// https://stackoverflow.com/questions/15900485/correct-way-to-convert-size-in-bytes-to-kb-mb-gb-in-javascript
function formatBytes(bytes,decimals) {
   if(bytes === 0) return '0 Bytes';
   var k = 1024,
       dm = decimals <= 0 ? 0 : decimals || 2,
       sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
       i = Math.floor(Math.log(bytes) / Math.log(k));
   return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}


function colorize(opaque, context) {
    var value = context.dataset.data[context.dataIndex];
    var x = value.x / 100;
    var y = value.y / 100;
    var r = x < 0 && y < 0 ? 250 : x < 0 ? 150 : y < 0 ? 50 : 0;
    var g = x < 0 && y < 0 ? 0 : x < 0 ? 50 : y < 0 ? 150 : 250;
    var b = x < 0 && y < 0 ? 0 : x > 0 && y > 0 ? 250 : 150;
    var a = opaque ? 1 : 0.5 * value.v / 1000;

    return 'rgba(' + r + ',' + g + ',' + b + ',' + a + ')';
}

