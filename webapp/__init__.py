from flask import Flask
from flask_bootstrap import Bootstrap
from flask_script import Manager, Server

app = Flask(__name__)
app.config['SECRET_KEY'] = '3824b746-665d-5969-8f59-951d7bb114bb'

manager = Manager(app)
bootstrap = Bootstrap(app)

# override the default "runserver" command to set a custom
# host and port (e.g. 0.0.0.0:80)
# manager.add_command("runserver", Server(
#     host=confighelper.WEBSERVER_HOST,
#     port=confighelper.WEBSERVER_PORT
# ))

from webapp.routes import *