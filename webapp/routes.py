import json
import multiprocessing
import os
import shutil
import sys
# import threading

from flask import jsonify, redirect, render_template, request, send_file

from helpers import charthelper, confighelper
from helpers.classes.repositorystatus import RepositoryStatus
from helpers.mongohelper import AbuseType, Client, Properties
from main_server import start
from scraper import add_repository, scrape_urls
from webapp import app, config_setup

# Server startup routine (e.g. loading data or whatever)
already_booted = False
config_name = None # Name of the configuration file to use

mongodb_client = Client()

processes = []


@app.route('/', methods=["GET", "POST"])
def index():
    """
    The index route. If jHound is launched for the first time,
    the user gets redirected to a dedicated "first start" page.
    """
    global already_booted, config_name
    already_booted = True

    # Probably the response of first_start-form
    if request.method == "POST":
        # Check for actual content
        if request.form["config_name"]:
            print("Creating config file...")
            config_dict = {}

            config_dict["config_name"] = request.form["config_name"]
            config_dict["mongo_url"] = request.form["mongo_url"]
            config_dict["mongo_username"] = request.form["mongo_username"]
            config_dict["mongo_password"] = request.form["mongo_password"]
            config_dict["collection_name"] = request.form["collection_name"]

            if len(request.form["mongo_port"]) > 0:
                config_dict["mongo_port"] = int(request.form["mongo_port"])
            else:
                config_dict["mongo_port"] = 27017

            config_setup.create_local_config(config_dict)

            config_name = config_dict["config_name"]
            mongodb_client.update_config_dict(config_name, {}, True)

    if config_name:
        return render_template('index.html')
    else:
        return render_template('first_start.html')


@app.route('/config', methods=['GET', 'POST'])
def config():
    """
    The config page, which lets the user create a new
    or edit an existing config.
    """
    global config_name

    if request.method == "POST":
        # Get values from config form and transform them
        form = request.form.to_dict()

        # Another config has been selected
        if "inputConfig" in form:
            config_name = form["inputConfig"]
            config_setup.update_config_param("config_name", config_name)
            return render_template(
                'config.html',
                config=mongodb_client.get_config(config_name),
                config_names=mongodb_client.get_available_configs(),
                current_config=config_name
            )
        # Create a new empty config
        elif "new_config_name" in form:
            new_config_name = form["new_config_name"]
            mongodb_client.update_config_dict(new_config_name, {}, upsert=True)
            config_setup.update_config_param("config_name", new_config_name)
            return redirect('/config')
        # Update a config
        else:
            master_dict = {}
            for key in ["address", "port", "delay_seconds", "max_threads", "node_update_interval"]:
                if key in form:
                    if len(form[key]) > 0:
                        master_dict[key] = form[key]
            form["master"] = master_dict

            for key in ["address", "port", "delay_seconds", "max_threads", "node_update_interval"]:
                if key in form:
                    del form[key]

            form["nodes"] = []

            mongodb_client.update_config_dict(config_name, form, upsert=True)
            return redirect('/')
    else:
        return render_template(
            'config.html',
            config=mongodb_client.get_config(config_name),
            config_names=mongodb_client.get_available_configs(),
            current_config=config_name
        )


@app.route("/config/create", methods=["POST"])
def create_config():
    """
    Creates a new config document and redirects back to
    the config page.
    """
    form = request.form.to_dict()
    if "config_name" in form:
        name = form["config_name"]
        del form["config_name"]
        mongodb_client.update_config_dict(name, form, upsert=True)
    return redirect("/config")


@app.route('/repositories/')
def repositories():
    """ Returns the repository overview. """
    return render_template('repositories.html')


@app.route('/repositories/add/')
@app.route('/repositories/new/')
def repositories_add():
    """ Returns the page for adding a repository. """
    return render_template('add_repository.html')


@app.route('/repository/<repository_uuid>')
def repository(repository_uuid = None):
    """
    Returns the page for a single repository.
    
    :param repository_uuid: (opt) The UUID of the repository.
    """
    if repository_uuid:
        return render_template('repository.html', uuid=repository_uuid)


@app.route('/result')
@app.route('/result/<repository_uuid>')
def result_page(repository_uuid = None):
    """
    Returns the result page.
    
    :param repository_uuid: (opt) The UUID of the repository.
    If none is given, the results of all repositories will be used.
    """
    result = {
        "name": mongodb_client.fetch_basic_repository_information(repository_uuid)["name"],
        "scraped_links": mongodb_client.get_amount_of_scraped_links(repository_uuid),
        "downloaded_documents": mongodb_client.get_amount_of_downloaded_documents(repository_uuid),
        "download_errors": mongodb_client.get_amount_of_errors(repository_uuid),
        "fatal_errors": 0 # Placeholder
    }

    if not repository_uuid:
        result["name"] = "All Repositories"

    downloaded_percentage = round(result["downloaded_documents"] / result["scraped_links"] * 100, 1)

    return render_template(
        "result.html",
        uuid=repository_uuid,
        result=result,
        downloaded_percentage=downloaded_percentage
    )


@app.route("/addRepository", methods=["POST"])
def add_repository_route():
    """ Adds a new repository. """
    print(request.form)
    if request.method == "POST":
        name = request.form["new_repository_name"]
        url = request.form["new_repository_url"]
        uuid = add_repository(name, url)
        return json.dumps({
            "code": 200,
            "message": "OK",
            "name": name,
            "url": url,
            "uuid": uuid 
            }), 200, {"Content-Type": "application/json"}
    else:
        pass


@app.route("/deleteRepository/<how>/<uuid>", methods=["POST"])
def delete_repository(uuid, how='Full'):
    """
    Deletes a repository from MongoDB and/or the local file system.

    :param uuid: The UUID of the repository.
    :param how: How to delete the repository: 'Full' for deleting it both \
    from the File System and from the Database, 'DB' to delete it from the \
    database only or 'Local' to delete the local files only.
    :returns: JSON-encoded HTTP status code, a message, and the repo UUID.
    """

    if how == 'Full' or how == 'DB':
        was_successful = mongodb_client.delete_repository(uuid)
        if not was_successful:
            return json.dumps({
                "code": 500,
                "message": "Could not be deleted from the database",
                "repo_uuid": uuid
            })

    if how == 'Full' or how == 'Local':
        try:
            with open('../config.ini', 'r') as configfile:
                for line in configfile:
                    if line[0].startswith('storage_directory'):
                        if os.path.exists('storage_directory' + uuid):  # directory does not exist before scraping
                            shutil.rmtree('storage_directory' + uuid)
        except IOError:  # open
            return json.dumps({
                "code": 500,
                "message": "Config file could not be opened or storage directory could not be deleted",
                "repo_uuid": uuid
            })

    return json.dumps({
        "code": 200,
        "message": "OK" if was_successful else "Not Found",
        "repo_uuid": uuid
    })


@app.route("/startScraping/<uuid>")
def start_scraping(uuid):
    """
    Starts the scraping process for a repository.
    
    :param repository_uuid: The UUID of the repository.
    :returns: JSON content, consisting of an HTTP status code and a message.
    """
    mongodb_client.set_repository_status(uuid, RepositoryStatus.SCRAPING)

    process = multiprocessing.Process(target=scrape_urls, args=(uuid,))
    processes.append(process)
    process.start()

    return json.dumps({
        "code": 200,
        "message": "OK"
    })


@app.route("/downloadRepository/<repository_uuid>", methods=["POST"])
def download_repository(repository_uuid):
    """
    Starts the download process of a repository.
    
    :param repository_uuid: The UUID of the repository.
    :returns: JSON content, consisting of an HTTP status code and a message.
    """
    try:
        # downloaded_ir = download interrupted
        # for testing purposes. delete it
        mongodb_client.set_repository_status(repository_uuid, RepositoryStatus.DOWNLOADING)
        process = multiprocessing.Process(
            target=start,
            args=(repository_uuid,),
            kwargs={"mode": "d"}
        )
        processes.append(process)
        process.start()
    # except KeyboardInterrupt:
    #     #  For debug purposes, we want to assume an interrupted download as completed
    #     # special status. should fail later
    #     mongodb_client.set_repository_status(repository_uuid, RepositoryStatus.DOWNLOADED_IR)
    #     return json.dumps({
    #         "code": 200,
    #         "message": "Download finished with interruption"
    #     })
    except Exception:
        return json.dumps({
            "code": 500,
            "message": "Download Process failed"
        })

    return json.dumps({
        "code": 200,
        "message": "Download started"
    })


@app.route("/getRepositories")
def get_repositories():
    """ Gets information about all existing repositories.
    
    :returns: JSON-encoded information about repos.  """
    repositories = mongodb_client.get_repositories()
    row = []
    for repo in repositories:
        row.append({
            "name": repo["name"],
            "url": repo["url"],
            "date_added": repo["date_added"],
            "uuid": repo["uuid"],
            "status": repo["status"]
        })
    return json.dumps(row), 200#, {'Content-Type': 'application/json'}


@app.route('/analyzeRepository/<repository_uuid>', methods=["POST"])
def analyze_repository(repository_uuid):
    """
    Starts the analysis process for a repository.
    
    :param repository_uuid: The UUID of the repository.
    :returns: JSON-encoded HTTP status code and a message. """
    mongodb_client.set_repository_status(repository_uuid, RepositoryStatus.ANALYZING)
    try:
        process = multiprocessing.Process(
            target=start,
            args=(repository_uuid,),
            kwargs={"mode": "a"}
        )
        processes.append(process)
        process.start()
    except Exception:
        pass
    
    return json.dumps({
        "code": 200,
        "message": "Finished"
    })


@app.route('/getBasicRepositoryInformation')
@app.route('/getBasicRepositoryInformation/<repository_uuid>')
def get_basic_repository_information(repository_uuid = None):
    """
    Get basic information about a single repository.
    
    :param repository_uuid: (opt) The UUID of the repository.
    :returns: JSON-encoded information about the repo.
    """
    if repository_uuid:
        try:
            basic_information = mongodb_client.fetch_basic_repository_information(repository_uuid)
            return json.dumps(basic_information), 200, {'Content-Type': 'application/json'}
        except ValueError as ex:
            return json.dumps({
                "code": 500,
                "message": str(ex)
            }), 500, {'Content-Type': 'application/json'}
    else:
        return json.dumps({
            "name": "Results",
            "url": None,
            "date_added": None,
            "status": "analyzed"
        }), 200, {'Content-Type': 'application/json'}


# Returns basic repository information (links_scraped, total_packages) if it was scraped yet
@app.route('/getBasicRepositoryStatistics/<repository_uuid>')
def get_basic_repository_statistics(repository_uuid):
    """
    Get basic statistics about a repository.
    
    :param repository_uuid: The UUID of the repository.
    :returns: The statistics, JSON-encoded.
    """
    if repository_uuid:
        try:
            basic_statistics = mongodb_client.get_basic_repository_statistics(repository_uuid)
            return json.dumps(basic_statistics), 200, {'Content-Type': 'application/json'}
        except ValueError as ex:  # happens e.g. when the repository was not yet crawled
            return json.dumps({
                "code": 500,
                "message": str(ex)
            }), 500, {'Content-Type': 'application/json'}
    else:
        return json.dumps({
            "code": 500,
            "message": "Please pass repository uuid"
        }), 500, {'Content-Type': 'application/json'}


# Returns basic repository information (links_scraped, total_packages) if it was scraped yet
@app.route('/getAdvancedRepositoryStatistics/<repository_uuid>', methods=["POST"])
def get_advanced_repository_statistics(repository_uuid):
    """
    Get advanced statistics about a repository.
    
    :param repository_uuid: The UUID of the repository.
    :returns: The statistics, JSON-encoded.
    """
    if repository_uuid:
        try:
            advanced_statistics = mongodb_client.get_advanced_repository_statistics(repository_uuid)
            return json.dumps(advanced_statistics), 200, {'Content-Type': 'application/json'}
        except ValueError as ex:  # happens e.g. when the repository was not yet analyzed
            return json.dumps({
                "code": 500,
                "message": str(ex)
            }), 500, {'Content-Type': 'application/json'}
    else:
        return json.dumps({
            "code": 500,
            "message": "Please pass repository uuid"
        }), 500, {'Content-Type': 'application/json'}


@app.route('/getScrapedLinksForRepository/<repository_uuid>', methods=["POST"])
def get_scraped_links_for_repository(repository_uuid):
    """
    Get the scraped links from a repository.
    
    :param repository_uuid: The UUID of the repository.
    :returns: The scraped links (or an error), JSON-encoded.
    """
    if repository_uuid:
        try:
            scraped_links = mongodb_client.get_scraped_links(repository_uuid)
            return json.dumps(scraped_links), 200, {'Content-Type': 'application/json'}
        except ValueError as ex:
            return json.dumps({
                "code": 500,
                "message": str(ex)
            }), 500, {'Content-Type': 'application/json'}
    else:
        return json.dumps({
            "code": 500,
            "message": "Please pass repository uuid"
        }), 500, {'Content-Type': 'application/json'}


#
# AJAX Endpoints for Result Page
#


@app.route('/getBasicStatistics', methods=["POST"])
@app.route('/getBasicStatistics/<repository_uuid>', methods=["POST"])
def get_basic_statistics(repository_uuid = None):
    try:
        scraped_links = mongodb_client.get_amount_of_scraped_links(repository_uuid)
        downloaded_documents = mongodb_client.get_amount_of_downloaded_documents(repository_uuid)
        download_errors = mongodb_client.get_amount_of_errors(repository_uuid)
        fatal_errors = 0
    except ValueError:
        return json.dumps({
            "code": 500,
            "message": "Not analyzed yet"
        }), 500, {'Content-Type': 'application/json'}

    return json.dumps({
        "code": 200,
        "scraped_links": scraped_links,
        "downloaded_documents": downloaded_documents,
        "download_errors": download_errors,
        "fatal_errors": fatal_errors
    }), 200, {'Content-Type': 'application/json'}


@app.route('/getFailedDownloads')
@app.route('/getFailedDownloads/<repository_uuid>')
def get_failed_downloads(repository_uuid = None):
    try:
        error_array = mongodb_client.get_failed_downloads(repository_uuid)
    except ValueError:
        return json.dumps({
            "code": 500,
            "message": "Not analyzed yet"
        }), 500, {'Content-Type': 'application/json'}

    return json.dumps(
        error_array
    ), 200, {'Content-Type': 'application/json'}


@app.route('/getOptionalRequiredProperties')
@app.route('/getOptionalRequiredProperties/<repository_uuid>')
def get_optional_and_required_properties(repository_uuid = None):
    try:
        required_properties = mongodb_client.get_property_sum(Properties.REQUIRED_PROPERTIES, repository_uuid)
        optional_properties = mongodb_client.get_property_sum(Properties.OPTIONAL_PROPERTIES, repository_uuid)
    except ValueError:
        return json.dumps({
            "code": 500,
            "message": "Not analyzed yet"
        }), 500, {'Content-Type': 'application/json'}

    return json.dumps({
        "code": 200,
        "required_properties": required_properties,
        "optional_properties": optional_properties
    }), 200, {'Content-Type': 'application/json'}


@app.route('/getDataTypeDistribution')
@app.route('/getDataTypeDistribution/<repository_uuid>')
def get_data_type_distribution(repository_uuid = None):
    try:
        amount_strings = mongodb_client.get_property_sum(Properties.STRING_COUNT, repository_uuid)
        amount_numbers = mongodb_client.get_property_sum(Properties.NUMBER_COUNT, repository_uuid)
        amount_integers = mongodb_client.get_property_sum(Properties.INTEGER_COUNT, repository_uuid)
        amount_arrays = mongodb_client.get_property_sum(Properties.ARRAY_COUNT, repository_uuid)
        amount_booleans = mongodb_client.get_property_sum(Properties.BOOLEAN_COUNT, repository_uuid)
        amount_objects = mongodb_client.get_property_sum(Properties.OBJECT_COUNT, repository_uuid)
        amount_nulls = mongodb_client.get_property_sum(Properties.NULL_COUNT, repository_uuid)
        amount_empty_strings = mongodb_client.get_property_sum(Properties.EMPTY_STRING_COUNT, repository_uuid)
        # amount_non_empty_strings = mongodb_client.get_property_sum(Properties.NON_EMPTY_STRING_COUNT, repository_uuid)
        amount_abusive_numbers = mongodb_client.get_property_sum(Properties.ABUSIVE_NUMBER_COUNT, repository_uuid)
        amount_abusive_booleans = mongodb_client.get_property_sum(Properties.ABUSIVE_BOOLEAN_COUNT, repository_uuid)
    except ValueError:
        return json.dumps({
            "code": 500,
            "message": "Not analyzed yet"
        }), 500, {'Content-Type': 'application/json'}

    return json.dumps({
        "code": 200,
        "strings": amount_strings,
        "numbers": amount_numbers,
        "integers": amount_integers,
        "booleans": amount_booleans,
        "arrays" : amount_arrays,
        "objects": amount_objects,
        "nulls": amount_nulls,
        "empty_strings": amount_empty_strings,
        # "non_empty_strings": amount_non_empty_strings,
        "abusive_numbers": amount_abusive_numbers,
        "abusive_booleans": amount_abusive_booleans
    }), 200, {'Content-Type': 'application/json'}


@app.route('/getFilesizeHistogramValues')
@app.route('/getFilesizeHistogramValues/<repository_uuid>')
def get_filesize_histogram(repository_uuid = None):
    try:
        file_size_array = mongodb_client.get_document_filesizes_poweroftwo(repository_uuid)
    except Exception:
        return json.dumps({
            "code": 500,
            "message": "Not analyzed yet"
        }), 500, {'Content-Type': 'application/json'}

    return json.dumps({
        "histogram_values": file_size_array
    }), 200, {'Content-Type': 'application/json'}


@app.route('/getBubblechartValues')
@app.route('/getBubblechartValues/<repository_uuid>')
def get_bubblechart_values(repository_uuid = None):
    try:
        data = mongodb_client.get_bubblechart_values(repository_uuid)
    except Exception:
        return json.dumps({
            "code": 500,
            "message": "Not analyzed yet"
        }), 500, {'Content-Type': 'application/json'}

    return json.dumps({
        "data": data
    }), 200, {'Content-Type': 'application/json'}


@app.route('/provenance/bubblechart/<x>/<y>/<repository_uuid>')
@app.route('/provenance/bubblechart/<x>/<y>')
def get_provenance_documents(x, y, repository_uuid = None):
    if x and y:
        try:
            data = mongodb_client.get_bubblechart_provenance(x, y, repository_uuid)
        except Exception as e:
            return json.dumps({
                "code": 500,
                "message": "Error fetching documents",
                "context": e
            }), 500, {'Content-Type': 'application/json'}

        return json.dumps({
            "data": data
        }), 200, {'Content-Type': 'application/json'}


@app.route('/provenance/file/<repository_uuid>/<file_uuid>')
def get_provenance_file(repository_uuid, file_uuid):
    if repository_uuid and file_uuid:
        try:
            confighelper.read_config(config_name)
            provenance_file = open(os.path.join(confighelper.STORAGE_DIRECTORY, repository_uuid, file_uuid), "r", encoding="utf8")
            provenance_file_content = provenance_file.read()
            return jsonify(provenance_file_content)
        except IOError:
            return json.dumps({
                "code": 500,
                "message": "File does not exist"
            }), 500, {'Content-Type': 'application/json'}
        finally:
            provenance_file.close()


@app.route('/shutdown')
def shutdown():
    # Get werkzeug.server.shutdown(); store it as shutdown_server()
    shutdown_server = request.environ.get("werkzeug.server.shutdown")
    if shutdown_server:
        shutdown_server()
        return "Shutting down jHound..."
    return "jHound is not running with the Werkzeug server. Shutdown failed."


@app.route("/node/get/<uuid>")
def get_node(uuid):
    if uuid == "all":
        return json.dumps(mongodb_client.get_nodes(config_name), indent=4)
    else:
        return json.dumps(mongodb_client.get_node(config_name, uuid), indent=4)


@app.route("/node/get/<uuid>/status")
def get_node_status(uuid):
    if uuid == "all":
        statuses = mongodb_client.get_node_statuses_all(config_name)
        result = []
        # Transform the result
        for status in statuses:
            truncated_status = status.copy()
            truncated_status["properties"] = truncated_status["properties"][-1]
            result.append(truncated_status)
        return json.dumps(result, indent=4)
    else:
        statuses = mongodb_client.get_node_status(config_name, uuid)
        return json.dumps(statuses, indent=4)


@app.route("/node/get/<uuid>/statuses")
def get_node_statuses(uuid):
    if uuid == "all":
        statuses = mongodb_client.get_node_statuses_all(config_name)
        return json.dumps(statuses, indent=4)
    else:
        statuses = mongodb_client.get_node_statuses(config_name, uuid)
        return json.dumps(statuses, indent=4)


@app.route("/node/add", methods=["POST"])
def add_node():
    form = request.form.to_dict()
    if set(["address", "port"]).issubset(form.keys()):
        name = None
        if "name" in form:
            if len(form["name"]) > 0:
                name = form["name"]

        uuid = mongodb_client.add_node(config_name, form["address"], form["port"], name)
        if uuid:
            response = {
                "code": 201,
                "message": "Created",
                "name": name if name else "",
                "address": form["address"],
                "port": form["port"],
                "uuid": uuid
            }

            return json.dumps(response, indent=4)
    
    return json.dumps({
        "code": 400,
        "message": "Bad Request"
    }, indent=4)


@app.route("/node/remove/<uuid>")
def remove_node(uuid):
    success = mongodb_client.remove_node(config_name, uuid)

    if success:
        return json.dumps({
            "code": 200,
            "message": "OK"
        }, indent=4)

    return json.dumps({
        "code": 400,
        "message": "Bad Request"
    }, indent=4)


@app.route("/node/toggle/<uuid>")
def toggle_node(uuid):
    (success, is_enabled) = mongodb_client.toggle_node(config_name, uuid)

    if success:
        return json.dumps({
            "code": 200,
            "message": "OK",
            "is_enabled": is_enabled
        }, indent=4)
    else:
        return json.dumps({
            "code": 400,
            "message": "Bad Request"
        }, indent=4)


@app.route("/getGeoJsonDocuments")
@app.route("/getGeoJsonDocuments/<uuid>")
def get_geojson_documents(uuid = None):
    docs = mongodb_client.get_geojson_documents(uuid)
    return json.dumps(docs, indent=4), 200, { "Content-Type": "application/json" }


@app.route("/getDocumentsWithStringAbuse/<abuse>")
@app.route("/getDocumentsWithStringAbuse/<abuse>/<uuid>")
def get_documents_with_string_abuse(abuse, uuid = None):
    if abuse == "number":
        abuse = AbuseType.ABUSIVE_NUMBER
    elif abuse == "boolean":
        abuse = AbuseType.ABUSIVE_BOOLEAN
    else:
        return json.dumps({"error": "abuse must either be number or boolean."}), 400, { "Content-Type": "application/json" }

    docs = mongodb_client.get_documents_with_string_abuse(abuse, uuid)
    return json.dumps(docs, indent=4), 200, { "Content-Type": "application/json" }


@app.route("/getErrorTypeCounts")
@app.route("/getErrorTypeCounts/<uuid>")
def get_error_type_counts(uuid = None):
    errors = []
    for error in mongodb_client.get_error_type_counts(uuid):
        errors.append(error)
    return json.dumps({"errors": errors}, indent=4), 200, { "Content-Type": "application/json" }


@app.route("/getErrorContextCounts")
@app.route("/getErrorContextCounts/<uuid>")
def get_error_context_counts(uuid = None):
    errors = []
    for error in mongodb_client.get_error_context_counts(uuid):
        errors.append(error)
    return json.dumps({"errors": errors}, indent=4), 200, { "Content-Type": "application/json" }


@app.route("/image/filesizeHistogram")
@app.route("/image/filesizeHistogram/<repository_uuid>")
def get_image_filesizes(repository_uuid = None):
    """
    Returns a graphic about the filesizes.

    :param repository_uuid: The UUID of the repository containing \
    the files.
    :returns: A PNG file (as attachment).  
    """

    filesizes = mongodb_client.get_document_filesizes_poweroftwo(repository_uuid)
    img_iobytes = charthelper.create_filesize_histogram_iobytes(filesizes)
    return send_file(
        img_iobytes,
        mimetype="image/png",
        as_attachment=True,
        attachment_filename="jhound_filesize_histogram.png"
    )


@app.route("/image/typeDistribution")
@app.route("/image/typeDistribution/<repository_uuid>")
def get_image_type_distribution(repository_uuid = None):
    """
    Returns a graphic about the type distributions.

    :param repository_uuid: The UUID of the repository containing \
    the files.
    :returns: A PNG file (as attachment).  
    """

    strings = mongodb_client.get_property_sum(Properties.STRING_COUNT,repository_uuid)
    numbers = mongodb_client.get_property_sum(Properties.NUMBER_COUNT, repository_uuid)
    integers = mongodb_client.get_property_sum(Properties.INTEGER_COUNT, repository_uuid)
    arrays = mongodb_client.get_property_sum(Properties.ARRAY_COUNT, repository_uuid)
    booleans = mongodb_client.get_property_sum(Properties.BOOLEAN_COUNT, repository_uuid)
    objects = mongodb_client.get_property_sum(Properties.OBJECT_COUNT, repository_uuid)
    nulls = mongodb_client.get_property_sum(Properties.NULL_COUNT, repository_uuid)
    
    img_iobytes = charthelper.create_types_graphic_iobytes(strings, numbers, integers, arrays, booleans, objects, nulls)
    return send_file(
        img_iobytes,
        mimetype="image/png",
        as_attachment=True,
        attachment_filename="jhound_type_distribution.png"
    )


@app.route("/image/propertyFrequencies")
@app.route("/image/propertyFrequencies/<repository_uuid>")
def get_image_property_frequencies(repository_uuid = None):
    """
    Returns a graphic about the property frequencies (required vs. optional).

    :param repository_uuid: The UUID of the repository containing \
    the files.
    :returns: A PNG file (as attachment).  
    """

    required = mongodb_client.get_property_sum(Properties.REQUIRED_PROPERTIES, repository_uuid)
    optional = mongodb_client.get_property_sum(Properties.OPTIONAL_PROPERTIES, repository_uuid)
    
    img_iobytes = charthelper.create_property_frequency_graphic_iobytes(required, optional)
    return send_file(
        img_iobytes,
        mimetype="image/png",
        as_attachment=True,
        attachment_filename="jhound_property_frequencies.png"
    )


@app.route("/image/bubblechart")
@app.route("/image/bubblechart/<repository_uuid>")
def get_image_bubblechart(repository_uuid = None):
    """
    Returns a bubblechart graphic.
    
    :param repository_uuid: The UUID of the repository containing the files.
    :returns: A PNG file (as attachment).  
    """

    values = mongodb_client.get_bubblechart_values(repository_uuid)
    img_iobytes = charthelper.create_bubblechart_graphic_iobytes(values)
    return send_file(
        img_iobytes,
        mimetype="image/png",
        as_attachment=True,
        attachment_filename="jhound_bubblechart.png"
    )


@app.route("/image/treeLevels")
@app.route("/image/treeLevels/<repository_uuid>")
def get_image_tree_levels(repository_uuid = None):
    """
    Returns a histogram consisting of the number of children/nodes on each \
    tree level and returns it as a byte stream.
    
    :param repository_uuid: The UUID of the repository containing \
    the files.
    :returns: A PNG file (as attachment).  
    """

    children_counts = mongodb_client.get_children_counts(repository_uuid)
    img_iobytes = charthelper.create_tree_level_histogram_iobytes(children_counts)
    return send_file(
        img_iobytes,
        mimetype="image/png",
        as_attachment=True,
        attachment_filename="jhound_tree_levels.png"
    )
    

@app.route("/status")
def get_status():
    """ ... """
    # Get repositories that are active -- meaning they are currently being downloaded or analyzed
    repos = mongodb_client.get_repositories()
    active_repos = [repo for repo in repos if repo["status"] in [RepositoryStatus.DOWNLOADING.value, RepositoryStatus.ANALYZING.value]]
    
    repo_list = []
    for repo in active_repos:
        repo_dict = {}
        repo_dict["status"] = repo["status"]
        repo_dict["uuid"] = repo["uuid"]
        
        progress = 0.0
        if repo["status"] == RepositoryStatus.DOWNLOADING.value:
            total_dl_count = len(mongodb_client.get_scraped_links(repo["uuid"]))
            downloaded_count = len(mongodb_client.get_downloaded_files(repo["uuid"]))
            error_count = mongodb_client.get_amount_of_errors(repo["uuid"])
            progress = round(((downloaded_count + error_count) / total_dl_count) * 100, 1)
        elif repo["status"] == RepositoryStatus.ANALYZING.value:
            total_file_count = len(mongodb_client.get_downloaded_files(repo["uuid"]))
            analyzed_file_count = len(mongodb_client.fetch_analyzed_files_from_repository(repo["uuid"]))
            progress = round((analyzed_file_count / total_file_count) * 100, 1)

        repo_dict["progress"] = progress
        repo_list.append(repo_dict)
    
    return str(repo_list)

# Set the config name during initialization
config_name = config_setup.get_config_name()