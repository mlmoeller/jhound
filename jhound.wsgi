#!/mnt/jam_data/data-tools-env/bin/python3

# Please leave this file unchanged
# It is needed for Apache+Passenger-WSGI on jam

import sys

sys.path.insert(0, "/opt/jhound-autodeploy/")
from webapp import app as application
