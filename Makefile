start:
	FLASK_DEBUG=1 python3 run_webapp.py runserver


init:
	python3 run_webapp.py runserver


install:
	pip3 install -r requirements.txt


install-user:
	pip3 install -r requirements.txt --user


test:
	python3 -m unittest discover -v -s tests


# example: make use config=jam
use:
	@bash -c "[ -f \"config.json\" ] && rm ./config.json; exit 0"
	@cp ./config/${config}.json ./config.json
	@echo Using ${config}.json.