webapp package
==============

Submodules
----------

webapp.config\_setup module
---------------------------

.. automodule:: webapp.config_setup
   :members:
   :undoc-members:
   :show-inheritance:

webapp.routes module
--------------------

.. automodule:: webapp.routes
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: webapp
   :members:
   :undoc-members:
   :show-inheritance:
