# jHound - Large-Scale Profiling of Open JSON Data

## Setup

jHound relies on a bunch of 3rd-party Python libraries, so at first you have to install them by using either `make install` as root user or within a virtual environment, or `make install-user` as default user outside of a virtual environment.

jHound also relies on a MongoDB, so please set up a database if you haven't already. There has to be a "jhound" user with "jhound" as password (support for custom user names will be added in a later version).

## Configuration

| __Key__ | __Value type__   | __Explanation__ | __Default value__ |
| ------------ | ------------- | --------------- | --------------- |
| max_links_fetching | Integer | The maximum amount of download links that shall be scraped. | _`(Infinite)`_ |
| max_links_download | Integer  | The maximum amount of files that shall be downloaded. | _`(Infinite)`_ |
| max_filesize | Integer | Given max_filesize = x, files bigger than x MiB will be ignored. | _`(Infinite)`_ |
| skip_resources | Integer | The number of links that shall be skipped during scraping, in other words: the offset. This is useful if the scraping process was interrupted. | `0` |
| timeout | Integer | The number of seconds after a web request fails due to a timeout. | `60` |
| resources_formats | Array of strings | The file formats that shall be scraped, according to CKAN. At the moment, all jHound can handle is JSON. | `["JSON"]` |
| storage_directory | String | The path where the files shall be saved. | `./jHound` |
| mongo_url | String | The address for the MongoDB database. | `localhost` |
| mongo_port | Integer | The port for the MongoDB database to listen to. | `27017` |
| collection_name | String | The name of the MongoDB collection where the information is stored. You can call it whatever you want. | `repositories` |
| master_address | String | The address of the master server. | `localhost` |
| master_port | Integer | The port at which the master server shall listen to. | `9876` |
| delay_seconds | Integer | The number of seconds before the master server sends a message. This is only useful for development purposes. | `0` |
| max_threads | Integer | Given max_threads = x, all nodes will use up to x threads to proceed the files. | `2` |
| nodes | Array of node objects (see below) | The node servers that shall be used. | `[]` |

### Node objects

Every "node object" is a JSON object with the following attributes (please note that address and port have to be set):

| __Key__ | __Value type__   | __Explanation__ | __Example__ |
| ------------ | ------------- | --------------- | --------------- |
| address | String | The address of the node server. | `192.168.2.143` |
| port | Integer | The port of the node server. | `1234` |
| uuid | UUID4 | (Optional) A UUID for the node to identify it. | `ef4b378f-4428-4c82-b8ed-918787863612` |

### Example

```json
{
    "max_filesize": 2048,
    "timeout": 30,
    "storage_directory": "/mnt/jhound_data",
    "mongo_url": "jhound.informatik.uni-rostock.de",
    "collection_name": "repositories_uni",
    "master": {
        "address": "localhost",
        "port": 9876,
        "max_threads": 4
    },
    "nodes": [
        {
            "address": "jh1.some.domain",
            "port": 1234
        },
        {
            "address": "jh2.some.domain",
            "port": 1234
        }
    ]
}
```

## Using the web app

To use the jHound web app, simply execute `make start` in the jHound directory.  
The Flask-based web app will launch locally (`localhost` or `127.0.0.1`, __not__ `0.0.0.0` (yet)) and listens on port 5000.

The jHound project consists of 3 phases: scraping, downloading and analyzing data.

## 1) Scrape data

Every CKAN repository consists of a collection of files. "Scraping" means to retrieve a list of links to all these files.

To scrape a repository, you have to add it to the database first. You can simply use the web app for that or, if you prefer a classical experience, the `scraper.py` script:  
`python3 scraper.py add`  
You will then be prompted to enter the repository URL and a corresponding name. Alternatively, provide these attributes directly:  
`python3 scraper.py add <URL> <NAME>`

The information, as well as a UUID, will be stored in the database. The UUID can be seen as an alias.

To scrape the repository, simply use the web app again (by clicking the "Scrape" button in the "Data Sources" overview) or execute:  
`python3 scraper.py scrape <UUID>`  
If you don't provide a UUID, a list of available repositories will be displayed, and you can choose between them. The repository will then be "scraped".

![Add repository](img/add_repo.png)

## 2) Download data

After scraping a repository, you can download its files. This can either be done via the web app as well, or by using the `scraper.py` script:  
`python3 scraper.py download <UUID>`  
Again, if you don't provide a UUID, a list of repositores to select from will be shown.

Depending on the size of the repository, the download process will probably take some time. Consider setting `MAX_DOWNLOAD_LINKS` to a small(er) value (see above) for testing purposes. (It is planned to parallelize the download process in the future.)

## 3) Analyze data

Due to a bug in the current release, it is not possible to analyze data locally, therefore the web app can't be used for that as well. In order to analyze the data, you have to start a master and at least one node server. The reason for that is that jHound is supposed to be fully parallelized in the future (hence the "Large-Scale" name), starting with its main feature, the data analysis.

So, if you want to analyze a downloaded repository, you have to...

1) Start at least one node server: `python3 node_server.py -p <PORT>` (If you don't provide a port, you will be prompted to enter one)  
2) Add the information for the nodes to your config file (see above).  
3) Start the master server: `python3 master_server.py -r <REPOSITORY UUID> [-q]` with `REPOSITORY_UUID` being the UUID of an existing repository. If you also provide `-q` or `--quiet`, you will see less output.

At the moment, it is recommended that everything runs on the same machine because the nodes are looking for the `STORAGE_DIRECTORY` of your configuration. It is still possible to distribute the node servers if you use a mount directory, though. This will also be improved in the future.

If everything is set up correctly, the analysis will start and the results will be stored in the database.

## 4) Show results

After a repository has been analyzed, you can take a look at the results by using the web app. In the "Data Sources" overview, you should see a green button saying "Show results". Simply click on that button to view the results.

![Repository overview](img/repo_overview.png)

![Repository results](img/repo_results.png)