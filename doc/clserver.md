# Master/node architecture

This project delivers a basic dedicated master/node server functionality for jHound.  
__Python 3.6+ is required.__

## General

Communication is done via the default `socket` library of Python 3.7+. No additional modules are required.  
Instead of handling every message as a new stream of bytes, this application makes use of the [makefile](https://docs.python.org/3.6/library/socket.html#socket.socket.makefile) approach which returns a file-like object that can be written into and read from, making bidirectional communication way easier.

## Node

To start a new node server, launch `node_server.py` with an available port number as `-p`/`--port` argument (required).  
Example:  
```bash
python3 node_server.py --port 1337
```

The node waits for incoming connections from a master server, sending the filename of the file that shall be analyzed. It then analyzes the specific file and returns the result to the master.

## Master

To start the master server, simply launch `master.py`. Unlike the node application, the master application has some optional arguments:

| __Shorttag__ | __Longtag__   | __Explanation__ |
| ------------ | ------------- | --------------- |
| -c           | --config      | _(optional)_ The path to the config file to be used. The default path is `./config.json`.|
| -r           | -- repository | The UUID of the repository that shall be analyzed. |

The master will send available file names in a _round robin_ topology. If a node refuses the connection, the server will be ignored as long as the master is running.

## Configuration

The project uses a configuration file to configure the master server and provide a list of the nodes. It could look like this:

```json
{
    "master": {
        "address": "123.123.123.123",
        "port": 9876,
        "directory_path": "/path/to/the/files/",
        "delay_seconds": 0
    },
    "nodes": [
        {
            "address": "321.321.321.1",
            "port": 1234
        },
        {
            "address": "321.321.321.2",
            "port": 1234,
            "uuid": "ef4b378f-4428-4c82-b8ed-918787863612"
        }
    ]
}
```

* "master" consists of information about the master server.
  * "address": The address of the server.
  * "port": The port the server listens on.
  * "directory_path": The path to the directory containing the files to be analyzed.
  * "delay_seconds": The delay between new connections made by the master.
* "node" is a list that contains the available nodes. Each node is represented by...
  * "address": The address of the node.
  * "port": The port of the node.
  * "uuid" is a UUID (RFC 4122) in order to identify the node. __(TODO: more details)__