# CKAN sraper

## General

...

## Available commands

### Adding a new repository

`python3 scraper.py add <url> <name>`

URL: The url for the CKAN repository, e.g. "data.humdata.org".  
Name: The display name for the new repository. This can be anything, e.g. "Humanitarian Data Exchange Repo".

If you don't enter a URL and a name, the scraper prompts you to enter both. If everything went fine, the UUID of the created repository will be printed.

### Deleting an existing repository

`python3 scraper.py delete <uuid>`

### Scraping and downloading

`python3 scraper.py [scrape | download] <uuid>`

If you don't provide a UUID, a list of all available repositories that can be scraped/downloaded will be shown. You then have to manually select one of them. 