# JSON analytics

The point of this tool is to provide statistics on the structure of JSON documents by dissecting these same documents for interesting traits.

This process is, for the moment, composed of two intertwined phases:

- extracting meta information about a document, such as the family relationship of each node, and pulling as much composite information from the said document as possible
- Compute statistics base on the numbers gathered in the first phases

These two phases are represented respectively by the two files :

- node_extractor.py
- statistics.py

## Setup

This tool requires the libraries *ijson* in order to parse huge JSON datasets efficiently and *matplotlib* for graph plotting, and runs under *python3*:
In order to avoid spoiling the rest of the computer with python packages, we suggest creating a virtual environment for this tool

```[bash]
> virtualenv --no-site-packages <name>
> source <name>/bin/activate
```

At this point, you are in a separate environment and only absolute necessary packages are installed.
You need to install the following packages in order to make the project work:

```[bash]
(env)> pip3 install -r <data-tools-dir>/requirements.txt
```

## Usage

The entirety of the tool is available via the file *document_analytics_script.py*:

```[bash]
python3 document_analytics_script.py -h
python3 document_analytics_script.py -f -dmyfile.json -s
python3 document_analytics_script.py -b -d./my-datasets
python3 document_analytics_script.py -b -d../../data-storage/AVDATA/datasets -nusdata
```
