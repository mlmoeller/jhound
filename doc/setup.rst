================================================
Setup
================================================

jHound relies on a bunch of 3rd-party Python libraries, so at first you have 
to install them by using either `make install` as root user or within a 
virtual environment, or `make install-user` as default user outside of a 
virtual environment.

jHound also relies on a MongoDB, so please set up a database if you haven't 
already. There has to be a "jhound" user with "jhound" as password (support 
for custom user names will be added in a later version).
(Note: Has been added since)

...