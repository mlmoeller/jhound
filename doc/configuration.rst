================================================
Configuration
================================================

==================  =====================   =========================== ===========================
**Key**             **Value type**          **Explanation**             **Default value**
==================  =====================   =========================== ===========================
max_links_fetching  Integer                 The maximum amount of       ``(Infinite)``
                                            download links that shall
                                            be scraped.     
max_links_download  Integer                 The maximum amount of files ``(Infinite)``
                                            that shall be downloaded.
max_filesize        Integer                 Given max_filesize = x,     ``(Infinite)``
                                            files bigger than x MiB
                                            will be ignored.
skip_resources      Integer                 The number of links that    ``0``
                                            shall be skipped during
                                            scraping, in other words:
                                            the offset. This is useful
                                            if the scraping process was
                                            interrupted.
timeout             Integer                 The number of seconds after ``60``
                                            a web request fails due to
                                            a timeout.
storage_directory   String                  The path where the files    ``./jHound``
                                            shall be saved.
mongo_url           String                  The address for the MongoDB ``localhost``
                                            database.
mongo_port          Integer                 The port for the MongoDB    ``27017``
                                            database to listen to.
collection_name     String                  The name of the MongoDB     ``repositories``
                                            collection where the
                                            information is stored.
                                            You can call it whatever
                                            you want.
master_address      String                  The address of the master   ``localhost``
                                            server.
master_port         Integer                 The port at which the       ``9876``
                                            master server shall listen
                                            to.
delay_seconds       Integer                 The number of seconds       ``0``
                                            before the master server
                                            sends a message. This is
                                            only useful for development
                                            purposes.
max_threads         Integer                 Given max_threads = x, all  ``2``
                                            nodes will use up to x
                                            threads to proceed the
                                            files.
nodes               Array of node objects   The node servers that       ``[]``
                                            shall be used.
==================  =====================   =========================== ===========================


Node objects
------------

Every "node object" is a JSON object with the following attributes (please note that address and port have to be set):

=======     ==============      =============================== ========================================
**Key**     **Value type**      **Explanation**                 **Example**
=======     ==============      =============================== ========================================
address     String              The address of the node server. ``192.168.2.143``
port        Integer             The port of the node server.    ``1234``
uuid        UUID4               (Optional) A UUID for the node  ``ef4b378f-4428-4c82-b8ed-918787863612``
                                to identify it.
=======     ==============      =============================== ========================================


Example
-------

.. code-block:: JSON
    :linenos:

    {
        "max_filesize": 2048,
        "timeout": 30,
        "storage_directory": "/mnt/jhound_data",
        "mongo_url": "jhound.informatik.uni-rostock.de",
        "collection_name": "repositories_uni",
        "master": {
            "address": "localhost",
            "port": 9876,
            "max_threads": 4
        },
        "nodes": [
            {
                "address": "jh1.some.domain",
                "port": 1234
            },
            {
                "address": "jh2.some.domain",
                "port": 1234
            }
        ]
    }