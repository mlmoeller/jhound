import json

import ijson

obj = {
    "id": "abcdef",
    "nr": 42,
    "roles": ["admin", "moderator"],
    "badges": [
        { "gold": 2 },
        { "silver": 13 }
    ]
}

json_string = json.dumps(obj, indent=4)
print(json_string + "\n")

parser = ijson.parse(json_string)
for (prefix, event, value) in parser:
    print(f"Event: {event}; Prefix: {prefix if prefix else '<empty string>'}; Value: {value}")
