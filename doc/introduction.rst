===============================================================================
Introduction / About jHound
===============================================================================

*jHound* has been developed for data exploration of NoSQL datasets. It scrapes
links from open data repositories, downloads the NoSQL datasets, parses them,
and derives several metrics from the data. *jHound* is written in Python 3
(at least 3.7.0) as a distributed system with a map-reduce process using
multiple analysis servers referred to as *nodes*. However, *jHound* can be
applied for exploring all kinds of available JSON documents.

The baseline of *jHound* are CKAN repositories. CKAN [#]_, the *Comprehensive
Knowledge Archive Network*, is a project in which any kind of public data can
be stored and is often used by governments to provide access to public
information. A CKAN repository consists of packages, which, in themselves,
consist of URLs to documents, called resources. Every repository, every package
and every resource comes with some metadata such as the total amount of
elements, data types, and more. For all data sources, the *jHound* analysis
workflow consists of the following three steps: retrieval, analysis, and
visualization.

.. [#] https://ckan.org/