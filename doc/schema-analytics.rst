schema\-analytics package
=========================

Submodules
----------

schema\-analytics.JSONSchemaParser module
-----------------------------------------

.. automodule:: schema-analytics.JSONSchemaParser
   :members:
   :undoc-members:
   :show-inheritance:

schema\-analytics.custom\_semantics module
------------------------------------------

.. automodule:: schema-analytics.custom_semantics
   :members:
   :undoc-members:
   :show-inheritance:

schema\-analytics.custom\_semantics\_ignored module
---------------------------------------------------

.. automodule:: schema-analytics.custom_semantics_ignored
   :members:
   :undoc-members:
   :show-inheritance:

schema\-analytics.schema\_analytics\_script module
--------------------------------------------------

.. automodule:: schema-analytics.schema_analytics_script
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: schema-analytics
   :members:
   :undoc-members:
   :show-inheritance:
