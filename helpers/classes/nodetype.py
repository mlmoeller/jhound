from enum import IntEnum

class NodeType(IntEnum):
    STRING = 1  
    INTEGER = 2
    NUMBER = 3
    BOOLEAN = 4
    ARRAY = 5
    OBJECT = 6
    UNNAMED_OBJECT = 7 # unused
    NULL = 8
    VOID = 9 # for buggy / strange values encountered
    PROPERTY = 10
    ARRAY_ITEM = 11 # unused
    EMPTY_STRING = 12
    NON_EMPTY_STRING = 13