from enum import Enum

class RepositoryStatus(Enum):
    ADDED = "added"
    SCRAPING = "scraping"
    SCRAPED = "scraped"
    DOWNLOADING = "downloading"
    DOWNLOADED = "downloaded"
    DOWNLOADED_IR = "downloaded_ir"  # Interrupted download
    ANALYZING = "analyzing"
    ANALYZED = "analyzed"