class ArrayProperties:
    def __init__(self):
        self.array = None
        self.array_properties = {
            "object": 0,
            "simple": 0,
            "mixed": 0,
            "only_array": 0,
            "empty": 0,
            "nested": [0] * 10
        }

    def nest(self):
        if self.array and self.array["obj_nest"] == 0:
            self.array["arr_nest"] += 1
        self.array = {"arr_nest": 0, "obj_nest": 0,
                      "simple": 0, "object": 0, "out": self.array}

    def un_nest(self):
        if self.array:
            self.array = self.array["out"]

    def add_simple(self):
        if self.array["obj_nest"] == 0:
            self.array["simple"] += 1

    def add_object(self):
        if self.array["obj_nest"] == 0:
            self.array["object"] += 1
        self.array["obj_nest"] += 1

    def end_of_object(self):
        """ needed to count only the elements seen at the very level of the array"""
        self.array["obj_nest"] -= 1

    def analysis(self):
        s, o, n = self.array["simple"], self.array["object"], self.array["arr_nest"]
        if s == 0 and o == 0 and n != 0:
            self.array_properties["only_array"] += 1
        elif s == 0 and o != 0 and n == 0:
            self.array_properties["object"] += 1
        elif s != 0 and o == 0 and n == 0:
            self.array_properties["simple"] += 1
        elif s == 0 and o == 0 and n == 0:
            self.array_properties["empty"] += 1
        else:
            self.array_properties["mixed"] += 1

        if n > 9:
            self.array_properties["nested"][9] += 1
        else:
            self.array_properties["nested"][n] += 1

    def is_in_array(self):
        return self.array is not None

    def get_results(self):
        return ([
                self.array_properties["simple"],
                self.array_properties["object"],
                self.array_properties["only_array"],
                self.array_properties["mixed"],
                self.array_properties["empty"]
                ],
                self.array_properties["nested"]
                )

    def get_amount_simple(self):
        return self.array_properties["simple"]

    def get_amount_object(self):
        return self.array_properties["object"]

    def get_amount_only_array(self):
        return self.array_properties["only_array"]

    def get_amount_mixed(self):
        return self.array_properties["mixed"]

    def get_amount_empty(self):
        return self.array_properties["empty"]

    def get_amount_nested(self):
        return self.array_properties["nested"]
