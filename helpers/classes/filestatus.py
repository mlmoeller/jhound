from enum import Enum

class FileStatus(Enum):
    NULL = "null"
    WIP = "wip"
    FINISHED = "finished"