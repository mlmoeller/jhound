from helpers.classes.nodetype import NodeType

class StatisticsBuilder:
    NODE_TYPES = [NodeType.STRING, NodeType.NUMBER, NodeType.INTEGER, NodeType.BOOLEAN,
                  NodeType.ARRAY, NodeType.OBJECT, NodeType.NULL]

    class TypeCountProperties:
        """ Contains the variables used to count the occurences of types. """

        def __init__(self):
            self.STRING = 0
            self.NUMBER = 0
            self.INTEGER = 0
            self.BOOLEAN = 0
            self.ARRAY = 0
            self.OBJECT = 0
            self.NULL = 0
            self.EMPTY_STRING = 0
            self.NON_EMPTY_STRING = 0

            self.ABUSED_STRING_NUMBER = 0
            self.ABUSED_STRING_BOOLEAN = 0


        def compact(self):
            """ Returns tuple ready to use elsewhere in the scripts. """

            return (
                self.STRING, self.NUMBER, self.INTEGER,
                self.BOOLEAN, self.ARRAY, self.OBJECT,
                self.NULL, self.EMPTY_STRING, self.NON_EMPTY_STRING,
                self.ABUSED_STRING_NUMBER, self.ABUSED_STRING_BOOLEAN 
            )


        def get_amount_of_strings(self):
            return self.STRING


        def get_amount_of_numbers(self):
            return self.NUMBER


        def get_amount_of_integers(self):
            return self.INTEGER


        def get_amount_of_booleans(self):
            return self.BOOLEAN


        def get_amount_of_arrays(self):
            return self.ARRAY


        def get_amount_of_objects(self):
            return self.OBJECT


        def get_amount_of_nulls(self):
            return self.NULL


        def get_amount_of_empty_strings(self):
            return self.EMPTY_STRING


        def get_amount_of_non_empty_strings(self):
            return self.NON_EMPTY_STRING


        def get_amount_of_abusive_numbers(self):
            return self.ABUSED_STRING_NUMBER


        def get_amount_of_abusive_booleans(self):
            return self.ABUSED_STRING_BOOLEAN


        def get_amounts(self):
            return {
                "string": self.get_amount_of_strings(),
                "number": self.get_amount_of_numbers(),
                "integer": self.get_amount_of_integers(),
                "boolean": self.get_amount_of_booleans(),
                "array": self.get_amount_of_arrays(),
                "object": self.get_amount_of_objects(),
                "null": self.get_amount_of_nulls(),
                "empty_string": self.get_amount_of_empty_strings(),
                "non_empty_string": self.get_amount_of_non_empty_strings(),
                "abusive_number": self.get_amount_of_abusive_numbers(),
                "abusive_boolean": self.get_amount_of_abusive_booleans()
            }


    class MultResProperties:
        """ Contains the variables used to count the occurences of multiplicity keywords in schemas.
        For reasons explained in the paper, these variables have no meaning for JSON documents. """

        def __init__(self):
            self.ALLOF, self.ANYOF, self.ONEOF = 0, 0, 0
            

        def compact(self):
            """ Returns tuple ready to use elsewhere in the scripts. """
            return self.ALLOF, self.ANYOF, self.ONEOF


        def get_allof(self):
            return self.ALLOF


        def get_anyof(self):
            return self.ANYOF


        def get_oneof(self):
            return self.ONEOF


    class RequiredProperties:
        """ Contains the variables used to count the number of required properties within a document. """

        def __init__(self):
            self.N_REQUIRED, self.N_PROPS = 0, 0
            self.REQUIRED_NAMES, self.PROP_NAMES = [], []


        def compact(self):
            """ Returns tuple reay to use elsewhere in the sripts. """
            return (self.N_REQUIRED, self.N_PROPS)


    def __init__(self):
        self.tcp = StatisticsBuilder.TypeCountProperties()
        self.mrp = StatisticsBuilder.MultResProperties()
        self.rp = StatisticsBuilder.RequiredProperties()


    def __str__(self):
        return f"TCP: {self.tcp.compact()}\n" + \
            f"MRP: {self.mrp.compact()}\n" + \
                f"RP: {self.rp.compact()}"


    def req_analyze_co(self, root_custom_objects):
        """ Computes the stats related to req/opt property characteristics. """
        for _, v in root_custom_objects.items():  # objects
            for _, tv in v["types"].items():  # instances
                parent_occurence = tv["occ"]
                for child_name, child_occurence in tv["children"].items():
                    if child_name not in StatisticsBuilder.NODE_TYPES:
                        if child_occurence == parent_occurence and child_name not in self.rp.REQUIRED_NAMES:
                            self.rp.N_REQUIRED += 1
                            self.rp.REQUIRED_NAMES.append(child_name)
                        if child_name not in self.rp.PROP_NAMES:
                            self.rp.N_PROPS += 1
                            self.rp.PROP_NAMES.append(child_name)


    def full_analysis(self, custom_objects):
        self.req_analyze_co(custom_objects)