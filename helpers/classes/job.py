import time
from enum import Enum

class JobType(Enum):
    ANALYSIS = "analysis"
    DOWNLOAD = "download"


class Job:
    def __init__(self, descriptor, pid = None):
        self.descriptor = descriptor
        self.timestamp = time.time()
        self.type = None
        self.pid = pid
        self.exitcode = None

    def get_descriptor(self):
        return self.descriptor

    def get_timestamp(self):
        return self.timestamp

    def passed_time(self):
        return time.time() - self.timestamp

    def __str__(self):
        return f"{self.type.value.upper()} - {self.descriptor} - {self.passed_time()} - {self.exitcode}" 


class AnalysisJob(Job):
    def __init__(self, descriptor, pid = None):
        super().__init__(descriptor, pid)
        self.type = JobType.ANALYSIS


class DownloadJob(Job):
    def __init__(self, descriptor, pid = None):
        super().__init__(descriptor, pid)
        self.type = JobType.DOWNLOAD