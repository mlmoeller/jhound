from enum import Enum

class ConfigKeys(Enum):
    # Scraper
    MAX_LINKS_DOWNLOAD = "max_links_download"
    MAX_LINKS_FETCHING = "max_links_fetching"
    MAX_FILESIZE = "max_filesize"
    SKIP_RESOURCES = "skip_resources"
    TIMEOUT = "timeout"
    STORAGE_DIRECTORY = "storage_directory"

    # MongoDB
    COLLECTION_NAME = "collection_name"

    # Master/Nodes
    MASTER_ADDRESS = "master_address"
    MASTER_PORT = "master_port"
    DELAY_SECONDS = "delay_seconds"
    MAX_THREADS = "max_threads"
    UPDATE_NODE_INTERVAL = "update_node_interval"