# This is a document containing every self-defined exception
# that can be thrown.

class InvalidConfigurationError(Exception):
    pass