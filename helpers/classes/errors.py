from enum import Enum

class Errors(Enum):
    """ This is just a pseudo-abstract class. """
    pass


class ScrapingErrors(Errors):
    """ Errors that might occur while scraping or downloading
    a repository. """
    TIMEOUT         =   "Timeout"
    SSL             =   "SSL"
    STATUSCODE      =   "Bad status code"
    FILETOOLARGE    =   "File too large"
    CONTENTTYPE     =   "Content-Type"
    MIMETYPE        =   "MimeType"
    PARSING         =   "Parsing after download"
    PROTOCOL        =   "Unsupported protocol"
    CONNECTION      =   "Connection error"
    UNKNOWN         =   "Unknown error"
    CKAN            =   "CKAN error"


class AnalysisErrors(Errors):
    """ Errors that might occur during the data analysis. """
    UNKNOWN             =   "Unknown error"