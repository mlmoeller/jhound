import uuid as ulib # because there's an attribute called "uuid"

class Node():
    def __init__(self, address, port, uuid = str(ulib.uuid4()), enabled = True, storage_directory = None):
        """ Constructor. Required: address, port """
        self.address = address
        self.port = port
        self.uuid = uuid if uuid else str(ulib.uuid4())
        self.files = []
        self.enabled = enabled
        self.uuid_sent = False
        self.storage_directory = storage_directory


    def __str__(self):
        """ String representation of the node.

            Format: <address>:<port> """
        return f"{self.address}:{self.port}"

    
    def get_address(self):
        """Returns the address of the node. """
        return self.address


    def get_port(self):
        """ Returns the port number of the node. """
        return self.port


    def get_semaphore(self):
        """ Returns the semaphore value of the node. """
        return len(self.files)


    def get_uuid(self):
        """ Returns the UUID of the node. """
        return self.uuid


    def get_connection_tuple(self):
        """ Returns a tuple containing the address and the port. """
        return (self.address, int(self.port))


    def get_files(self):
        """ Returns the UUID of the file the node currently works on. """
        return self.files


    def add_file(self, file):
        """ Sets the UUID of the file the node currently works on. """
        self.files.append(file)


    def remove_file(self, file):
        """ Removes a file from the file list.
        
        :returns: True if everything went fine, else False. """
        try:
            self.files.remove(file)
            return True
        except ValueError:
            return False


    def is_enabled(self):
        return self.enabled


    def toggle(self):
        self.enabled = not self.enabled


    def register_sent_uuid(self):
        self.uuid_sent = True


    def knows_uuid(self):
        return self.uuid_sent


    def get_storage_directory(self):
        return self.storage_directory