import io
import math
from matplotlib import pyplot as plt

plt.style.use("ggplot")

def format_bytes(amount_bytes, decimals):
    """ Prettifies an amount of bytes.

    :param amount_bytes: The number of bytes.
    :returns: The number of bytes, but human-readable. """
    
    if amount_bytes == 0:
        return "0 Bytes"
    k = 1024
    dm = 0 if decimals <= 0 else (decimals or 2)
    sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB"]
    i = math.floor(math.log(amount_bytes) / math.log(k))
    return f"{float(round(amount_bytes / k**i, dm))} {sizes[i]}"


def reset_plot(original_function):
    """ Decorator for resetting the plot before creating a new one.
    Usage: @reset_plot """
    def wrapper(*args, **kwargs):
        plt.clf()
        return original_function(*args, **kwargs)
    return wrapper


@reset_plot
def create_property_frequency_graphic_iobytes(required, optional):
    """ Draws a pie chart consisting of required and optional
    parameter counts and returns it as a byte stream.

    :param required: The amount of required properties.
    :param optional: The amount of optional properties.
    :returns: The image as IO stream. """

    # plt.title("property frequency")
    plt.pie([required, optional], labels=["always present", "not always present"])
    # plt.legend()
    plt.tight_layout()

    pic_stream = io.BytesIO()
    plt.savefig(pic_stream, format="png", dpi=300, bbox_inches="tight")
    pic_stream.seek(0)
    return pic_stream


# def create_types_graphic_base64(strings, numbers, integers, booleans, arrays, objects, nulls, empty_strings = 0, non_empty_strings = 0):
@reset_plot
def create_types_graphic_iobytes(strings, numbers, integers, booleans, arrays, objects, nulls):
    """ Draws a pie chart consisting of type counts and returns it as
    a byte stream.

    :param strings: The amount of strings.
    :param numbers: The amount of numbers.
    :param integers: The amount of integers.
    :param booleans: The amount of booleans.
    :param arrays: The amount of arrays.
    :param objects: The amount of objects.
    :param nulls: The amount of null values.
    :returns: The image as IO stream. """

    # plt.title("data type distribution")
    plt.pie(
        [strings, numbers, integers, booleans, arrays, objects, nulls],
        labels=["strings", "numbers", "integers", "booleans", "arrays", "objects", "nulls"]
    )
    # plt.legend(loc="lower center")
    plt.tight_layout()

    pic_stream = io.BytesIO()
    plt.savefig(pic_stream, format="png", dpi=300, bbox_inches="tight")
    pic_stream.seek(0)
    return pic_stream


@reset_plot
def create_bubblechart_graphic_iobytes(values):
    """ Draws a bubble chart showing the data distribution
    and returns it as a byte stream.

    :param values: ...
    :returns: The image as IO stream. """

    x, y, r, dcs = [], [], [], []
    for value in values:
        x.append(value["x"])
        y.append(value["y"])
        r.append(float(value["r"])**2)
        dcs.append(value["dc"])
        plt.text(value["x"], value["y"], str(value["dc"]), size=11, horizontalalignment="center")

    plt.xticks(range(math.floor(min(x)), math.ceil(max(x))+2))
    plt.yticks(range(math.floor(min(y)), math.ceil(max(y))+2))
    plt.xlabel("Tree level")
    plt.ylabel("Level with bulk of data")
    
    plt.scatter(x, y, r, c=x, cmap="winter", alpha=0.5)
    plt.tight_layout()

    pic_stream = io.BytesIO()
    plt.savefig(pic_stream, format="png", dpi=300, bbox_inches="tight")
    pic_stream.seek(0)
    return pic_stream


@reset_plot
def create_filesize_histogram_iobytes(filesizes):
    """ Draws a histogram consisting of the file sizes
    and returns it as a byte stream.

    :param filesizes: ...
    :returns: The image as IO stream. """

    diagram_keys = []
    diagram_values = []
    for key, value in filesizes.items():
        diagram_keys.append(format_bytes(2**(key - 1), 0) + " to " + format_bytes(2**key, 0))
        diagram_values.append(value)

    plt.xticks(rotation=90)
    plt.xlabel("File size")
    plt.ylabel("Number of documents")
    plt.bar(diagram_keys, diagram_values)
    plt.tight_layout()

    # Add absolute values
    for i, value in enumerate(diagram_values):
        plt.text(diagram_keys[i], value - 2 if value >= 5 else value + 1, str(value), ha="center")

    pic_stream = io.BytesIO()
    plt.savefig(pic_stream, format="png", dpi=300, bbox_inches="tight")
    pic_stream.seek(0)
    return pic_stream


@reset_plot
def create_tree_level_histogram_iobytes(children_counts):
    """ Draws a histogram consisting of the number of children/nodes
    on each tree level and returns it as a byte stream.
    
    :param filesizes: An (int, int) dictionary where the keys are the tree
    levels and the values are the number of nodes on that level.
    :returns: The image as IO stream. """

    plt.xlabel("Tree level")
    plt.ylabel("Number of nodes")
    plt.bar(children_counts.keys(), children_counts.values())
    plt.tight_layout()

    # Add absolute values
    for i, value in enumerate(children_counts.values()):
        plt.text(i, value - 2 if value >= 5 else value + 1, str(value), ha="center")

    pic_stream = io.BytesIO()
    plt.savefig(pic_stream, format="png", dpi=300, bbox_inches="tight")
    pic_stream.seek(0)
    return pic_stream