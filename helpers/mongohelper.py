import datetime
import json
import math
import os
import uuid
from collections import OrderedDict, defaultdict
from enum import Enum

import pymongo
from pymongo.errors import OperationFailure

from helpers.classes.config_keys import ConfigKeys
from helpers.classes.exceptions import InvalidConfigurationError
from helpers.classes.repositorystatus import RepositoryStatus


class Properties(Enum):
    REQUIRED_PROPERTIES     =   "$files.analysis_properties.required_properties"
    OPTIONAL_PROPERTIES     =   "$files.analysis_properties.optional_properties"
    STRING_COUNT            =   "$files.analysis_properties.type_counts.string"
    NUMBER_COUNT            =   "$files.analysis_properties.type_counts.number"
    INTEGER_COUNT           =   "$files.analysis_properties.type_counts.integer"
    BOOLEAN_COUNT           =   "$files.analysis_properties.type_counts.boolean"
    ARRAY_COUNT             =   "$files.analysis_properties.type_counts.array"
    OBJECT_COUNT            =   "$files.analysis_properties.type_counts.object"
    NULL_COUNT              =   "$files.analysis_properties.type_counts.null"
    EMPTY_STRING_COUNT      =   "$files.analysis_properties.string_abuse.empty_string"
    ABUSIVE_NUMBER_COUNT    =   "$files.analysis_properties.string_abuse.abusive_number"
    ABUSIVE_BOOLEAN_COUNT   =   "$files.analysis_properties.string_abuse.abusive_boolean"


class DurationType(Enum):
    ANALYSIS_DURATION = "analysis_durations"
    DOWNLOAD_DURATION = "download_durations"


class AbuseType(Enum):
    ABUSIVE_NUMBER = "abusive_number"
    ABUSIVE_BOOLEAN = "abusive_boolean"
    EMPTY_STRING = "empty_string"


class Client():
    def __init__(self):
        self._client = None
        self._database = None
        self.CONFIG_NAME = None
        self.COLLECTION_NAME = None
        
        self.MONGO_URL = None
        self.MONGO_PORT = None
        self.MONGO_USERNAME = None
        self.MONGO_PASSWORD = None


    def read_local_config(self):
        """ Reads the LOCAL config file and updates the used values. """
        # Load local config file (including MongoDB configuration)
        parent_directory = __file__[:-len(os.path.basename(__file__))].replace("helpers/", "").replace("helpers\\", "")

        if os.path.exists(parent_directory + "config.json"):
            local_config_file = open(parent_directory + "config.json", "r", encoding="utf8")
            local_config = json.load(local_config_file)
            local_config_file.close()

            if "config_name" in local_config:
                self.CONFIG_NAME = local_config["config_name"]

            if "mongo_url" in local_config:
                self.MONGO_URL = local_config["mongo_url"]

            if "mongo_port" in local_config:
                self.MONGO_PORT = int(local_config["mongo_port"])

            if "mongo_username" in local_config:
                self.MONGO_USERNAME = local_config["mongo_username"]

            if "mongo_password" in local_config:
                self.MONGO_PASSWORD = local_config["mongo_password"]

            if "collection_name" in local_config:
                self.COLLECTION_NAME = local_config["collection_name"]


    def _connect(self):
        """ Establishes a connection to the database if none exists yet. """

        if not self._client:
            if not (self.MONGO_URL and self.MONGO_USERNAME and self.MONGO_PASSWORD and self.COLLECTION_NAME):
                self.read_local_config()
                if not (self.MONGO_URL and self.MONGO_USERNAME and self.MONGO_PASSWORD and self.COLLECTION_NAME):
                    raise InvalidConfigurationError("The MongoDB configuration is invalid. Please check it again.")

            self._client = pymongo.MongoClient(
                host=f"{self.MONGO_URL}:{self.MONGO_PORT}",
                username=self.MONGO_USERNAME,
                password=self.MONGO_PASSWORD,
                authSource="jhound",
                authMechanism='SCRAM-SHA-256'
            )

            self._database = self._client.jhound[self.COLLECTION_NAME]


    def _disconnect(self):
        """ Disconnects from the database. """
        if self._client:
            try:
                self._client.close()
            except Exception:
                pass
            # Reset to make a next _connect() call possible
            self._client = None
            self._database = None


    def add_repository(self, name, url):
        """ Stores a new repository.

        :param name: The name of the repository (e.g. European Data Portal).
        :param url: The url to the repository (e.g. data.humdata.org).
        :returns: The (created) UUID of the repository. """
        self._connect()
        entity = {
            "type": "repository",
            "uuid": str(uuid.uuid4()),
            "name": name,
            "url": url,
            "date_added": str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")),
            "status": "added"
        }
        self._database.insert_one(entity)
        return entity.get("uuid")


    def delete_repository(self, uuid):
        """ Deletes a stored repository.

        :param uuid: The internal uuid of the repository.
        :returns: True if deletion was successful, otherwise False. """
        self._connect()
        result = self._database.delete_one(
            { "uuid": { "$regex": f"^{uuid}" } }
        )
        return result.deleted_count > 0


    def add_repository_stats(self, repository_uuid, total_packages, links_scraped):
        self._connect()
        if repository_uuid:
            condition = {
                "uuid": repository_uuid
            }
            properties = {
                "total_packages": total_packages,
                "links_scraped": links_scraped,
                "status": "scraped"
            }
            self._database.update_one(condition, {'$set': properties})
            return True


    def add_scraped_urls(self, repository_uuid, urls):
        self._connect()
        if repository_uuid:
            condition = {
                "uuid": repository_uuid,
                "type": "repository"
            }

            update = {
                "$push": {
                    "scraped_urls": { "$each": urls }
                }
            }

            self._database.update_one(condition, update)


    def get_repositories(self):
        """ Returns available repositories. """
        self._connect()
        condition = {
            "uuid": {"$exists": True}
        }
        projection = {
            "name": 1,
            "url": 1,
            "date_added": 1,
            "uuid": 1,
            "status": 1,
            "_id": 0
        }
        resultset = self._database.find(condition, projection).sort('date_added', -1)
        return list(resultset)


    def get_repository_url(self, repository_uuid):
        self._connect()
        if repository_uuid:
            condition = { "uuid": repository_uuid }
            projection = {
                "url": 1,
                "_id": 0  # suppress implicit MongoDB internal id
            }
            result = self._database.find_one(condition, projection)
            return result["url"]


    def get_scraped_links(self, repository_uuid):
        self._connect()
        if repository_uuid:
            condition = {
                "uuid": repository_uuid
            }
            projection = {
                "scraped_urls": 1,
                "_id": 0
            }
            resultset = self._database.find_one(condition, projection)
            if "scraped_urls" in resultset:
                return list(resultset["scraped_urls"])
            else:
                return None


    def get_downloaded_files(self, repository_uuid):
        self._connect()
        if repository_uuid:
            condition = {
                "uuid": repository_uuid
            }
            projection = {
                "files.uuid": 1,
                "_id": 0
            }
            resultset = self._database.find(condition, projection)

            downloaded_uuids = []
            for array_files in resultset:
                for array_uuids in array_files["files"]:
                    downloaded_uuids.append(array_uuids["uuid"])

            return list(downloaded_uuids)


    def fetch_basic_repository_information(self, repository_uuid):
        """ Returns basic information (name, url, status, ...)  of a
        repository.

        :param repository_uuid: The UUID of the repository.
        :returns: A result set. """
        self._connect()
        condition = {
            "uuid": repository_uuid
        }

        projection = {
            "name": 1,
            "url": 1,
            "date_added": 1,
            "status": 1,
            "_id": 0
        }

        result_set = self._database.find_one(condition, projection)
        if not result_set:  # The repository UUID did not exist
            raise ValueError(f"Repository UUID {repository_uuid} is invalid")
        else:
            return result_set


    def fetch_analyzed_files_from_repository(self, repository_uuid):
        self._connect()
        condition = {
            "uuid": repository_uuid,
            "files": { "$exists": True }
        }

        projection = {
            "files": 1,
            "_id": 0
        }

        result_set = self._database.find_one(condition, projection)
        result_set = [file["uuid"] for file in list(result_set["files"]) if "analysis_properties" in file.keys()]
        return result_set


    def get_basic_repository_statistics(self, repository_uuid = None):
        self._connect()
        condition = {
            "uuid": repository_uuid if repository_uuid else { "$exists": True },
            "type": "repository",
            "links_scraped": {
                "$exists": True
            },
            "total_packages": {
                "$exists": True
            }
        }

        projection = {
            "links_scraped": 1,
            "total_packages": 1,
            "_id": 0
        }

        result_set = self._database.find_one(condition, projection)
        if not result_set:
            if not repository_uuid:
                raise ValueError(f"There are no repository statistics yet.")
            else:
                raise ValueError(f"The repository {repository_uuid} does not exist or has no basic statistics yet.")
        else:
            return result_set


    def get_advanced_repository_statistics(self, repository_uuid = None):
        self._connect()
        condition = {
            "uuid": repository_uuid if repository_uuid else { "$exists": True },
            "type": "repository",
            "files.analysis_properties": {
                "$exists": True,
            }
        }

        projection = {
            "files.uuid": 1,
            "files.analysis_properties": 1,
            "_id": 0
        }

        result_set = self._database.find(condition, projection)
        if not result_set.count():
            if not repository_uuid:
                raise ValueError(f"There are no repository statistics yet.")
            else:
                raise ValueError(f"The repository {repository_uuid} does not exist or has no basic statistics yet.")
        else:
            return list(result_set)


    def store_new_file_information(self, repository_uuid, file_uuid, file_url, file_hash):
        self._connect()
        if repository_uuid:
            condition = {
                "uuid": repository_uuid
            }
            properties = {
                "files": {
                    "uuid": file_uuid,
                    "url": file_url,
                    "download_datetime": str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")),
                    "sha256": file_hash
                }
            }
            self._database.update_one(condition, {"$push": properties})


    def store_scraping_error(self, repository_uuid, error_type, context = None):
        self._connect()
        condition = {
            "uuid": repository_uuid,
            "type": "repository"
        }

        properties = {
            "errors": {
                "type": error_type.value,
                "context": context,
                "time": str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            }
        }

        self._database.update_one(condition, {"$push": properties})


    def store_download_error(self, repository_uuid, error_type, error_url, context = None):
        self._connect()
        if repository_uuid:
            condition = {
                "uuid": repository_uuid
            }

            properties = {
                "errors": {
                    "type": error_type.value,
                    "url": error_url,
                    "time": str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")),
                    "context": context
                }
            }

            self._database.update_one(condition, {"$push": properties})


    def set_repository_status(self, repository_uuid, status: RepositoryStatus):
        self._connect()
        if repository_uuid:
            condition = {
                "uuid": repository_uuid
            }

            properties = {
                "status": str(status.value)
            }

            self._database.update_one(condition, {"$set": properties})


    def compute_global_stats(self, globs):
        """ In: list of (height, [nb_elements_0, ..., nb_elements_h])
        Out: (list of heights, list of densest levels, list of occurences)
        all have the same length """

        tuples = []
        occ = []

        for glob in globs:
            tup = (glob[0], glob[1].index(max(glob[1])))  # get the index of the level with the most nodes
            if tup not in tuples:
                tuples.append(tup)
                occ.append(1)
            else:
                i = tuples.index(tup)
                occ[i] += 1
        depth, max_levels = [], []

        for tup in tuples:
            depth.append(tup[0])
            max_levels.append(tup[1])

        return (depth, max_levels, occ)


    def get_property_sum(self, property: Properties, repository_uuid = None):
        self._connect()
        pipeline = [
            {
                "$match": {
                    "type": "repository",
                    "uuid": repository_uuid if repository_uuid else { "$exists": True },
                    "files.analysis_properties": { "$exists": True }
                }
            },
            {
                "$unwind": "$files"
            },
            {
                "$group": {
                    "_id": None,
                    "sum": {
                        "$sum": property.value
                    }
                }
            },
            {
                "$project": {
                    "_id": 0
                }
            }
        ]

        result_set = self._database.aggregate(pipeline)
        try:
            return result_set.next()["sum"]
        except IndexError:
            raise ValueError("Not analyzed yet")


    def get_document_filesizes(self, repository_uuid = None):
        self._connect()
        pipeline = [
            {
                "$match": {
                    "type": "repository",
                    "uuid": repository_uuid if repository_uuid else { "$exists": True }
                }
            },
            {
                "$project": {
                    's': '$files.filesize'
                }
            },
            {
                "$unwind": "$s"
            },
            {
                "$group": {
                    "_id": "$sizes",
                    "sizes": {
                        "$addToSet": "$s"
                    }
                }
            },
            {
                "$project": {
                    "_id": 0,
                    "sizes": 1
                }
            }
        ]

        result_set = self._database.aggregate(pipeline)
        filesize_array = result_set.next()['sizes']
        return filesize_array


    def get_document_filesizes_poweroftwo(self, repository_uuid = None):
        filesize_array = self.get_document_filesizes(repository_uuid)

        cluster_array = defaultdict(int)
        encountered_min, encountered_max = None, None

        ''' 
        We want a histogram of filesizes to the power of n
        Hence we can convert any value to binary and count its length, e.g.
        1001   --> 2^3 <= i < 2^4
        1      --> 2^0 <= i < 2^1
        100111 --> 2^5 <= i < 2^6
        '''

        for fsize in filesize_array:
            fsizebin = format(fsize, 'b')  # convert it to binary
            cluster = len(str(fsizebin))

            cluster_array[cluster] += 1

            encountered_min = cluster if encountered_min is None or cluster < encountered_min else encountered_min
            encountered_max = cluster if encountered_max is None or cluster > encountered_max else encountered_max

        # fill non-occuring key between minimal and maximal key with zeros

        for key in range(encountered_min + 1, encountered_max - 1):
            cluster_array[key] += 0

        # Sort it by key and return it

        cluster_array = OrderedDict(sorted(cluster_array.items(), key=lambda t: t[0]))
        return cluster_array


    def get_bubblechart_values(self, repository_uuid = None):
        self._connect()
        pipeline = [
            {
                "$match": {
                    "type": "repository",
                    "uuid": repository_uuid if repository_uuid else { "$exists": True }
                }
            },
            {
                "$unwind": "$files"
            },
            {
                "$project": {
                    "_id": 0,
                    "max_depth": "$files.analysis_properties.globs.max_depth",
                    "children": "$files.analysis_properties.globs.children"
                }
            }
        ]

        resultset = self._database.aggregate(pipeline)
        globs_array = []
        diagram_values = []

        # Rebuild globs structure from Nicolas
        for record in resultset:
            # Ignore empty records. These occur if the analysis_properties are missing
            if record:
                globs_array.append((record['max_depth'], record['children']))

        depth, max_levels, occurences = self.compute_global_stats(globs_array)
        log_occurences = [math.log10(occurency) for occurency in occurences]
        max_value_of_log_occurences = max(log_occurences)
        scale_factor = max_value_of_log_occurences / 100  # biggest bubble has a radius of 100px

        for i in range(0, len(depth)):
            diagram_values.append({'x': depth[i], 'y': max_levels[i], 'r': (log_occurences[i] / scale_factor), 'dc': occurences[i]})

        return diagram_values


    def get_bubblechart_provenance(self, provenance_x, provenance_y, repository_uuid = None):
        self._connect()
        pipeline = [
            {
                "$match": {
                    "type": "repository",
                    "uuid": repository_uuid if repository_uuid else { "$exists": True }
                }
            },
            {
                "$unwind": "$files"
            },
            {
                "$project": {
                    "_id": 0,
                    "max_depth": "$files.analysis_properties.globs.max_depth",
                    "children": "$files.analysis_properties.globs.children",
                    "document_uuid": "$files.uuid"
                }
            }
        ]
        resultset = self._database.aggregate(pipeline)  # Cursor with {"max_depth": .., "children": [..]"}
        globs_array = []
        max_depth_array = []
        children_array = []
        document_array = []
        provenance_values = []

        for record in resultset:
            if "max_depth" in record and "children" in record:
                max_depth_array.append(record['max_depth'])
                children_array.append(record['children'])
                globs_array.append((record['max_depth'] ,record['children']))
                document_array.append(record['document_uuid'])

        for i in range(0, len(globs_array)):
            if str(max_depth_array[i]) == str(provenance_x) and str(children_array[i].index(max(children_array[i]))) == str(provenance_y):
                provenance_values.append(document_array[i])

        return provenance_values


    def get_amount_of_scraped_links(self, repository_uuid = None):
        self._connect()
        pipeline = [
            {
                "$match": {
                    "type": "repository",
                    "uuid": repository_uuid if repository_uuid else { "$exists": True }
                }
            },
            {
                "$project": {
                    "_id": 0,
                    "scrapeCount": {
                        "$size": "$scraped_urls"
                    }
                }
            },
            {
                "$group": {
                    "_id": None, "sum": {
                        "$sum": "$scrapeCount"
                    }
                }
            },
            {
                "$project": {
                    "_id": 0
                }
            }
        ]

        result_set = self._database.aggregate(pipeline)
        scrape_count = result_set.next()["sum"]
        return scrape_count


    def get_amount_of_downloaded_documents(self, repository_uuid = None):
        self._connect()
        pipeline = [
            {
                "$match": {
                    "type": "repository",
                    "uuid": repository_uuid if repository_uuid else { "$exists": True }
                }
            },
            {
                "$project": {
                    "_id": 0,
                    "ddCount": {
                        "$size": "$files"
                    }
                }
            },
            {
                "$group": {
                    "_id": None, "sum": {
                        "$sum": "$ddCount"
                    }
                }
            },
            {
                "$project": {
                    "_id": 0
                }
            }
        ]

        result_set = self._database.aggregate(pipeline)
        downloaded_documents = result_set.next()["sum"]
        return downloaded_documents


    def get_amount_of_errors(self, repository_uuid = None):
        self._connect()
        pipeline = [
            {
                "$match": {
                    "type": "repository",
                    "uuid": repository_uuid if repository_uuid else { "$exists": True }
                }
            },
            {
                "$project": {
                    "_id": 0,
                    "errorCount": {
                        "$size": "$errors"
                    }
                }
            },
            {
                "$group": {
                    "_id": None, "sum": {
                        "$sum": "$errorCount"
                    }
                }
            },
            {
                "$project": {
                    "_id": 0
                }
            }
        ]

        try:
            result_set = self._database.aggregate(pipeline)
            error_count = result_set.next()["sum"]
        except OperationFailure: # errorCount is not present
            error_count = 0

        return error_count


    def get_amount_of_analyzed_documents(self, repository_uuid = None):
        self._connect()
        pipeline = [
            {
                "$match": {
                    "type": "repository",
                    "uuid": repository_uuid if repository_uuid else { "$exists": True }
                }
            },
            {
                "$project": {
                    "_id": 0,
                    "properties": "$files.analysis_properties"
                }
            },
            { "$unwind": "$properties" },
            { "$count": "sum" }
        ]

        try:
            result_set = self._database.aggregate(pipeline)
            analyzed_count = result_set.next()["sum"]
        except OperationFailure:
            analyzed_count = 0

        return analyzed_count


    def get_failed_downloads(self, repository_uuid = None):
        self._connect()
        pipeline = [
            {
                "$match": {
                    "type": "repository",
                    "uuid": repository_uuid if repository_uuid else { "$exists": True }
                }
            },
            {
                "$unwind": "$errors"
            },
            {
                "$project": {
                    "_id": 0,
                    "errors": 1,
                }
            }
        ]

        try:
            return_set = []
            result_set = self._database.aggregate(pipeline)
            for errors in result_set:
                return_set.append({
                    "type": errors["errors"]["type"],
                    "url": errors["errors"]["url"],
                    "context": errors["errors"]["context"],
                    "ts": errors["errors"]["time"]
                })
        except Exception:
            pass
        return return_set


    def store_full_document_results(
        self,
        repository_uuid,
        document_uuid,
        filesize,
        stat_builder,
        glob,
        array_properties,
        is_geojson,
        node_uuid = None):
        """ Store all properties of an analysed document at once.

        :param repository_uuid: The UUID of the repository holding the document.
        :param document_uuid: The UUID of the actual document.
        :param filesize: The filesize of the document (file).
        :param stat_builder: A StatisticsBuilder object holding the statistics \
        of the document.
        :param glob: A dictionary holding information about global properties.
        :param array_properties: An ArrayProperties object holding array \
        properties.
        :param is_geojson: Whether or not GeoJSON has been detected.
        :param node_uuid: The UUID of the node that analyzed the file. """
        self._connect()
        if repository_uuid and document_uuid:
            condition = {
                "uuid": repository_uuid,
                "files.uuid": document_uuid
            }

            # amounts = stat_builder.tcp.get_amounts()

            properties = {
                "files.$.filesize": filesize,
                "files.$.analysis_properties.required_properties": stat_builder.rp.N_REQUIRED,
                "files.$.analysis_properties.optional_properties": stat_builder.rp.N_PROPS - stat_builder.rp.N_REQUIRED,
                "files.$.analysis_properties.multiplicity_keywords.allof": stat_builder.mrp.get_allof(),
                "files.$.analysis_properties.multiplicity_keywords.anyof": stat_builder.mrp.get_anyof(),
                "files.$.analysis_properties.multiplicity_keywords.oneof": stat_builder.mrp.get_oneof(),

                "files.$.analysis_properties.type_counts.string": stat_builder.tcp.get_amount_of_strings(),
                "files.$.analysis_properties.type_counts.number": stat_builder.tcp.get_amount_of_numbers(),
                "files.$.analysis_properties.type_counts.integer": stat_builder.tcp.get_amount_of_integers(),
                "files.$.analysis_properties.type_counts.boolean": stat_builder.tcp.get_amount_of_booleans(),
                "files.$.analysis_properties.type_counts.array": stat_builder.tcp.get_amount_of_arrays(),
                "files.$.analysis_properties.type_counts.object": stat_builder.tcp.get_amount_of_objects(),
                "files.$.analysis_properties.type_counts.null": stat_builder.tcp.get_amount_of_nulls(),

                "files.$.analysis_properties.string_abuse.empty_string": stat_builder.tcp.get_amount_of_empty_strings(),
                # "files.$.analysis_properties.type_counts.non_empty_string": stat_builder.tcp.get_amount_of_non_empty_strings(),
                "files.$.analysis_properties.string_abuse.abusive_number": stat_builder.tcp.get_amount_of_abusive_numbers(),
                "files.$.analysis_properties.string_abuse.abusive_boolean": stat_builder.tcp.get_amount_of_abusive_booleans(),

                "files.$.analysis_properties.globs.max_depth": glob["max_depth"],
                "files.$.analysis_properties.globs.children": glob["children"],
                
                "files.$.analysis_properties.array_properties.simple": array_properties.get_amount_simple(),
                "files.$.analysis_properties.array_properties.object": array_properties.get_amount_object(),
                "files.$.analysis_properties.array_properties.only_array": array_properties.get_amount_only_array(),
                "files.$.analysis_properties.array_properties.mixed": array_properties.get_amount_mixed(),
                "files.$.analysis_properties.array_properties.empty": array_properties.get_amount_empty(),
                "files.$.analysis_properties.array_properties.nested": array_properties.get_amount_nested(),

                "files.$.analysis_properties.is_geojson": is_geojson
            }

            if node_uuid:
                properties["files.$.responsible_node"] = node_uuid

            self._database.update_one(condition, {'$set': properties})


    def store_duration(self, repository_uuid, duration_type: DurationType, duration, node_count = 0):
        """ Stores the duration of an analysis process.

        :param repository_uuid: The UUID of the repository.
        :param duration_type: Either DurationType.analysisDuration or \
        DurationType.downloadDuration.
        :param duration: The duration of the analysis process.
        :param node_count: The number of used nodes. """
        self._connect()

        condition = {
            "uuid": repository_uuid,
            "type": "repository"
        }

        update = {
            "$push": {
                duration_type.value: {
                    "duration": duration,
                    "datetime": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                    "nodes": node_count
                }
            }
        }
        
        self._database.update_one(condition, update)


    def get_geojson_documents(self, repository_uuid = None):
        """ Get all documents where GeoJSON was detected.

        :param repository_uuid: (opt) The UUID of the repository.
        :returns: A list of UUIDs representing the files that are probably \
        GeoJSON documents. """
        self._connect()

        pipeline = [
            {
                "$match": {
                    "type": "repository",
                    "uuid": repository_uuid if repository_uuid else { "$exists": True }
                }
            },
            {
                "$unwind": "$files"
            },
            {
                "$match": {
                    "files.analysis_properties.is_geojson": True
                }
            },
            {
                "$group": {
                    "_id": "$_id",
                    "files": {
                        "$push": "$files.uuid"
                    }
                }
            },
            {
                "$project": { "_id": 0 }
            }
        ] 

        result = self._database.aggregate(pipeline)
        for r in result:
            return r["files"]
        return []


    def get_documents_with_string_abuse(self, abuse_type: AbuseType, repository_uuid = None):
        """ Get all documents where abusive strings were detected.

        :param abuse_type: The type of the string abuse (AbuseType).
        :param repository_uuid: (opt) The UUID of the repository.
        :returns: A list of UUIDs representing the files that contain \
        abusive strings. """
        self._connect()

        pipeline = [
            {
                "$match": {
                    "type": "repository",
                    "uuid": repository_uuid if repository_uuid else { "$exists": True },
                    "files.analysis_properties.string_abuse": { "$exists": True }
                }
            },
            {
                "$unwind": "$files"
            },
            {
                "$match": {
                    f"files.analysis_properties.string_abuse.{abuse_type.value}": { "$gt": 0 }
                }
            }, 
            {
                "$group": {
                    "_id": None,
                    "files": {
                        "$push": "$files.uuid"
                    }
                }
            },
            {
                "$project": { "_id": 0 }
            }
        ]

        result = self._database.aggregate(pipeline)
        for r in result:
            return r["files"]
        return []


    def get_string_abuse_counts(self, abuse_type: AbuseType, repository_uuid = None):
        """ Get the amount of abused strings.

        :param abuse_type: The type of the string abuse (AbuseType).
        :param repository_uuid: (opt) The UUID of the repository.
        :returns: The number of abused strings. """
        self._connect()

        pipeline = [
            {
                "$match": {
                    "type": "repository",
                    "uuid": repository_uuid if repository_uuid else { "$exists": True },
                    "files.analysis_properties.string_abuse": { "$exists": True }
                }
            },
            {
                "$unwind": "$files"
            },
            {
                "$match": {
                    f"files.analysis_properties.string_abuse.{abuse_type.value}": { "$gt": 0 }
                }
            }, 
            {
                "$group": {
                    "_id": None,
                    "count": { "$sum": f"$files.analysis_properties.string_abuse.{abuse_type.value}" } 
                }
            },
            {
                "$project": { "_id": 0 }
            }
        ]

        result = self._database.aggregate(pipeline).next()
        return result["count"]


    def get_error_type_counts(self, repository_uuid = None):
        """ Gets all error types as well as their frequency.

        :param repository_uuid: (opt) The UUID of the repository.
        :returns: A generator that yields \
        ``{ "type": <error type>, count: <count> }`` dictionaries. """
        self._connect()

        pipeline = [
            {
                "$match": {
                    "type": "repository",
                    "uuid": repository_uuid if repository_uuid else { "$exists": True },
                    "errors": { "$exists": True }
                }
            },
            {
                "$unwind": "$errors"
            },
            {
                "$group": {
                    "_id": "$errors.type",
                    "count": { "$sum": 1 }
                }
            },
            {
                "$project": {
                    "type": "$_id",
                    "_id": 0,
                    "count": 1
                }
            },
            {
                "$sort": {
                    "count": -1
                }
            }
        ]

        for result in self._database.aggregate(pipeline):
            yield result


    def get_error_context_counts(self, repository_uuid = None):
        """ Gets all error contexts as well as their frequency.

        :param repository_uuid: (opt) The UUID of the repository.
        :returns: A generator that yields \
        ``{ "context": <error context>, count: <count> }`` dictionaries. """
        self._connect()

        pipeline = [
            {
                "$match": {
                    "type": "repository",
                    "uuid": repository_uuid if repository_uuid else { "$exists": True },
                    "errors": { "$exists": True }
                }
            },
            {
                "$unwind": "$errors"
            },
            {
                "$group": {
                    "_id": "$errors.context",
                    "count": { "$sum": 1 }
                }
            },
            {
                "$project": {
                    "context": "$_id",
                    "_id": 0,
                    "count": 1
                }
            },
            {
                "$sort": {
                    "count": -1
                }
            }
        ]

        for result in self._database.aggregate(pipeline):
            yield result


    def get_config(self, config_name):
        """ Retrieves a configuration document from the database.

        :param config_name: The name of the configuration to retrieve.
        :returns: dict """
        self._connect()
        condition = {
            "type": "configuration",
            "name": config_name
        }

        projection = {
            "_id": 0
        }

        result_dict = self._database.find_one(condition, projection)
        return dict(result_dict) if result_dict else None


    def update_config(self, config_name, key: ConfigKeys, value):
        """ Updates a single parameter of a configuration.

        :param config_name: The name of the configuration.
        :param key: The key that shall be updated (type: ConfigKey).
        :param value: The new value.
        :returns: True if the update was successful, otherwise False. """
        self._connect()

        condition = {
            "type": "configuration",
            "name": config_name
        }

        update = {
            "$set": {
                key.value: value
            }
        }

        return bool(self._database.update_one(condition, update).modified_count)


    def get_node_statuses_all(self, config_name):
        """ Gets the status information of all nodes that are
        present in the nodeCollection in the database.

        :param config_name: The name of the configuration file the nodes \
        belong to, for example 'config'.
        :returns: A list of node information dictionaries. """
        self._connect()

        selection = {
            "type": "nodeCollection",
            "for_config": config_name
        }

        projection = {
            "_id": 0,
            "nodes.address": 0,
            "nodes.port": 0
        }

        result_set = self._database.find_one(selection, projection)
        if result_set:
            if "nodes" in dict(result_set):
                return list(result_set["nodes"])
        return []


    def get_node_statuses(self, config_name, node_uuid):
        """ Gets all statuses of a single node.
        :param config_name: The name of the configuration file the node \
        belongs to, for example 'config'.

        :param node_uuid: The UUID of the node.
        :returns: The node statuses as a list of dictionaries. """
        all_statuses = self.get_node_statuses_all(config_name)
        # Get the right node
        node_statuses = [s for s in all_statuses if s["node_uuid"] == node_uuid][0]
        return node_statuses


    def get_node_status(self, config_name, node_uuid):
        """ Gets the last status of a single node.

        :param config_name: The name of the configuration file the node \
        belongs to, for example 'config'.
        :param node_uuid: The UUID of the node.
        :returns: The latest node status as a dictionary. """
        node_statuses = self.get_node_statuses(config_name, node_uuid)
        # Return the latest status only
        result = node_statuses
        result["properties"] = result["properties"][-1]
        return result


    def update_config_dict(self, config_name, dictionary, upsert=False):
        """ Updates multiple parameters of a configuration at once.
        The parameters and their new values have to be part of a dictionary.

        :param config_name: The name of the configuration.
        :param dictionary: The dictionary containing the keys and values.
        :param upsert: Whether or not a new config file shall be created if \
        no such file exists yet.
        :returns: True if the update was successful, otherwise False."""

        # TODO some parameter checks? Or use of helpers.classes.config_keys.ConfigKeys()?
        self._connect()

        # Remove empty entries
        update_dict = {}
        for key in dictionary:
            if dictionary[key]:
                update_dict[key] = dictionary[key]

        condition = {
            "type": "configuration",
            "name": config_name
        }

        # If an empty dict is provided, add an empty node list for
        # the $set operator to work properly
        if len(update_dict.keys()) == 0:
            update_dict["nodes"] = []

        update = {
            "$set": update_dict
        }

        return bool(self._database.update_one(condition, update, upsert=upsert).modified_count)


    def add_node(self, config_name, address, port, name = None):
        """ Adds a new node to the configuration.

        :param config_name: The name of the configuration.
        :param address: The address of the node.
        :param port: The port of the node.
        :param name: (opt) A name for the node.
        :returns: The new node's UUID, if the node has been added \
        successfully, otherwise None. """
        self._connect()

        node_dict = {
            "address": address,
            "port": port,
            "uuid": str(uuid.uuid4()), # New UUID
            "enabled": True # Option to enable/disable the node
        }
        if name:
            node_dict["name"] = name

        condition = {
            "type": "configuration",
            "name": config_name
        }

        update = {
            "$push": {"nodes": node_dict}
        }

        success = bool(self._database.update_one(condition, update).modified_count)
        if success:
            return node_dict["uuid"]
        else:
            return None


    def remove_node(self, config_name, node_uuid):
        """ Deletes a node from a configuration.

        :param config_name: The name of the configuration the node belongs to.
        :param node_uuid: The UUID of the node to remove.
        :returns: (bool) Whether or not the node has been deleted \
        successfully. """
        self._connect()

        condition = {
            "type": "configuration",
            "name": config_name
        }

        update = {
            "$pull": {"nodes": {"uuid": node_uuid}}
        }

        return bool(self._database.update_one(condition, update).modified_count)


    def get_available_configs(self):
        """ Gets the list of all available config files.
        
        :returns: A list of the config names. """
        self._connect()

        selection = {
            "type": "configuration"
        }
        
        projection = {
            "name": 1,
            "_id": 0
        }

        result = self._database.find(selection, projection).sort("name", pymongo.ASCENDING)
        config_names = []
        for config in result:
            config_names.append(config["name"])
        return config_names


    def get_node(self, config_name, node_uuid):
        """ Get the information of a single node.

        :param config_name: The name of the config the node belongs to.
        :param node_uuid: The UUID of the node.
        :returns: A dictionary which consists of the node's attributes. """
        self._connect()
        
        selection = {
            "type": "configuration",
            "name": config_name,
            "nodes.uuid": node_uuid
        }

        projection = {
            "_id": 0,
            "nodes.$": 1
        }

        result = self._database.find_one(selection, projection, limit=1)
        return dict(result)["nodes"][0]


    def get_nodes(self, config_name):
        """ Get the information of all nodes.

        :param config_name: The name of the config the nodes belong to.
        :returns: A list of dictionaries consisting of the nodes's \
        attributes. """
        self._connect()

        selection = {
            "type": "configuration",
            "name": config_name,
            "nodes": { "$exists": True }
        }

        projection = {
            "_id": 0,
            "nodes": 1
        }

        result = self._database.find_one(selection, projection)
        return dict(result)["nodes"]


    def toggle_all_nodes(self, config_name):
        """ Toggles the state of every node.

        :param config_name: The name of the config the node belongs to. """
        self._connect()

        nodes = self.get_nodes(config_name)
        for node in nodes:
            self.toggle_node(config_name, node["uuid"])


    def toggle_node(self, config_name, node_uuid):
        """ Enables or disables a node, depending on its current state.

        :param config_name: The name of the config the node belongs to.
        :param node_uuid: The UUID of the node.
        :returns: A tuple of two boolean values. \
        The first determines the success of the execution, the second \
        determines the new state of the node (enabled = True). """
        self._connect()

        # Get current status of the node
        selection = {
            "type": "configuration",
            "name": config_name,
            "nodes.uuid": node_uuid
        }

        projection = {
            "nodes.uuid": 1,
            "nodes.enabled": 1,
            "_id": 0
        }

        result = dict(self._database.find_one(selection, projection))["nodes"]

        is_enabled = None
        for node in result:
            if node["uuid"] == node_uuid:
                is_enabled = node["enabled"]

        # Toggle the current status: enabled = !enabled
        update = {
            "$set": {"nodes.$[elem].enabled": not is_enabled}
        }

        # Return whether or not at least one row was affected
        result = self._database.update_one(selection, update, array_filters=[{"elem.uuid": node_uuid}])
        return (bool(result.modified_count), not is_enabled)


    def store_node_for_download(self, repository_uuid, file_uuid, node_uuid):
        """ Sets the UUID of the node that downloaded a certain file.

        :param repository_uuid: The UUID of the repository holding the file.
        :param file_uuid: The UUID of the downloaded file.
        :param node_uuid: The UUID of the node that downloaded the file.
        :returns: Whether or not the operation was successful. """
        self._connect()
        
        selection = {
            "type": "repository",
            "uuid": repository_uuid,
            "files": {"$exists": True},
            "files.uuid": file_uuid
        }

        update = {
            "$set": {"files.$[elem].downloaded_by_node": node_uuid}
        }

        result = self._database.update_one(selection, update, array_filters=[{"elem.uuid": file_uuid}])
        return bool(result.modified_count)


    def get_files_downloaded_by_node(self, repository_uuid, node_uuid):
        """ Gets the UUIDs of files downloaded by a certain node.

        :param repository_uuid: The UUID of the repository holding the files.
        :param node_uuid: The UUID of the node. """
        self._connect()

        pipeline = [
            {
                "$match": {
                    "type": "repository",
                    "uuid": node_uuid
                }
            },
            { "$unwind": "$files" },
            { "$match": {
                "files.downloaded_by_node": node_uuid
                }
            },
            
            { "$project": { "uuid":"$files.uuid", "_id": 0 }},
            { "$unwind": '$uuid'}
        ]

        result = self._database.aggregate(pipeline)
        return [file["uuid"] for file in result]


    def get_children_counts(self, repository_uuid = None):
        """ Gets the number of chidren per tree level.

        :param repository_uuid: The UUID of the repository holding the files.  
        :returns: An (int, int) dictionary where the keys are the tree \
        levels and the values are the number of nodes on that level. """
        self._connect()
        
        pipeline = [
            {
                "$match": {
                    "type": "repository",
                    "uuid": repository_uuid if repository_uuid else { "$exists": True }
                }
            },
            {
                "$project": {
                    "children": "$files.analysis_properties.globs.children",
                    "_id": 0
                }
            }
        ]

        result_set = self._database.aggregate(pipeline).next()["children"]
        
        counter_dict = {}
        for result in result_set:
            for i, value in enumerate(result):
                if not i in counter_dict:
                    counter_dict[i] = 0
                counter_dict[i] += value

        return counter_dict


    def update_node_status(self, config_name, node_uuid, node_stats):
        """
        Stores information of a node to the database.
        
        :param config_name: The name of the configuration file the nodes \
        belong to, for example 'config'.
        :param node_uuid: The UUID of the node.
        :param node_stats: A dictionary containing the information. It should \
        look like this:
        
        {
            "address": <node address>,  
            "port": <node port>,  
            "cpu_usage": <CPU usage>,  
            "ram_total": <total amount of RAM>,  
            "ram_available": <amount of RAM that's available>,  
            "platform": <platform string (platform.platform())>,  
            "distro": <distro name (distro.linux_distribution()[0])>  
        }  
        """
        self._connect()
        
        if not (config_name and node_uuid and node_stats):
            # TODO: Throw an error
            print("There are missing parameters.")
        else:
            condition = {
                "type": "nodeCollection",
                "for_config": config_name
            }

            properties = {
                "datetime": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                "cpu_usage": node_stats["cpu_usage"],
                "ram_total": node_stats["ram_total"],
                "ram_available": node_stats["ram_available"]
            }

            node_metadata = {
                "node_uuid": node_uuid,
                "address": node_stats["address"],
                "port": node_stats["port"],
                "platform": node_stats["platform"],
                "distro": node_stats["distro"],
                "properties": []
            }
            
            # Check if a nodeCollection already exists, if not,
            # create a new one
            if not self._database.find(condition).count():
                self._database.insert_one({
                    "type": "nodeCollection",
                    "for_config": config_name,
                    "nodes": []
                })

            # Check if there's an existing entry for the node,
            # if not, create a new one
            count = self._database.find(
                {
                    "type": "nodeCollection",
                    "for_config": config_name,
                    "nodes.node_uuid": node_uuid
                }
            ).count()

            if not count:
                self._database.update_one(condition, {
                    "$push": {"nodes": node_metadata}}
                )

            # Add the node information
            self._database.update_one({
                    "type": "nodeCollection",
                    "for_config": config_name
                }, {"$push": {"nodes.$[elem].properties": properties}},
                array_filters=[{"elem.node_uuid": node_uuid}]
            )