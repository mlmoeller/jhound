import hashlib
import io
import json
import os
import sys
import time
import uuid
import zipfile

import ijson
import requests

from helpers import confighelper
from helpers.classes.errors import ScrapingErrors
from helpers.colors import TColorsForeground as Colors
from helpers.colors import cstring
from helpers.mongohelper import Client

OUTPUT = True
mongodb_client = Client()

def create_message(content):
    """
    Transforms a string into a sendable message.

    :param content: The message string.
    :returns: The message as a byte array.
    """
    return bytearray(map(ord, content))


def recvall(socket):
    """ Receive everything. """
    data = bytes()
    while True:
        packet = socket.recv(128)
        if not packet:
            break
        data += packet
    return data


def execute_function_periodically(func, seconds):
    """
    Executes a function every x seconds.

    :param func: The function. If you need to pass arguments, consider using \
    the 'lambda' keyword.
    :param seconds: The amount of seconds between two function calls.
    """
    while True:
        func()
        time.sleep(seconds)


def oprint(text, end = "\n", file = sys.stdout):
    if OUTPUT:
        print(text, end=end, file=file)


# TODO: Restructure method
def download_file(link, storage_path, repository_uuid, ignore_ssl = False, output = True):
    """
    Downloads the files of a single CKAN dataset.

    This method catches and tracks several errors in order to store \
    them to the "errors" section in the database. Possible errors are: 
        - SSL: The server's TLS certificate can't be validated
        - TIMEOUT: The request ends with a timeout error, probably \
          due to a large file size or an instable internet \
          connection.
        - FILETOOLARGE: The file size exceeds the set limit.
        - STATUSCODE: The HTTP request returns a a non-2XX \
          status code (e.g. 401, 403, 404, 5XX, ...)
        - CONTENTTYPE: The content type in the HTTP header of the \
          request doesn't match any of the given content types.
        - MIMETYPE: The MimeType doesn't match any of the given \
          MimeTypes.
        - PROTOCOL: Unsupported protocol (e.g. FTP).
        
    :param link: The URL to the file to be downloaded.
    :param storage_path: The path to the directory of the new file.
    :param repository_uuid: The UUID of the repository holding the file.
    :param ignore_ssl: (opt) Whether or not SSL errors shall be ignored.
    :returns: The UUID (filename) of the newly created file, else False
    """
    
    global OUTPUT
    OUTPUT = output

    link = link.strip()
    try:
        oprint(f"> Downloading {link}...")

        # Validating file size
        r = requests.head(
            link,
            headers={"Accept-Encoding": "deflate"}
        )

        if "Content-Length" in r.headers:
            content_length = int(r.headers["Content-Length"])
            if confighelper.MAX_FILESIZE > 0 and content_length / 1024**2 > confighelper.MAX_FILESIZE:
                oprint(cstring(f"[WARNING] File {link} too big; ignoring.", foreground=Colors.YELLOW), file=sys.stderr)
                mongodb_client.store_download_error(
                    repository_uuid, ScrapingErrors.FILETOOLARGE, link, str(content_length / 1024**2)
                )
                return False
        elif confighelper.MAX_FILESIZE:
            oprint("[WARNING] Content-Length header missing; file size needs " +
                f"to be checked after the file has been downloaded.")
        
        r = requests.get(
            link,
            stream=True,
            verify=(not ignore_ssl),
            timeout=confighelper.TIMEOUT
        )

    except requests.exceptions.InvalidSchema:
        oprint(
            cstring(f"[WARNING] File {link}: Unsupported protocol ({link.split(':')[0].upper()})", foreground=Colors.YELLOW),
            file=sys.stderr
        )
        mongodb_client.store_download_error(
            repository_uuid,
            ScrapingErrors.PROTOCOL,
            link,
            link.split(':')[0].upper()
        )
        return False

    # Timeout
    except requests.exceptions.Timeout as e:
        oprint(cstring("[ERROR] Timeout.", foreground=Colors.RED), file=sys.stderr)
        mongodb_client.store_download_error(repository_uuid, ScrapingErrors.TIMEOUT, link, str(e))
        return False

    # Invalid SSL certificate
    except requests.exceptions.SSLError as e:
        oprint(cstring("[ERROR] Server Certificate cannot be validated. Skipping resource.", foreground=Colors.RED), file=sys.stderr)
        mongodb_client.store_download_error(repository_uuid, ScrapingErrors.SSL, link, str(e))
        return False

    # Connection error
    except requests.exceptions.ConnectionError as e:
        oprint(cstring("[ERROR] Connection error.", foreground=Colors.RED), file=sys.stderr)
        mongodb_client.store_download_error(repository_uuid, ScrapingErrors.CONNECTION, link, str(e))
        return False

    # Other error
    except Exception as e:
        print(e)
        oprint(cstring("[ERROR] Unknown error occured. Skipping resource.", foreground=Colors.RED), file=sys.stderr)
        mongodb_client.store_download_error(repository_uuid, ScrapingErrors.UNKNOWN, link, str(e))
        return False
    
    # Bad status code
    if r.status_code != requests.codes["ok"]:
        oprint(cstring(f"[ERROR] The resource at {link} could not be downloaded (Bad status code {r.status_code})", foreground=Colors.RED), \
            file=sys.stderr)
        mongodb_client.store_download_error(repository_uuid, ScrapingErrors.STATUSCODE, link, str(r.status_code))
        return False

    else:
        # Everything went fine
        filename = str(uuid.uuid4())
        
        # Write json content to file
        # Check if the storage path exists; if not, create directories
        if not os.path.exists(storage_path):
            try:
                os.makedirs(storage_path)
            except FileExistsError:
                # Another node just created the folder
                pass
        
        f = open(os.path.join(storage_path, str(filename)), "wb")

        # File is being stored in chunks à 1024B
        file_length = 0
        for chunk in r.iter_content(1024):
            if chunk:
                f.write(chunk)
                file_length += len(chunk)
        f.close()
        oprint(f"> Downloaded {link} ({round(file_length / 1024**2, 2)} MiB), stored as {filename}.")

        # Check size of downloaded file
        if confighelper.MAX_FILESIZE > 0: 
            if file_length / 1024**2 > confighelper.MAX_FILESIZE:
                oprint(cstring(f"[WARNING] File {filename} too big; deleting.", foreground=Colors.YELLOW), file=sys.stderr)
                os.remove(os.path.join(storage_path, str(filename)))
                mongodb_client.store_download_error(
                    repository_uuid, ScrapingErrors.FILETOOLARGE, link, str(file_length / 1024**2)
                )
                return False

        # Calculate SHA256 hash
        f = open(os.path.join(storage_path, filename), "rb")
        file_hash = hashlib.sha256()
        for chunk in iter(lambda: f.read(1024), b""):
            file_hash.update(chunk)
        f.close()
        oprint(f"SHA256 hash: {file_hash.hexdigest()}")

        # Check if we fetched valid JSON
        # @TODO Find a more efficient alternative or skip this part
        is_valid = False
        oprint(f"> Validating {filename}")
        try:
            with open(os.path.join(storage_path, filename), "rb") as f:
                parser = ijson.parse(f)
                for _ in parser:
                    pass
            oprint(f"> Valid. \n")
            mongodb_client.store_new_file_information(
                repository_uuid,
                str(filename),
                str(r.url),
                str(file_hash.hexdigest())
            )
            is_valid = True
        except ijson.JSONError as e:
            oprint(cstring(
                "[WARNING] Invalid JSON found; documenting it and deleting the file.",
                foreground=Colors.YELLOW
            ), file=sys.stderr)
            os.remove(os.path.join(storage_path, filename))
            mongodb_client.store_download_error(repository_uuid, ScrapingErrors.PARSING, link, str(e))
        finally:
            f.close()
            return filename if is_valid else False

        return False
