import ijson
from helpers.classes.nodetype import NodeType
from helpers.classes.arrayproperties import ArrayProperties

ROOT_NAME = ""

geojson_types = [
    "Point",
    "LineString",
    "Polygon",
    "MultiPoint",
    "MultiLineString",
    "MultiPolygon",
    "Feature",
    "FeatureCollection",
    "GeometryCollection"
]

def infer_type(ijson_event_type):
    """ Define a node type for a leaf based on the event raised by ijson """
    if ijson_event_type == "string":
        return NodeType.STRING
    elif ijson_event_type == "boolean":
        return NodeType.BOOLEAN
    elif ijson_event_type == "integer":
        return NodeType.INTEGER
    elif ijson_event_type == "number":
        return NodeType.NUMBER
    elif ijson_event_type == "null":
        return NodeType.NULL
    else:
        return NodeType.VOID


def create_object(node_name, custom_objects):
    """ Alias for the creation of a custom object structure

    types: dictionary of all the node types an object gets
    in a JSON document, see create_object_type
    ltype: internal type used to determine which type has
    last been given to an object (used to give children properly) """
    custom_objects[node_name] = {"types": {}, "ltype": NodeType.VOID}


def create_object_type(node, node_type):
    """ Create a type structure for a node

    children: objects contained within the node in a JSON-document,
    referred by their node name or their node type
    occ: number of times the child occurs, generally """
    node["types"][node_type] = {"children": {}, "occ": 1}


def parent_name(prefix, custom_objects):
    """ Find the parent of a node according to the tree name
    The parent is the first node (leaf excluded) whose name is
    defined in the custom_objects array
    e.g. parent_name("abc.def.ghi.item.item") = "ghi", if ghi
    has been defined in custom_objects before
    e.g. parent_name("") = ROOT_NAME """
    nodes = prefix.split('.')
    p = ROOT_NAME
    length = len(nodes)
    n = 1
    for i in range(length - 2, -1, -1):  # one liner ?
        if nodes[i] in custom_objects:
            p = nodes[i]
            break
        n += 1
    return p, n


def leaf_name(prefix):
    """ Find the last part of a tree name
    e.g. leaf_name("abc.def.ghi.jkl") = "jkl" """
    nodes = prefix.split('.')
    return nodes[-1]
    

# global_properties_counter = False for test cases
def act_start_map(pre, custom_objects, global_properties_counter=False):
    """ Function mapped to the start_map event of ijson  
    Used to update the status of a node (current type, occurences of OBJECT type)"""
    #global properties - depth computation
    if global_properties_counter: 
        curr_depth = global_properties_counter["depth"]
        # ? increment taken care of by map_key
        # global_properties_counter["children"][curr_depth] += 1
        global_properties_counter["depth"] += 1
        if curr_depth == global_properties_counter["max_depth"]:
            global_properties_counter["max_depth"] += 1
            global_properties_counter["children"] += [0]
    
    lname = leaf_name(pre)
    if lname not in custom_objects:  # nested array situation
        return False
    lnode = custom_objects[lname]
    # the node is used as an object
    if NodeType.OBJECT not in lnode["types"]:
        create_object_type(lnode, NodeType.OBJECT)
    else:
        lnode["types"][NodeType.OBJECT]["occ"] += 1
    lnode["ltype"] = NodeType.OBJECT


def act_end_map(pre, global_properties_counter):
    """ Function mapped to the end_map event of ijson """
    global_properties_counter["depth"] -= 1


def act_start_array(pre, custom_objects): 
    """ Function mapped to the start_array event of ijson  
    Used to update the status of a node (current type, occurences of ARRAY type)"""
    lname = leaf_name(pre)
    if lname not in custom_objects:  # nested array situation
        return False
    lnode = custom_objects[lname]
    # the node is used as an array
    # if the current parent has never been tagged as an array
    if NodeType.ARRAY not in lnode["types"]:
        create_object_type(lnode, NodeType.ARRAY)
    else:
        lnode["types"][NodeType.ARRAY]["occ"] += 1
    lnode["ltype"] = NodeType.ARRAY


def act_end_array(pre, array_properties):
    """ Function mapped to the end_array event of ijson """
    array_properties.analysis()


# global_properties_counter = False for test cases
def act_map_key(pre, val, custom_objects, global_properties_counter = False):  # property met
    """ Function mapped to the map_key event of ijson  
    This event occurs when a new "prop":value object is parsed by ijson,
    we receive here prop """
    # quick fix for the ONLY keyword that gets us in trouble
    if val == "item":
        val = "__int__item" 
    lname = leaf_name(pre)
    if val not in custom_objects:  # first time seeing prop
        create_object(val, custom_objects)

    try:
        if lname in custom_objects:  # our parent is directly above us
            pnode = custom_objects[lname]
            pnode = pnode["types"][pnode["ltype"]]["children"]
        else:   # value in an array
            (pname, number_arrays) = parent_name(pre, custom_objects)
            pnode = custom_objects[pname]
            pnode = pnode["types"][pnode["ltype"]]["children"]
            for _ in range(1, number_arrays):  # nested array handling
                if NodeType.ARRAY not in pnode:
                    pnode[NodeType.ARRAY] = {"children": {}}
                pnode = pnode[NodeType.ARRAY]["children"]
    except Exception:
        pass

    # occ
    if val not in pnode:
        pnode[val] = 1
    else:
        pnode[val] += 1

    # global prop count
    if global_properties_counter:
        curr_depth = global_properties_counter["depth"]
        global_properties_counter["children"][curr_depth] += 1


def act_leaf(pre, typ, val, custom_obj):
    """ Function mapped to the  (string|boolean|number|null) event of ijson  
    Occurs when a primitive-typed value is encountered by ijson """
    node_type = infer_type(typ)
    lname = leaf_name(pre)

    custom_objects = custom_obj.copy()

    if lname in custom_objects.keys():  # value of a property
        try:
            pnode = custom_objects[lname]
        except Exception:
            print("".join(["=" for i in range(80)])) # ====...
            print(f"lname: {lname}")
            print(f"custom_object.keys(): {custom_objects.keys()}")
            print("".join(["=" for i in range(80)])) # ====...
            return

        pnode_types = pnode["types"]
        # only way to know a map_key we received is for a simple ("prop":string|number|boolean|null)
        if NodeType.PROPERTY not in pnode_types:
            create_object_type(pnode, NodeType.PROPERTY)
        pnode["ltype"], pnode = NodeType.PROPERTY, pnode_types[NodeType.PROPERTY]["children"]
    else:
        # value in an array
        (pname, number_arrays) = parent_name(pre, custom_objects)
        pnode = custom_objects[pname]
        pnode = pnode["types"][pnode["ltype"]]["children"]
        for _ in range(1, number_arrays):  # nested array handling
            if NodeType.ARRAY not in pnode:
                pnode[NodeType.ARRAY] = {"children": {}}
            pnode = pnode[NodeType.ARRAY]["children"]
    # occ
    if node_type not in pnode:
        pnode[node_type] = 1
    else:
        pnode[node_type] += 1

#
# IMPORTANT FUNCTIONS
#

def parsing_init(root_name, custom_objects):
    """ Initialization function of the node extractor  
    Creates a root object to allow for the recursion to have a base case  """
    global ROOT_NAME
    custom_objects.clear()
    ROOT_NAME = root_name
    create_object(root_name, custom_objects)
    create_object_type(custom_objects[root_name], NodeType.OBJECT)
    custom_objects[root_name]["ltype"] = NodeType.OBJECT


def parsing_done(custom_objects):
    global ROOT_NAME
    custom_objects.clear()
    ROOT_NAME = ""


def new_element_parsed(parsed, custom_objects, tcp, global_properties_counter, array_properties):
    """ Called each time an ijson event is caught  
    Parse the event and call the appropriate function """
    (pre, typ, val) = parsed

    if typ == "start_map":
        if tcp:
            tcp.OBJECT += 1
        if array_properties.is_in_array():
            array_properties.add_object() 
        act_start_map(pre, custom_objects, global_properties_counter)
    elif typ == "end_map":
        act_end_map(pre, global_properties_counter)
        if array_properties.is_in_array():
            array_properties.end_of_object() 
    elif typ == "start_array":
        if tcp:
            tcp.ARRAY += 1
        array_properties.nest()
        act_start_array(pre, custom_objects)
    elif typ == "end_array":
        act_end_array(pre, array_properties)
        array_properties.un_nest()
    elif typ == "map_key":
        act_map_key(pre, val, custom_objects, global_properties_counter)
    else:   # string, boolean, null, integer, number
        if tcp:
            if typ == "string":
                tcp.STRING += 1
                
                if val == "":
                    tcp.EMPTY_STRING += 1
                else:
                    tcp.NON_EMPTY_STRING += 1
                
                if val.lower() in ["true", "false"]:
                    tcp.ABUSED_STRING_BOOLEAN += 1

                try:
                    float(val)
                    tcp.ABUSED_STRING_NUMBER += 1
                except ValueError:
                    pass
            elif typ == "number":
                tcp.NUMBER += 1
            elif typ == "integer":
                tcp.INTEGER += 1
            elif typ == "boolean":
                tcp.BOOLEAN += 1
            elif typ == "null":
                tcp.NULL += 1
        if array_properties.is_in_array():
            array_properties.add_simple()
        act_leaf(pre, typ, val, custom_objects)


def parse_document(collection_name, file_path, tcp = False, custom_objects = {}):
    """ API used to benefit from the full functionalities of this part
    from the outside world """
    
    glob = {
        "depth": -1,
        "max_depth": 0,
        "children": [0]
    }

    array_properties = ArrayProperties()
    
    # parsing_init to deal with the root special case
    parsing_init(collection_name, custom_objects)

    is_geojson = False

    file = open(file_path, "rb")
    for p in ijson.parse(file):  # p = (prefix, event_type, value)
        (prefix, event, value) = p
        
        # Check for GeoJSON
        if event == "string" and prefix == "type" and value in geojson_types:
            is_geojson = True
        
        # Process ijson tuple
        new_element_parsed(p, custom_objects, tcp, glob, array_properties)
    file.close()

    return (custom_objects, glob, array_properties, is_geojson)