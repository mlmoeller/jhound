import json
import os
import sys

from helpers.colors import TColorsForeground, cstring
from helpers.mongohelper import Client

mongodb_client = Client()

# Read config.json to determine the name of the config document to use
parent_directory = __file__[:-len(os.path.basename(__file__))].replace("helpers/", "")
if os.path.exists(parent_directory + "config.json"):
    local_config_file = open(parent_directory + "config.json", "r", encoding="utf8")
    local_config = json.load(local_config_file)
    # print(local_config)
    local_config_file.close()

    if "config_name" in local_config:
        CONFIG_NAME = local_config["config_name"]
    else:
        exit(cstring("[ERROR] No config name found in config.json.", foreground=TColorsForeground.RED))

KEYS = [
    "max_links_download", "max_links_fetching", "chunk_size",
    "max_filesize", "skip_resources", "timeout", "storage_directory",
    "scraping_thread_count"
    # TODO: add master keys
]

# Default config parameters - scraper
MAX_LINKS_DOWNLOAD = 0
MAX_LINKS_FETCHING = 0
CHUNK_SIZE = 500
MAX_FILESIZE = 0
SKIP_RESOURCES = 0
TIMEOUT = 60
STORAGE_DIRECTORY = "./jhound"
SCRAPING_THREAD_COUNT = 4

# Default config parameters - Client/Server
MASTER_ADDRESS = "localhost"
MASTER_PORT = 9876
DELAY_SECONDS = 0
MAX_THREADS = 2
NODES = []
NODE_UPDATE_INTERVAL = 10

def print_config():
    """ Loads the config and prints parameters fetched from
    config.ini file. """
    load()
    
    if MAX_LINKS_DOWNLOAD:
        print(f"> Maximum number of downloads for this session: {MAX_LINKS_DOWNLOAD}")
    if MAX_LINKS_FETCHING:
        print(f"> Maximum number of links to be fetched for this session: {MAX_LINKS_FETCHING}")
    if MAX_FILESIZE:
        print(f"> Maximum file size: {MAX_FILESIZE} MiB") 
    print(f"> Chunk size: {CHUNK_SIZE}")
    print(f"> Skipping the first {SKIP_RESOURCES} resources links when scraping")
    print(f"> Datasets directory: {STORAGE_DIRECTORY}")
    print(f"> Using {SCRAPING_THREAD_COUNT} threads while scraping")
    print(f"> Timeout of downloads: {TIMEOUT}")
    print()
    print(f"> Master URL + port: {MASTER_ADDRESS}:{MASTER_PORT}")
    print(f"> Delay: {DELAY_SECONDS} seconds")
    print(f"> Max number of threads per node: {MAX_THREADS}")
    print(f"> Node will be looked up every {NODE_UPDATE_INTERVAL} seconds")
    print("> Nodes:")
    for node in NODES:
        for prop in node:
            print(f"  {prop}: {node[prop]}")
        print()


def read_config(config_name):
    """ Reads the configuration from the database.
    
        :param config_name: The name of the configuration document \
        that shall be used. """

    global MAX_LINKS_DOWNLOAD, MAX_LINKS_FETCHING, CHUNK_SIZE, SKIP_RESOURCES, \
        STORAGE_DIRECTORY, SCRAPING_THREAD_COUNT, TIMEOUT, MAX_FILESIZE, \
        MASTER_ADDRESS, MASTER_PORT, DELAY_SECONDS, MAX_THREADS, \
        NODES, NODE_UPDATE_INTERVAL

    config = mongodb_client.get_config(config_name)
    # Check if the config exists
    if not config:
        return

    if "max_links_download" in config:
        MAX_LINKS_DOWNLOAD = int(config["max_links_download"])

    if "max_links_fetching" in config:
        MAX_LINKS_FETCHING = int(config["max_links_fetching"])

    if "chunk_size" in config:
        CHUNK_SIZE = int(config["chunk_size"])

    if "max_filesize" in config:
        MAX_FILESIZE = int(config["max_filesize"])
    
    if "storage_directory" in config:
        STORAGE_DIRECTORY = config["storage_directory"]

    if "scraping_thread_count" in config:
        SCRAPING_THREAD_COUNT = config["scraping_thread_count"]
    
    if "skip_resources" in config:
        SKIP_RESOURCES = int(config["skip_resources"])
    
    if "timeout" in config:
        TIMEOUT = int(config["timeout"])

    if "master" in config:
        if "address" in config["master"]:
            MASTER_ADDRESS = config["master"]["address"]

        if "port" in config["master"]:
            MASTER_PORT = int(config["master"]["port"])

        if "delay_seconds" in config["master"]:
            DELAY_SECONDS = int(config["master"]["delay_seconds"])

        if "max_threads" in config["master"]:
            MAX_THREADS = int(config["master"]["max_threads"])

        if "node_update_interval" in config:
            NODE_UPDATE_INTERVAL = int(config["master"]["node_update_interval"])

    if "nodes" in config:
        NODES = list(config["nodes"])


def load():
    parent_directory = __file__[:-len(os.path.basename(__file__))].replace("helpers/", "")
    if os.path.exists(parent_directory + "config.json"):
        read_config(CONFIG_NAME)

load()

if __name__ == "__main__":
    print_config()
