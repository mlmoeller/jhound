#!/bin/bash

sudo tar -xzvf jhound.tar.gz --overwrite -C jhound/

for i in {1..4}
do
    sudo scp jhound.tar.gz jamc$i:jhound/
    sudo ssh jamc$i "killall python3 && cd jhound && tar -xzvf jhound.tar.gz --overwrite && nohup python3 node_server.py -p 1234 2>&1 && exit"
done
