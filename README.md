# data-tools

link to current version of the PDF file (recap.pdf): https://v2.overleaf.com/project/5b1516e728b2c369311cd01a

## sources :
- github_datasets : see the script github_scrap.sh for more info (Various formats)
- json-schema : scrap of all the schemas in [Schemastore](http://schemastore.org/json/) (Schemes)
- json-schema-kubernetes : scrap of all the (most recent) schemas stored [here](https://github.com/garethr/kubernetes-json-schema) (Schemes)

## CKAN scraper

A tool designed to provide means to both retrieve dataset URLs from CKAN databases and to download these datasets

[Link to the documentation](https://git.informatik.uni-rostock.de/nberton/data-tools/blob/master/ckan_scraper/README.md)


## JSON Documents

Extracting statistics from JSON documents

[Link to the documentation](https://git.informatik.uni-rostock.de/nberton/data-tools/blob/master/document-analytics/README.md)

## Extracting statistics from real JSON schemata

Extracting properties from JSON schemata created by various companies

[Link to the documentation](https://git.informatik.uni-rostock.de/nberton/data-tools/blob/master/schema-analytics/README.md)

## Link to the data storage
Available in [this repo](https://git.informatik.uni-rostock.de/nberton/data-storage)
