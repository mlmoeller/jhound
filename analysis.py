import getopt
import json
import os
import sys
import time
from os.path import basename, getsize, isfile, join

from helpers import confighelper
from helpers.classes.repositorystatus import RepositoryStatus
from helpers.classes.statisticsbuilder import StatisticsBuilder
from helpers.mongohelper import Client, DurationType
from helpers.nodeextractor import parse_document

mongodb_client = Client()

def compute_file(collection_name, document_name, full_file_path = None):
    """
    Compute stats for a single file.

    :param collection_name: name of the collection (probably a UUID)  
    :param document_name: name of the document to be analysed (probably a UUID as well)
    :returns: (filesize, StatisticsBuilder(), glob[], ArrayProperties()) tuple
    """

    file_path = join(confighelper.STORAGE_DIRECTORY, collection_name, document_name)
    print(f"{basename(file_path)} [{round(getsize(file_path) / (1024**2), 2)}MB]")

    # reset all internal variables of the statbuilder
    stat_builder = StatisticsBuilder()
    (custom_objects, glob, array_properties, is_geojson) = parse_document(
        collection_name, file_path, stat_builder.tcp
    )
    
    # execute a post-analysis function to eventually transform the stats in a better format
    stat_builder.full_analysis(custom_objects)
    
    # get file size and return the results as a tuple
    filesize = os.stat(file_path).st_size
    return (filesize, stat_builder, glob, array_properties, is_geojson)


def compute_files(repository_uuid):
    """
    Compute stats for a set of files.

    :param repository_uuid: The UUID of the repository holding the files.
    :returns: The duration in seconds (float), if everything went fine
    """

    start_time = time.time() # Capture start time

    uuid_list = mongodb_client.get_downloaded_files(repository_uuid)
    count = 0
    documents_len = len(uuid_list)
    
    for document_uuid in uuid_list:
        document_path = os.path.join(confighelper.STORAGE_DIRECTORY, repository_uuid, document_uuid)
        if not os.path.exists(document_path):
            raise FileNotFoundError(str(document_path))
        count += 1
        print(f"({count}/{documents_len}) - {basename(document_path)} [{round(getsize(document_path) / (1024**2), 2)}MB]")

        try:
            (filesize, stat_builder, glob, array_properties, is_geojson) = compute_file(
                repository_uuid,
                document_uuid,
                full_file_path=document_path
            )

            mongodb_client.store_full_document_results(
                repository_uuid,
                document_uuid,
                filesize,
                stat_builder,
                glob,
                array_properties,
                is_geojson
            )
        except Exception:
            pass
        

    duration = time.time() - start_time
    print(f"> Done. Duration: {round(duration, 2)} seconds")
    mongodb_client.store_duration(repository_uuid, DurationType.ANALYSIS_DURATION, duration)

    mongodb_client.set_repository_status(repository_uuid, RepositoryStatus.ANALYZED)
    return duration


if __name__ == "__main__":
    if len(sys.argv) < 2:
        exit("Missing positional parameter: UUID")

    uuid = sys.argv[1]
    duration = compute_files(uuid)
    print(f"Done analyzing repository {uuid}. Duration: {duration}")