import getopt
import multiprocessing
import os
import pickle
import platform
import socket
import sys
import threading
import time
import uuid

import distro
import psutil
import requests

from analysis import compute_file
from helpers import colors, commons, confighelper
from helpers.classes.job import AnalysisJob, DownloadJob

CLIENT_PORT = None # The port the node listens to
DIRECTORY = None # The local directory/mount point

OUTPUT = True

def oprint(text, end = "\n", file = sys.stdout):
    if OUTPUT:
        print(text, end=end, file=file)

# parameter management
try:
    (opts, args) = getopt.getopt(sys.argv[1:], "d:qp:", ["directory=", "quiet", "port="])
except getopt.GetoptError as err:
    exit(err)

for (opt, arg) in opts:
    if opt in ("-p", "--port"):
        CLIENT_PORT = int(arg)
    elif opt in ("-d", "--directory"):
        DIRECTORY = arg
    elif opt in ("-q", "--quiet"):
        OUTPUT = False

# Ask for a port if none was given
if not CLIENT_PORT:
    CLIENT_PORT = input("> Enter a port: ")
    try:
        CLIENT_PORT = int(CLIENT_PORT)
    except ValueError:
        exit("> The port number must be an integer. Exiting.")

# Address and port of the master server
SERVER_ADDRESS = confighelper.MASTER_ADDRESS
SERVER_PORT = confighelper.MASTER_PORT

# Each node gets a unique id from the master server to identify
NODE_ID = ""
repository_uuid = None

# The local directory of the repository files
# (probably some kind of mount point)
# STORAGE_DIRECTORY = DIRECTORY if DIRECTORY else confighelper.STORAGE_DIRECTORY
STORAGE_DIRECTORY = confighelper.STORAGE_DIRECTORY

# Initialize node socket
s = socket.socket()
s.bind(("", CLIENT_PORT))

# List of all running processes
processes = []
jobs = []


def remove_dead_processes():
    """ Checks the processes list for dead processes and removes them. \
    Also stores their exit codes in the corresponding jobs. """
    for process in processes[:]:
        if not process.is_alive():
            for job in jobs:
                if job.pid == process.pid:
                    job.exitcode = process.exitcode
            processes.remove(process)


def print_jobs():
    """ Prints all jobs that haven't terminated successfully (yet). """
    if len(jobs) > 0:
        colors.cprint("=" * 80, foreground=colors.TColorsForeground.RED)
        print("Jobs:")
        for job in [j for j in jobs if j.exitcode != 0]:
            print(job.type.value.upper(), job.descriptor, job.passed_time(), job.exitcode, sep="\t")
        colors.cprint("=" * 80, foreground=colors.TColorsForeground.RED)


def send_message(message):
    """ Sends a message to the master.

    :param message: The already encoded message that shall be sent. """
    send_socket = socket.socket()
    send_socket.connect((SERVER_ADDRESS, SERVER_PORT))
    send_socket.send(message)
    oprint("> Results sent.")
    send_socket.close()


def analyze(absolute_filename, relative_filename = None):
    """
    A temporary method for simulating the analysis process.

    :param absolute_filename: The absolute path to the file.
    :param relative_filename: The name of the file itself.
    """
    relative_filename = absolute_filename if not relative_filename else relative_filename
    result = compute_file("collection", absolute_filename)

    # Serialize result tuple and convert the outcoming byte sequence to
    # a hexadecimal string
    result_bytes = pickle.dumps(result)
    message = commons.create_message(f"ANALYZED {NODE_ID} {relative_filename} {result_bytes.hex()}")
    send_message(message)


def download(url):
    """
    Downloads a file and stores it into the STORAGE_DIRECTORY.
    
    :param url: The URL for the file.
    :returns: The UUID of the downloaded file, if the download was \
    successful, else False.
    """

    storage_path = os.path.join(STORAGE_DIRECTORY, repository_uuid)
    file_uuid = commons.download_file(url, storage_path, repository_uuid, OUTPUT)
    if file_uuid:
        oprint(f"> Successfully downloaded {url}. Stored as {file_uuid}.")
    else:
        oprint(f"> Error while downloading {url} (see MongoDB).")

    # Send response to master
    message = commons.create_message(f"DOWNLOADED {NODE_ID} {url} {file_uuid}")
    send_message(message)


def get_status():
    """ Returns the current node status as a message.
    
    :returns: The CPU percentage, available and total RAM as an encoded message. """

    cpu_percent = psutil.cpu_percent()
    ram = psutil.virtual_memory()
    platform_string = platform.platform()
    linux_distro = distro.linux_distribution()[0]
    jobs_bytes = pickle.dumps(jobs)
    # STATUS <node UUID> <CPU usage> <total RAM> <available (free) RAM> <running jobs> <OS> <distro>
    message = commons.create_message(f"STATUS {NODE_ID} {cpu_percent} {ram.total} {ram.available} {jobs_bytes.hex()} {platform_string} {linux_distro}")
    return message


def listen():
    """ Listens for incoming messages, e.g. by the master. """
    global STORAGE_DIRECTORY, s, NODE_ID, repository_uuid

    # Listen for incoming messages
    print("> Listening...")
    s.listen()
    while True:
        # Accept connection and receive content
        (con, addr) = s.accept()
        oprint(f"\n> Connection from {addr[0]}:{addr[1]}")
        message = commons.recvall(con)
        command = message.decode("utf8").split(" ")[0]

        # Distinguish between the possible commands
        oprint(f"> Command: {command}")
        # Shut the node down
        if command == "SHUTDOWN":
            oprint("> Shutting down.")
            s.close()
            exit()

        # Set the node's UUID and the UUID of the repository
        elif command == "UUID":
            NODE_ID = message.decode("utf8").split(" ")[1]
            repository_uuid = message.decode("utf8").split(" ")[2]
            print(f"> Node UUID: {NODE_ID}\n  Repository UUID: {repository_uuid}")
        
        # Send the current status of the node
        elif command == "STATUS":
            send_message(get_status())
        
        # Analyze a certain file in a repository
        elif command == "ANALYZE":
            filename = message.decode("utf8").split(" ")[1]
            absolute_filename = os.path.join(STORAGE_DIRECTORY, repository_uuid, filename)
            oprint(f"> File name: {filename}")

            process = multiprocessing.Process(target=analyze, args=(absolute_filename,))
            processes.append(process)
            process.start()

            job = AnalysisJob(filename, process.pid)
            jobs.append(job)

        # Download a certain URL and store its content to a new file
        elif command == "DOWNLOAD":
            file_url = message.decode("utf8").split(" ")[1]

            process = multiprocessing.Process(target=download, args=(file_url,))
            processes.append(process)
            process.start()
            
            job = DownloadJob(file_url, process.pid)
            jobs.append(job)

        oprint("> Connection closed.")
        con.close()


if __name__ == "__main__":
    try:
        threading.Thread(target=listen).start()

        threading.Thread(
            target=commons.execute_function_periodically,
            args=(remove_dead_processes, 60),
            daemon=True
        ).start()
        
        # threading.Thread(
        #     target=commons.execute_function_periodically,
        #     args=(print_jobs, 20),
        #     daemon=True
        # ).start()
    except KeyboardInterrupt:
        print("\n> Exiting...")
        for process in processes:
            process.terminate()
        exit()
