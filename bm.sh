staged_analysis() {
    # Legacy
    python3 analysis.py $1
    
    # 1 node
    python3 toggle.py 58bc6943-0621-472c-8bae-3071ab4545fb
    python3 main_server.py -ar $1
    
    # 2 nodes
    python3 toggle.py 8f56a6fd-b35e-4e87-b40f-09e2ebec3c07
    python3 main_server.py -ar $1
    
    # 3 nodes
    python3 toggle.py 930a3d8c-a3a2-4187-98db-de06a670b7f8
    python3 main_server.py -ar $1

    # 4 nodes
    python3 toggle.py fcc5af1d-935c-4b4e-af84-1ae9a154d1eb
    python3 main_server.py -ar $1

    # Reset
    python3 toggle.py all
}

staged_analysis ea8d204e-f764-42f1-9fb5-cf89e176c4df # CKAN UK
staged_analysis df638d9c-3f0a-4099-9dd3-17219ba759c8 # DataHub
staged_analysis ecb6ec96-e637-4bb6-a6dd-639b3a0e1964 # Dados BR
staged_analysis dc591076-14b3-4b15-8a0f-a0d49f45cb81 # gov.ie